﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

namespace Commom.Database
{
    //public partial class _Default : System.Web.UI.Page
    //{
    //    SqlConnection con = new SqlConnection();
    //    SqlCommand sqlcmd = new SqlCommand();
    //    SqlDataAdapter da = new SqlDataAdapter();
    //    DataTable dt = new DataTable();
    public static class _Default
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //}


        public static void btnBackup_Click()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand sqlcmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            //IF SQL Server Authentication then Connection String  
            //con.ConnectionString = @"Server=MyPC\SqlServer2k8;database=" + YourDBName + ";uid=sa;pwd=password;";  

            //IF Window Authentication then Connection String  
            con.ConnectionString = @"Server=KRITI\TEX;database=SOFTWARE;Integrated Security=true;";

            string backupDIR = "D:\\INVENTORYSONI\\Backup";
            if (!System.IO.Directory.Exists(backupDIR))
            {
                System.IO.Directory.CreateDirectory(backupDIR);
            }
            try
            {
                con.Open();
                sqlcmd = new SqlCommand("backup database SOFTWARE to disk='" + backupDIR + "\\" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".Bak'", con);
                sqlcmd.ExecuteNonQuery();
                con.Close();
                //  lblError.Text = "Backup database successfully";
            }
            catch (Exception ex)
            {
                 //lblError.Text = "Error Occured During DB backup process !<br>" + ex.ToString();
            }
        }
             public static void restore()
        {
            SqlConnection con = new SqlConnection();
            SqlCommand sqlcmd = new SqlCommand();
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            //IF SQL Server Authentication then Connection String  
            //con.ConnectionString = @"Server=MyPC\SqlServer2k8;database=" + YourDBName + ";uid=sa;pwd=password;";  

            //IF Window Authentication then Connection String  
            con.ConnectionString = @"Server=KRITI\TEX;database=SOFTWARE;Integrated Security=true;";

            string backupDIR = "D:\\INVENTORYSONI\\Backup";
            if (!System.IO.Directory.Exists(backupDIR))
            {
                System.IO.Directory.CreateDirectory(backupDIR);
            }
            try
            {
                con.Open();
                sqlcmd = new SqlCommand("restore database SOFTWARE from disk='" + backupDIR + "\\"  + "softwaremain.Bak'", con);
                sqlcmd.ExecuteNonQuery();
                con.Close();
                //  lblError.Text = "Backup database successfully";
            }
            catch (Exception ex)
            {
                // lblError.Text = "Error Occured During DB backup process !<br>" + ex.ToString();
            }
        }
    }
}