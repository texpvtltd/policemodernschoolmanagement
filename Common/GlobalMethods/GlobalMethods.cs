﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using System.Web;

namespace Commom.GlobalMethods
{
    public static class GlobalMethods
    {
       // private UnitOfWork _unitOfWork;
     
        /// <summary>
        /// Get 32 character Token string 
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            return string.Format("{0:ddMMyyyyhhmmssffffff}", DateTime.Now) + Guid.NewGuid().ToString("N").Substring(0, 12);
        }       
    }
}
