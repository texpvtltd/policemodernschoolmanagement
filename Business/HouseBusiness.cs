﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class HouseBusiness
{

    public HouseRepository _houseRepository;
    private UnitOfWork _unitOfWork;

    public HouseBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _houseRepository = new HouseRepository(dfactory);
        }

    public House Insert(House cs)
    {
        _houseRepository.Insert(cs);
        return cs;
    }

    public House Update(House cs)
    {
        _houseRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _houseRepository.Delete(id);
    }

    public void Delete(House cs)
    {
        _houseRepository.Delete(cs);
    }

    public House Find(long? id)
    {
        return _houseRepository.Find(id);
    }

    public IEnumerable<House> GetAllUsers()
    {
        return _houseRepository.Query().Select();
    }
    public Task<List<House>> GetListWTAsync(Expression<Func<House, bool>> exp = null, Func<IQueryable<House>, IOrderedQueryable<House>> orderby = null)
    {
        return _houseRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<House> GetListWT(Expression<Func<House, bool>> exp = null, Func<IQueryable<House>, IOrderedQueryable<House>> orderby = null)
    {
        return _houseRepository.GetListWithNoTracking(exp, orderby);
    }


    public House GetUserById(int id)
    {
        return _houseRepository.Query(u => u.HouseID == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(House user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(House cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }


   












}