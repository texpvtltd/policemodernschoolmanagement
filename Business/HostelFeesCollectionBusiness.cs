﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
   public  class HostelFeesCollectionBusiness
    {

         public HostelFeesCollectionTransferRepository _hostelfeesRepository;
        private UnitOfWork _unitOfWork;

        public HostelFeesCollectionBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _hostelfeesRepository = new HostelFeesCollectionTransferRepository(dfactory);
        }
        public HostelFeesCollection Insert(HostelFeesCollection cs)
        {
            _hostelfeesRepository.Insert(cs);
            return cs;
        }

        public HostelFeesCollection Update(HostelFeesCollection cs)
        {
            _hostelfeesRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _hostelfeesRepository.Delete(id);
        }

        public void Delete(HostelFeesCollection cs)
        {
            _hostelfeesRepository.Delete(cs);
        }

        public HostelFeesCollection Find(long? id)
        {
            return _hostelfeesRepository.Find(id);
        }

        public IEnumerable<HostelFeesCollection> GetAllUsers()
        {
            return _hostelfeesRepository.Query().Select();
        }
        public Task<List<HostelFeesCollection>> GetListWTAsync(Expression<Func<HostelFeesCollection, bool>> exp = null, Func<IQueryable<HostelFeesCollection>, IOrderedQueryable<HostelFeesCollection>> orderby = null)
        {
            return _hostelfeesRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HostelFeesCollection> GetListWT(Expression<Func<HostelFeesCollection, bool>> exp = null, Func<IQueryable<HostelFeesCollection>, IOrderedQueryable<HostelFeesCollection>> orderby = null)
        {
            return _hostelfeesRepository.GetListWithNoTracking(exp, orderby);
        }


        public HostelFeesCollection GetUserById(int id)
        {
            return _hostelfeesRepository.Query(u => u.FeesId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(HostelFeesCollection user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(HostelFeesCollection cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   


    }
}
