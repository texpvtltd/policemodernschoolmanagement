﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class BatchBusiness
{

    public BatchRepository _BatchRepository;
    private UnitOfWork _unitOfWork;

    public BatchBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _BatchRepository = new BatchRepository(dfactory);
    }

    public Batch Insert(Batch cs)
    {
        _BatchRepository.Insert(cs);
        return cs;
    }

    public Batch Update(Batch cs)
    {
        _BatchRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _BatchRepository.Delete(id);
    }

    public void Delete(Batch cs)
    {
        _BatchRepository.Delete(cs);
    }

    public Batch Find(long? id)
    {
        return _BatchRepository.Find(id);
    }

    public IEnumerable<Batch> GetAllUsers()
    {
        return _BatchRepository.Query().Select();
    }
    public Task<List<Batch>> GetListWTAsync(Expression<Func<Batch, bool>> exp = null, Func<IQueryable<Batch>, IOrderedQueryable<Batch>> orderby = null)
    {
        return _BatchRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<Batch> GetListWT(Expression<Func<Batch, bool>> exp = null, Func<IQueryable<Batch>, IOrderedQueryable<Batch>> orderby = null)
    {
        return _BatchRepository.GetListWithNoTracking(exp, orderby);
    }


    public Batch GetUserById(int id)
    {
        return _BatchRepository.Query(u => u.BatchId == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(Batch user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(Batch cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }















}