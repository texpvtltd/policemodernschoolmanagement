﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
  public  class HostalTypeBusiness
    {


        public HostalTypeRepository _hostypeRepository;
      private UnitOfWork _unitOfWork;

    public HostalTypeBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _hostypeRepository = new HostalTypeRepository(dfactory);
    }


    public HostalType Insert(HostalType cs)
    {
        _hostypeRepository.Insert(cs);
        return cs;
    }

    public HostalType Update(HostalType cs)
    {
        _hostypeRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _hostypeRepository.Delete(id);
    }

    public void Delete(HostalType cs)
    {
        _hostypeRepository.Delete(cs);
    }

    public HostalType Find(long? id)
    {
        return _hostypeRepository.Find(id);
    }

    public IEnumerable<HostalType> GetAllUsers()
    {
        return _hostypeRepository.Query().Select();
    }
    public Task<List<HostalType>> GetListWTAsync(Expression<Func<HostalType, bool>> exp = null, Func<IQueryable<HostalType>, IOrderedQueryable<HostalType>> orderby = null)
    {
        return _hostypeRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<HostalType> GetListWT(Expression<Func<HostalType, bool>> exp = null, Func<IQueryable<HostalType>, IOrderedQueryable<HostalType>> orderby = null)
    {
        return _hostypeRepository.GetListWithNoTracking(exp, orderby);
    }


    public HostalType GetUserById(int id)
    {
        return _hostypeRepository.Query(u => u.HostalTypeID == id).Select().FirstOrDefault();
    }







    public bool AddUpdateDeleteClass(HostalType user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(HostalType cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }































    }
}
