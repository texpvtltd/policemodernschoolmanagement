﻿
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;




namespace Business
{
   public class LeaveApplicationBusiness
    {



        
         public  LeaveApplicationRepository _leaveapplicationRepository;
        private UnitOfWork _unitOfWork;

        public LeaveApplicationBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _leaveapplicationRepository = new LeaveApplicationRepository(dfactory);
    }

        public AddLeaveApplication Insert(AddLeaveApplication cs)
        {
            _leaveapplicationRepository.Insert(cs);
            return cs;
        }

        public AddLeaveApplication Update(AddLeaveApplication cs)
        {
            _leaveapplicationRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _leaveapplicationRepository.Delete(id);
        }

        public void Delete(AddLeaveApplication cs)
        {
            _leaveapplicationRepository.Delete(cs);
        }

        public AddLeaveApplication Find(long? id)
        {
            return _leaveapplicationRepository.Find(id);
        }

        public IEnumerable<AddLeaveApplication> GetAllUsers()
        {
            return _leaveapplicationRepository.Query().Select();
        }
        public Task<List<AddLeaveApplication>> GetListWTAsync(Expression<Func<AddLeaveApplication, bool>> exp = null, Func<IQueryable<AddLeaveApplication>, IOrderedQueryable<AddLeaveApplication>> orderby = null)
        {
            return _leaveapplicationRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AddLeaveApplication> GetListWT(Expression<Func<AddLeaveApplication, bool>> exp = null, Func<IQueryable<AddLeaveApplication>, IOrderedQueryable<AddLeaveApplication>> orderby = null)
        {
            return _leaveapplicationRepository.GetListWithNoTracking(exp, orderby);
        }


        public AddLeaveApplication GetUserById(int id)
        {
            return _leaveapplicationRepository.Query(u => u.ApplicatId == id).Select().FirstOrDefault();
        }

        public bool AddUpdateDeleteAddAddLeaveApplication(AddLeaveApplication user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }




    }
}
