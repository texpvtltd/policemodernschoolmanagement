﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class SessionBusiness
    {


        public SessionRepository _SessionRepository;
        private UnitOfWork _unitOfWork;
        public SessionBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _SessionRepository = new SessionRepository(dfactory);
        }

        public Session Insert(Session cs)
        {
            _SessionRepository.Insert(cs);
            return cs;
        }

        public Session Update(Session cs)
        {
            _SessionRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _SessionRepository.Delete(id);
        }

        public void Delete(Session cs)
        {
            _SessionRepository.Delete(cs);
        }

        public Session Find(long? id)
        {
            return _SessionRepository.Find(id);
        }

        public IEnumerable<Session> GetAllUsers()
        {
            return _SessionRepository.Query().Select();
        }
        public Task<List<Session>> GetListWTAsync(Expression<Func<Session, bool>> exp = null, Func<IQueryable<Session>, IOrderedQueryable<Session>> orderby = null)
        {
            return _SessionRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Session> GetListWT(Expression<Func<Session, bool>> exp = null, Func<IQueryable<Session>, IOrderedQueryable<Session>> orderby = null)
        {
            return _SessionRepository.GetListWithNoTracking(exp, orderby);
        }


        public Session GetUserById(int id)
        {
            return _SessionRepository.Query(u => u.SessionID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(Session user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Session cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
