﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Business
{
    public class HostalDetailBusiness
    {
        public HostalDetailRepository _hosdetailRepository;
     private UnitOfWork _unitOfWork;

    public HostalDetailBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _hosdetailRepository = new HostalDetailRepository(dfactory);
    }


    public HostalDetail Insert(HostalDetail cs)
    {
        _hosdetailRepository.Insert(cs);
        return cs;
    }

    public HostalDetail Update(HostalDetail cs)
    {
        _hosdetailRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _hosdetailRepository.Delete(id);
    }

    public void Delete(HostalDetail cs)
    {
        _hosdetailRepository.Delete(cs);
    }

    public HostalDetail Find(long? id)
    {
        return _hosdetailRepository.Find(id);
    }

    public IEnumerable<HostalDetail> GetAllUsers()
    {
        return _hosdetailRepository.Query().Select();
    }
    public Task<List<HostalDetail>> GetListWTAsync(Expression<Func<HostalDetail, bool>> exp = null, Func<IQueryable<HostalDetail>, IOrderedQueryable<HostalDetail>> orderby = null)
    {
        return _hosdetailRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<HostalDetail> GetListWT(Expression<Func<HostalDetail, bool>> exp = null, Func<IQueryable<HostalDetail>, IOrderedQueryable<HostalDetail>> orderby = null)
    {
        return _hosdetailRepository.GetListWithNoTracking(exp, orderby);
    }


    public HostalDetail GetUserById(int id)
    {
        return _hosdetailRepository.Query(u => u.HostalId== id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(HostalDetail user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                user.IsDelete = "true";
              //  user.Schoolid = Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID);
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(HostalDetail cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }
























    }
}
