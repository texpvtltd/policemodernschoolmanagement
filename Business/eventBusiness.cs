﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{



    public class eventBusiness
    {
        public EventRepository _EventRepository;
        private UnitOfWork _unitOfWork;

        public eventBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _EventRepository = new EventRepository(dfactory);
        }

        public Event Insert(Event cs)
        {
            _EventRepository.Insert(cs);
            return cs;
        }

        public Event Update(Event cs)
        {
            _EventRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _EventRepository.Delete(id);
        }

        public void Delete(Event cs)
        {
            _EventRepository.Delete(cs);
        }

        public Event Find(long? id)
        {
            return _EventRepository.Find(id);
        }

        public IEnumerable<Event> GetAllUsers()
        {
            return _EventRepository.Query().Select();
        }
        public Task<List<Event>> GetListWTAsync(Expression<Func<Event, bool>> exp = null, Func<IQueryable<Event>, IOrderedQueryable<Event>> orderby = null)
        {
            return _EventRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Event> GetListWT(Expression<Func<Event, bool>> exp = null, Func<IQueryable<Event>, IOrderedQueryable<Event>> orderby = null)
        {
            return _EventRepository.GetListWithNoTracking(exp, orderby);
        }


        public Event GetUserById(int id)
        {
            return _EventRepository.Query(u => u.Id == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(Event user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Event cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
