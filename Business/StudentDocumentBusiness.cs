﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{

   public class StudentDocumentBusiness
    {

      public StudentDocumentRepository _docRepository;
        private UnitOfWork _unitOfWork;


        public StudentDocumentBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _docRepository = new StudentDocumentRepository(dfactory);
        }



        public StudentDocument Insert(StudentDocument cs)
        {
            _docRepository.Insert(cs);
            return cs;
        }

        public StudentDocument Update(StudentDocument cs)
        {
            _docRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _docRepository.Delete(id);
        }

        public void Delete(StudentDocument cs)
        {
            _docRepository.Delete(cs);
        }

        public StudentDocument Find(long? id)
        {
            return _docRepository.Find(id);
        }

        public IEnumerable<StudentDocument> GetAllUsers()
        {
            return _docRepository.Query().Select();
        }
        public Task<List<StudentDocument>> GetListWTAsync(Expression<Func<StudentDocument, bool>> exp = null, Func<IQueryable<StudentDocument>, IOrderedQueryable<StudentDocument>> orderby = null)
        {
            return _docRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<StudentDocument> GetListWT(Expression<Func<StudentDocument, bool>> exp = null, Func<IQueryable<StudentDocument>, IOrderedQueryable<StudentDocument>> orderby = null)
        {
            return _docRepository.GetListWithNoTracking(exp, orderby);
        }


        public StudentDocument GetUserById(int id)
        {
            return _docRepository.Query(u => u.DocID == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(StudentDocument user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(StudentDocument cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


    }






}
