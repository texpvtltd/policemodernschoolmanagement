﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
    public class VisitorBusiness
    {
        public VisitorRepository _VisitorRepository;
        private UnitOfWork _unitOfWork;
        public VisitorBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _VisitorRepository = new VisitorRepository(dfactory);
        }
        public Visitor Insert(Visitor cs)
        {
            _VisitorRepository.Insert(cs);
            return cs;
        }

        public Visitor Update(Visitor cs)
        {
            _VisitorRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _VisitorRepository.Delete(id);
        }

        public void Delete(Visitor cs)
        {
            _VisitorRepository.Delete(cs);
        }

        public Visitor Find(long? id)
        {
            return _VisitorRepository.Find(id);
        }

        public IEnumerable<Visitor> GetAllUsers()
        {
            return _VisitorRepository.Query().Select();
        }
        public Task<List<Visitor>> GetListWTAsync(Expression<Func<Visitor, bool>> exp = null, Func<IQueryable<Visitor>, IOrderedQueryable<Visitor>> orderby = null)
        {
            return _VisitorRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Visitor> GetListWT(Expression<Func<Visitor, bool>> exp = null, Func<IQueryable<Visitor>, IOrderedQueryable<Visitor>> orderby = null)
        {
            return _VisitorRepository.GetListWithNoTracking(exp, orderby);
        }


        public Visitor GetUserById(int id)
        {
            return _VisitorRepository.Query(u => u.Id == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteVisitor(Visitor user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Visitor cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
