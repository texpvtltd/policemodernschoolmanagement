﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
   public class SchoolBusiness
    {


          public SchoolRepository _schoolRepository;       
        private UnitOfWork _unitOfWork;
        public SchoolBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _schoolRepository = new SchoolRepository(dfactory);
        }

        public School Insert(School cs)
        {
            _schoolRepository.Insert(cs);
            return cs;
        }

        public School Update(School cs)
        {
            _schoolRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _schoolRepository.Delete(id);
        }

        public void Delete(School cs)
        {
            _schoolRepository.Delete(cs);
        }

        public School Find(long? id)
        {
            return _schoolRepository.Find(id);
        }

        public IEnumerable<School> GetAllUsers()
        {
            return _schoolRepository.Query().Select();
        }
        public Task<List<School>> GetListWTAsync(Expression<Func<School, bool>> exp = null, Func<IQueryable<School>, IOrderedQueryable<School>> orderby = null)
        {
            return _schoolRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<School> GetListWT(Expression<Func<School, bool>> exp = null, Func<IQueryable<School>, IOrderedQueryable<School>> orderby = null)
        {
            return _schoolRepository.GetListWithNoTracking(exp, orderby);
        }


        public School GetUserById(int id)
        {
            return _schoolRepository.Query(u => u.SchoolID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(School user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(School cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
              //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
