﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
   public class HostelVisitorBusiness
    {
        public HostelVisitorRepository _hostelvisitorRepository;
        private UnitOfWork _unitOfWork;

        public HostelVisitorBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _hostelvisitorRepository = new HostelVisitorRepository(dfactory);
        }
        public HostelVisitor Insert(HostelVisitor cs)
        {
            _hostelvisitorRepository.Insert(cs);
            return cs;
        }

        public HostelVisitor Update(HostelVisitor cs)
        {
            _hostelvisitorRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _hostelvisitorRepository.Delete(id);
        }

        public void Delete(HostelVisitor cs)
        {
            _hostelvisitorRepository.Delete(cs);
        }

        public HostelVisitor Find(long? id)
        {
            return _hostelvisitorRepository.Find(id);
        }

        public IEnumerable<HostelVisitor> GetAllUsers()
        {
            return _hostelvisitorRepository.Query().Select();
        }
        public Task<List<HostelVisitor>> GetListWTAsync(Expression<Func<HostelVisitor, bool>> exp = null, Func<IQueryable<HostelVisitor>, IOrderedQueryable<HostelVisitor>> orderby = null)
        {
            return _hostelvisitorRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HostelVisitor> GetListWT(Expression<Func<HostelVisitor, bool>> exp = null, Func<IQueryable<HostelVisitor>, IOrderedQueryable<HostelVisitor>> orderby = null)
        {
            return _hostelvisitorRepository.GetListWithNoTracking(exp, orderby);
        }


        public HostelVisitor GetUserById(int id)
        {
            return _hostelvisitorRepository.Query(u => u.VisitorId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteHostelVisitor(HostelVisitor user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(HostelVisitor cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   

    }
}
