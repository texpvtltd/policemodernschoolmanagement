﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class EmployeeBusiness
{

    public EmployeeRepository _EmployeeRepository;
    private UnitOfWork _unitOfWork;

    public EmployeeBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _EmployeeRepository = new EmployeeRepository(dfactory);
    }

    public Employee Insert(Employee cs)
    {
        _EmployeeRepository.Insert(cs);
        return cs;
    }

    public Employee Update(Employee cs)
    {
        _EmployeeRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _EmployeeRepository.Delete(id);
    }

    public void Delete(Employee cs)
    {
        _EmployeeRepository.Delete(cs);
    }

    public Employee Find(long? id)
    {
        return _EmployeeRepository.Find(id);
    }

    public IEnumerable<Employee> GetAllUsers()
    {
        return _EmployeeRepository.Query().Select();
    }
    public Task<List<Employee>> GetListWTAsync(Expression<Func<Employee, bool>> exp = null, Func<IQueryable<Employee>, IOrderedQueryable<Employee>> orderby = null)
    {
        return _EmployeeRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<Employee> GetListWT(Expression<Func<Employee, bool>> exp = null, Func<IQueryable<Employee>, IOrderedQueryable<Employee>> orderby = null)
    {
        return _EmployeeRepository.GetListWithNoTracking(exp, orderby);
    }


    public Employee GetUserById(int id)
    {
        return _EmployeeRepository.Query(u => u.EmpId == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(Employee user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(Employee cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }















}