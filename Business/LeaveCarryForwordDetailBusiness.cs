﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
  public  class LeaveCarryForwordDetailBusiness
    {
        



                public LeaveCrryForwordDetailRepository _carryforwordbusiness;
        private UnitOfWork _unitOfWork;

        public LeaveCarryForwordDetailBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _carryforwordbusiness = new LeaveCrryForwordDetailRepository(dfactory);
    }
        public LeaveCarryForwardDetail Insert(LeaveCarryForwardDetail cs)
        {
            _carryforwordbusiness.Insert(cs);
            return cs;
        }

        public LeaveCarryForwardDetail Update(LeaveCarryForwardDetail cs)
        {
            _carryforwordbusiness.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _carryforwordbusiness.Delete(id);
        }

        public void Delete(LeaveCarryForwardDetail cs)
        {
            _carryforwordbusiness.Delete(cs);
        }

        public LeaveCarryForwardDetail Find(long? id)
        {
            return _carryforwordbusiness.Find(id);
        }

        public IEnumerable<LeaveCarryForwardDetail> GetAllUsers()
        {
            return _carryforwordbusiness.Query().Select();
        }
        public Task<List<LeaveCarryForwardDetail>> GetListWTAsync(Expression<Func<LeaveCarryForwardDetail, bool>> exp = null, Func<IQueryable<LeaveCarryForwardDetail>, IOrderedQueryable<LeaveCarryForwardDetail>> orderby = null)
        {
            return _carryforwordbusiness.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<LeaveCarryForwardDetail> GetListWT(Expression<Func<LeaveCarryForwardDetail, bool>> exp = null, Func<IQueryable<LeaveCarryForwardDetail>, IOrderedQueryable<LeaveCarryForwardDetail>> orderby = null)
        {
            return _carryforwordbusiness.GetListWithNoTracking(exp, orderby);
        }


        public LeaveCarryForwardDetail GetUserById(int id)
        {
            return _carryforwordbusiness.Query(u => u.LeaveCarryForwordId == id).Select().FirstOrDefault();
        }

        public bool AddUpdateDeleteAddLeaveCarryForwardDetail(LeaveCarryForwardDetail user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }












    }
}
