﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
  public  class LeaveCategoryBusiness
    {




        public LeaveCategoryRepository _leavecategorybusiness;
        private UnitOfWork _unitOfWork;

        public LeaveCategoryBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _leavecategorybusiness = new LeaveCategoryRepository(dfactory);
    }


        public LeaveCategory Insert(LeaveCategory cs)
        {
            _leavecategorybusiness.Insert(cs);
            return cs;
        }

        public LeaveCategory Update(LeaveCategory cs)
        {
            _leavecategorybusiness.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _leavecategorybusiness.Delete(id);
        }

        public void Delete(LeaveCategory cs)
        {
            _leavecategorybusiness.Delete(cs);
        }

        public LeaveCategory Find(long? id)
        {
            return _leavecategorybusiness.Find(id);
        }

        public IEnumerable<LeaveCategory> GetAllUsers()
        {
            return _leavecategorybusiness.Query().Select();
        }
        public Task<List<LeaveCategory>> GetListWTAsync(Expression<Func<LeaveCategory, bool>> exp = null, Func<IQueryable<LeaveCategory>, IOrderedQueryable<LeaveCategory>> orderby = null)
        {
            return _leavecategorybusiness.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<LeaveCategory> GetListWT(Expression<Func<LeaveCategory, bool>> exp = null, Func<IQueryable<LeaveCategory>, IOrderedQueryable<LeaveCategory>> orderby = null)
        {
            return _leavecategorybusiness.GetListWithNoTracking(exp, orderby);
        }


        public LeaveCategory GetUserById(int id)
        {
            return _leavecategorybusiness.Query(u => u.LeaveCatId == id).Select().FirstOrDefault();
        }

        public bool AddUpdateDeleteAddLeaveCategory(LeaveCategory user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }














    }
}
