﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{


   
     public class AttendenceBusiness
    {
        public AttendanceRepository _attendenceRepository;
        private UnitOfWork _unitOfWork;

        public AttendenceBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _attendenceRepository = new AttendanceRepository(dfactory);
        }


        public Attendance Insert(Attendance cs)
        {
            _attendenceRepository.Insert(cs);
            return cs;
        }

        public Attendance Update(Attendance cs)
        {
            _attendenceRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _attendenceRepository.Delete(id);
        }

        public void Delete(Attendance cs)
        {
            _attendenceRepository.Delete(cs);
        }

        public Attendance Find(long? id)
        {
            return _attendenceRepository.Find(id);
        }

        public IEnumerable<Attendance> GetAllUsers()
        {
            return _attendenceRepository.Query().Select();
        }
        public Task<List<Attendance>> GetListWTAsync(Expression<Func<Attendance, bool>> exp = null, Func<IQueryable<Attendance>, IOrderedQueryable<Attendance>> orderby = null)
        {
            return _attendenceRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Attendance> GetListWT(Expression<Func<Attendance, bool>> exp = null, Func<IQueryable<Attendance>, IOrderedQueryable<Attendance>> orderby = null)
        {
            return _attendenceRepository.GetListWithNoTracking(exp, orderby);
        }


        public Attendance GetUserById(int id)
        {
            return _attendenceRepository.Query(u => u.AttendanceID == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(Attendance user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Attendance cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   























    }
}
