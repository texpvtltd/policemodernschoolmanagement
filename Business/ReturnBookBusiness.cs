﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
   public  class ReturnBookBusiness
    {
        public ReturnBookRepository returnbookRepository;
        private UnitOfWork _unitOfWork;


        public ReturnBookBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            returnbookRepository = new ReturnBookRepository(dfactory);
        }


        public Returnbook Insert(Returnbook cs)
        {
            returnbookRepository.Insert(cs);
            return cs;
        }

        public Returnbook Update(Returnbook cs)
        {
            returnbookRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            returnbookRepository.Delete(id);
        }

        public void Delete(Returnbook cs)
        {
            returnbookRepository.Delete(cs);
        }

        public Returnbook Find(long? id)
        {
            return returnbookRepository.Find(id);
        }

        public IEnumerable<Returnbook> GetAllUsers()
        {
            return returnbookRepository.Query().Select();
        }
        public Task<List<Returnbook>> GetListWTAsync(Expression<Func<Returnbook, bool>> exp = null, Func<IQueryable<Returnbook>, IOrderedQueryable<Returnbook>> orderby = null)
        {
            return returnbookRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Returnbook> GetListWT(Expression<Func<Returnbook, bool>> exp = null, Func<IQueryable<Returnbook>, IOrderedQueryable<Returnbook>> orderby = null)
        {
            return returnbookRepository.GetListWithNoTracking(exp, orderby);
        }


        public Returnbook GetUserById(int id)
        {
            return returnbookRepository.Query(u => u.ReturnBookID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteReturnbook(Returnbook user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Returnbook cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   



















    }
}
