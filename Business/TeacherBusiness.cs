﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class TeacherBusiness
    {


        public SchoolUserRepository _SchoolUserRepository;
        private UnitOfWork _unitOfWork;
        public TeacherBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _SchoolUserRepository = new SchoolUserRepository(dfactory);
        }

        public SchoolUser Insert(SchoolUser cs)
        {
            _SchoolUserRepository.Insert(cs);
            return cs;
        }

        public SchoolUser Update(SchoolUser cs)
        {
            _SchoolUserRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _SchoolUserRepository.Delete(id);
        }

        public void Delete(SchoolUser cs)
        {
            _SchoolUserRepository.Delete(cs);
        }

        public SchoolUser Find(long? id)
        {
            return _SchoolUserRepository.Find(id);
        }

        public IEnumerable<SchoolUser> GetAllUsers()
        {
            return _SchoolUserRepository.Query().Select();
        }
        public Task<List<SchoolUser>> GetListWTAsync(Expression<Func<SchoolUser, bool>> exp = null, Func<IQueryable<SchoolUser>, IOrderedQueryable<SchoolUser>> orderby = null)
        {
            return _SchoolUserRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<SchoolUser> GetListWT(Expression<Func<SchoolUser, bool>> exp = null, Func<IQueryable<SchoolUser>, IOrderedQueryable<SchoolUser>> orderby = null)
        {
            return _SchoolUserRepository.GetListWithNoTracking(exp, orderby);
        }


        public SchoolUser GetUserById(int id)
        {
            return _SchoolUserRepository.Query(u => u.SchoolUserID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(SchoolUser user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(SchoolUser cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
