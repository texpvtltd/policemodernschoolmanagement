﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class SchoolRoleBusiness
{

    public SchoolroleRepository _SchoolRoleRepository;
    private UnitOfWork _unitOfWork;

    public SchoolRoleBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _SchoolRoleRepository = new SchoolroleRepository(dfactory);
    }

    public SchoolRole Insert(SchoolRole cs)
    {
        _SchoolRoleRepository.Insert(cs);
        return cs;
    }

    public SchoolRole Update(SchoolRole cs)
    {
        _SchoolRoleRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _SchoolRoleRepository.Delete(id);
    }

    public void Delete(SchoolRole cs)
    {
        _SchoolRoleRepository.Delete(cs);
    }

    public SchoolRole Find(long? id)
    {
        return _SchoolRoleRepository.Find(id);
    }

    public IEnumerable<SchoolRole> GetAllUsers()
    {
        return _SchoolRoleRepository.Query().Select();
    }
    public Task<List<SchoolRole>> GetListWTAsync(Expression<Func<SchoolRole, bool>> exp = null, Func<IQueryable<SchoolRole>, IOrderedQueryable<SchoolRole>> orderby = null)
    {
        return _SchoolRoleRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<SchoolRole> GetListWT(Expression<Func<SchoolRole, bool>> exp = null, Func<IQueryable<SchoolRole>, IOrderedQueryable<SchoolRole>> orderby = null)
    {
        return _SchoolRoleRepository.GetListWithNoTracking(exp, orderby);
    }


    public SchoolRole GetUserById(int id)
    {
        return _SchoolRoleRepository.Query(u => u.RoleId == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(SchoolRole user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                user.IsDelete = "true";
                user.Schoolid =  Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID);
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(SchoolRole cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }















}