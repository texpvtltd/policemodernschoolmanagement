﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
      
  public  class BookCategoryBusiness
    {
        public BookCategoryRepository _bookcategory;
        private UnitOfWork _unitOfWork;



          public BookCategoryBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _bookcategory = new BookCategoryRepository(dfactory);
        }

          public BookCategory Insert(BookCategory cs)
          {
              _bookcategory.Insert(cs);
              return cs;
          }

          public BookCategory Update(BookCategory cs)
          {
              _bookcategory.Update(cs);
              return cs;
          }
          public void Delete(long? id)
          {
              _bookcategory.Delete(id);
          }

          public void Delete(BookCategory cs)
          {
              _bookcategory.Delete(cs);
          }

          public BookCategory Find(long? id)
          {
              return _bookcategory.Find(id);
          }

          public IEnumerable<BookCategory> GetAllUsers()
          {
              return _bookcategory.Query().Select();
          }
          public Task<List<BookCategory>> GetListWTAsync(Expression<Func<BookCategory, bool>> exp = null, Func<IQueryable<BookCategory>, IOrderedQueryable<BookCategory>> orderby = null)
          {
              return _bookcategory.GetListWithNoTrackingAsync(exp, orderby);
          }

          public List<BookCategory> GetListWT(Expression<Func<BookCategory, bool>> exp = null, Func<IQueryable<BookCategory>, IOrderedQueryable<BookCategory>> orderby = null)
          {
              return _bookcategory.GetListWithNoTracking(exp, orderby);
          }


          public BookCategory GetUserById(int id)
          {
              return _bookcategory.Query(u => u.BookCategoryID == id).Select().FirstOrDefault();
          }


          public bool AddUpdateDeleteBookCategory(BookCategory user, string action)
          {
              bool isSuccess = true;
              try
              {
                  if (action == "I")
                  {
                      Insert(user);
                  }
                  else if (action == "U")
                  {

                      Update(user);
                  }
                  else if (action == "D")
                  {
                      Delete(user);
                  }
                  _unitOfWork.SaveChanges();
              }
              catch (Exception ex)
              {
                  isSuccess = false;
                  throw ex;
              }
              return isSuccess;
          }

          public bool ProfileUpdate(BookCategory cs, string action, int vid)
          {
              bool isSuccess = true;
              try
              {
                  //  user.Password = Md5Encryption.Encrypt(user.Password);




                  if (action == "I")
                  {
                      Insert(cs);
                  }
                  else if (action == "U")
                  {
                      Update(cs);
                  }
                  else if (action == "D")
                  {
                      Delete(cs);
                  }
                  _unitOfWork.SaveChanges();
              }
              catch (Exception ex)
              {
                  isSuccess = false;
                  throw ex;
              }
              return isSuccess;
          }
















    }
}
