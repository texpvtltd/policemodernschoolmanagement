﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Business
{
   public class IssueBookBusiness
    {
        public BookIssueRepository _bookissueRepository;
        private UnitOfWork _unitOfWork;
        public IssueBookBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _bookissueRepository = new BookIssueRepository(dfactory);
        }
        public IssueBook Insert(IssueBook cs)
        {
            _bookissueRepository.Insert(cs);
            return cs;
        }

        public IssueBook Update(IssueBook cs)
        {
            _bookissueRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _bookissueRepository.Delete(id);
        }

        public void Delete(IssueBook cs)
        {
            _bookissueRepository.Delete(cs);
        }

        public IssueBook Find(long? id)
        {
            return _bookissueRepository.Find(id);
        }

        public IEnumerable<IssueBook> GetAllUsers()
        {
            return _bookissueRepository.Query().Select();
        }
        public Task<List<IssueBook>> GetListWTAsync(Expression<Func<IssueBook, bool>> exp = null, Func<IQueryable<IssueBook>, IOrderedQueryable<IssueBook>> orderby = null)
        {
            return _bookissueRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<IssueBook> GetListWT(Expression<Func<IssueBook, bool>> exp = null, Func<IQueryable<IssueBook>, IOrderedQueryable<IssueBook>> orderby = null)
        {
            return _bookissueRepository.GetListWithNoTracking(exp, orderby);
        }


        public IssueBook GetUserById(int id)
        {
            return _bookissueRepository.Query(u => u.BookIssuedID == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteBook(IssueBook user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(IssueBook cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   






















    }
}
