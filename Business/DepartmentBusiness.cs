﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class DepartmentBusiness
{

    public DepartmentRepository _DepartmentRepository;
    private UnitOfWork _unitOfWork;

    public DepartmentBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _DepartmentRepository = new DepartmentRepository(dfactory);
    }

    public Department Insert(Department cs)
    {
        _DepartmentRepository.Insert(cs);
        return cs;
    }

    public Department Update(Department cs)
    {
        _DepartmentRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _DepartmentRepository.Delete(id);
    }

    public void Delete(Department cs)
    {
        _DepartmentRepository.Delete(cs);
    }

    public Department Find(long? id)
    {
        return _DepartmentRepository.Find(id);
    }

    public IEnumerable<Department> GetAllUsers()
    {
        return _DepartmentRepository.Query().Select();
    }
    public Task<List<Department>> GetListWTAsync(Expression<Func<Department, bool>> exp = null, Func<IQueryable<Department>, IOrderedQueryable<Department>> orderby = null)
    {
        return _DepartmentRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<Department> GetListWT(Expression<Func<Department, bool>> exp = null, Func<IQueryable<Department>, IOrderedQueryable<Department>> orderby = null)
    {
        return _DepartmentRepository.GetListWithNoTracking(exp, orderby);
    }


    public Department GetUserById(int id)
    {
        return _DepartmentRepository.Query(u => u.DeptId == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(Department user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                user.IsDelete = "true";
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(Department cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }















}