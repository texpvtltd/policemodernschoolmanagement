﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
    public class AddTransportBusiness
    {
          public AddTransportRepository _addtransportRepository;
        private UnitOfWork _unitOfWork;

        public AddTransportBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _addtransportRepository = new AddTransportRepository(dfactory);
        }



        public AddTransport Insert(AddTransport cs)
        {
            _addtransportRepository.Insert(cs);
            return cs;
        }

        public AddTransport Update(AddTransport cs)
        {
            _addtransportRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _addtransportRepository.Delete(id);
        }

        public void Delete(AddTransport cs)
        {
            _addtransportRepository.Delete(cs);
        }

        public AddTransport Find(long? id)
        {
            return _addtransportRepository.Find(id);
        }

        public IEnumerable<AddTransport> GetAllUsers()
        {
            return _addtransportRepository.Query().Select();
        }
        public Task<List<AddTransport>> GetListWTAsync(Expression<Func<AddTransport, bool>> exp = null, Func<IQueryable<AddTransport>, IOrderedQueryable<AddTransport>> orderby = null)
        {
            return _addtransportRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AddTransport> GetListWT(Expression<Func<AddTransport, bool>> exp = null, Func<IQueryable<AddTransport>, IOrderedQueryable<AddTransport>> orderby = null)
        {
            return _addtransportRepository.GetListWithNoTracking(exp, orderby);
        }


        public AddTransport GetUserById(int id)
        {
            return _addtransportRepository.Query(u => u.VehicleID == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(AddTransport user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AddTransport cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   



    }
}
