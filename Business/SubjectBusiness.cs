﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class SubjectBusiness
    {


        public SubjectRepository _SubjectRepository;
        private UnitOfWork _unitOfWork;
        public SubjectBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _SubjectRepository = new SubjectRepository(dfactory);
        }

        public subjectnew Insert(subjectnew cs)
        {
            _SubjectRepository.Insert(cs);
            return cs;
        }

        public subjectnew Update(subjectnew cs)
        {
            _SubjectRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _SubjectRepository.Delete(id);
        }

        public void Delete(subjectnew cs)
        {
            _SubjectRepository.Delete(cs);
        }

        public subjectnew Find(long? id)
        {
            return _SubjectRepository.Find(id);
        }

        public IEnumerable<subjectnew> GetAllUsers()
        {
            return _SubjectRepository.Query().Select();
        }
        public Task<List<subjectnew>> GetListWTAsync(Expression<Func<subjectnew, bool>> exp = null, Func<IQueryable<subjectnew>, IOrderedQueryable<subjectnew>> orderby = null)
        {
            return _SubjectRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<subjectnew> GetListWT(Expression<Func<subjectnew, bool>> exp = null, Func<IQueryable<subjectnew>, IOrderedQueryable<subjectnew>> orderby = null)
        {
            return _SubjectRepository.GetListWithNoTracking(exp, orderby);
        }


        public subjectnew GetUserById(int id)
        {
            return _SubjectRepository.Query(u => u.SubjectID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(subjectnew user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(subjectnew cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
