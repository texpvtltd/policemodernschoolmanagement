﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
  public  class LeaveDetailBusiness
    {


           public LeaveDetailRepository _leavedetailbusiness;
        private UnitOfWork _unitOfWork;

        public LeaveDetailBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _leavedetailbusiness = new LeaveDetailRepository(dfactory);
    }

        public LeaveDetail Insert(LeaveDetail cs)
        {
              _leavedetailbusiness  .Insert(cs);
            return cs;
        }

        public LeaveDetail Update(LeaveDetail cs)
        {
              _leavedetailbusiness  .Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
              _leavedetailbusiness  .Delete(id);
        }

        public void Delete(LeaveDetail cs)
        {
              _leavedetailbusiness  .Delete(cs);
        }

        public LeaveDetail Find(long? id)
        {
            return   _leavedetailbusiness  .Find(id);
        }

        public IEnumerable<LeaveDetail> GetAllUsers()
        {
            return   _leavedetailbusiness  .Query().Select();
        }
        public Task<List<LeaveDetail>> GetListWTAsync(Expression<Func<LeaveDetail, bool>> exp = null, Func<IQueryable<LeaveDetail>, IOrderedQueryable<LeaveDetail>> orderby = null)
        {
            return   _leavedetailbusiness  .GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<LeaveDetail> GetListWT(Expression<Func<LeaveDetail, bool>> exp = null, Func<IQueryable<LeaveDetail>, IOrderedQueryable<LeaveDetail>> orderby = null)
        {
            return   _leavedetailbusiness  .GetListWithNoTracking(exp, orderby);
        }


        public LeaveDetail GetUserById(int id)
        {
            return   _leavedetailbusiness  .Query(u => u.LeaveDetailId == id).Select().FirstOrDefault();
        }

        public bool AddUpdateDeleteAddLeaveDetail(LeaveDetail user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }














    }
}
