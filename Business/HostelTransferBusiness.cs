﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
   public  class HostelTransferBusiness
    {


         public HostelTransferRepository _hostetransferrepository;       
        private UnitOfWork _unitOfWork;
        public HostelTransferBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _hostetransferrepository = new HostelTransferRepository(dfactory);
        }



        public HostelTransfer Insert(HostelTransfer cs)
        {
            _hostetransferrepository.Insert(cs);
            return cs;
        }

        public HostelTransfer Update(HostelTransfer cs)
        {
            _hostetransferrepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _hostetransferrepository.Delete(id);
        }

        public void Delete(HostelTransfer cs)
        {
            _hostetransferrepository.Delete(cs);
        }

        public HostelTransfer Find(long? id)
        {
            return _hostetransferrepository.Find(id);
        }

        public IEnumerable<HostelTransfer> GetAllUsers()
        {
            return _hostetransferrepository.Query().Select();
        }
        public Task<List<HostelTransfer>> GetListWTAsync(Expression<Func<HostelTransfer, bool>> exp = null, Func<IQueryable<HostelTransfer>, IOrderedQueryable<HostelTransfer>> orderby = null)
        {
            return _hostetransferrepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HostelTransfer> GetListWT(Expression<Func<HostelTransfer, bool>> exp = null, Func<IQueryable<HostelTransfer>, IOrderedQueryable<HostelTransfer>> orderby = null)
        {
            return _hostetransferrepository.GetListWithNoTracking(exp, orderby);
        }


        public HostelTransfer GetUserById(int id)
        {
            return _hostetransferrepository.Query(u => u.TransferId == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteHostelTransfer(HostelTransfer user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(HostelTransfer cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   




















    }
}
