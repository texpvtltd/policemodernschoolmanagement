﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
   public  class ClassBusiness
    {
        public ClassRepository _classRepository;       
        private UnitOfWork _unitOfWork;
        public ClassBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _classRepository = new ClassRepository(dfactory);
        }

        public Class Insert(Class cs)
        {
            _classRepository.Insert(cs);
            return cs;
        }

        public Class Update(Class cs)
        {
            _classRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _classRepository.Delete(id);
        }

        public void Delete(Class cs)
        {
            _classRepository.Delete(cs);
        }

        public Class Find(long? id)
        {
            return _classRepository.Find(id);
        }

        public IEnumerable<Class> GetAllUsers()
        {
            return _classRepository.Query().Select();
        }
        public Task<List<Class>> GetListWTAsync(Expression<Func<Class, bool>> exp = null, Func<IQueryable<Class>, IOrderedQueryable<Class>> orderby = null)
        {
            return _classRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Class> GetListWT(Expression<Func<Class, bool>> exp = null, Func<IQueryable<Class>, IOrderedQueryable<Class>> orderby = null)
        {
            return _classRepository.GetListWithNoTracking(exp, orderby);
        }


        public Class GetUserById(int id)
        {
            return _classRepository.Query(u => u.ClassID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(Class user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Class cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
              //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   


    }
}
