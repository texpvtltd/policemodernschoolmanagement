﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
  public  class SectionBusiness
    {


        public SectionRepository _SectionRepository;       
        private UnitOfWork _unitOfWork;
        public SectionBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _SectionRepository = new SectionRepository(dfactory);
        }

        public Section Insert(Section cs)
        {
            _SectionRepository.Insert(cs);
            return cs;
        }

        public Section Update(Section cs)
        {
            _SectionRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _SectionRepository.Delete(id);
        }

        public void Delete(Section cs)
        {
            _SectionRepository.Delete(cs);
        }

        public Section Find(long? id)
        {
            return _SectionRepository.Find(id);
        }

        public IEnumerable<Section> GetAllUsers()
        {
            return _SectionRepository.Query().Select();
        }
        public Task<List<Section>> GetListWTAsync(Expression<Func<Section, bool>> exp = null, Func<IQueryable<Section>, IOrderedQueryable<Section>> orderby = null)
        {
            return _SectionRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Section> GetListWT(Expression<Func<Section, bool>> exp = null, Func<IQueryable<Section>, IOrderedQueryable<Section>> orderby = null)
        {
            return _SectionRepository.GetListWithNoTracking(exp, orderby);
        }


        public Section GetUserById(int id)
        {
            return _SectionRepository.Query(u => u.SectionID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteData(Section user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Section cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
              //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }























    }
}
