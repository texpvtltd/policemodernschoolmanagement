﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
     public class HostelRegisterBusiness
    {
        public HostelRegisterRepository _hostelRegisterRepository;       
        private UnitOfWork _unitOfWork;
        public HostelRegisterBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _hostelRegisterRepository = new HostelRegisterRepository(dfactory);
        }

        public HostelRegister Insert(HostelRegister cs)
        {
            _hostelRegisterRepository.Insert(cs);
            return cs;
        }

        public HostelRegister Update(HostelRegister cs)
        {
            _hostelRegisterRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _hostelRegisterRepository.Delete(id);
        }

        public void Delete(HostelRegister cs)
        {
            _hostelRegisterRepository.Delete(cs);
        }

        public HostelRegister Find(long? id)
        {
            return _hostelRegisterRepository.Find(id);
        }

        public IEnumerable<HostelRegister> GetAllUsers()
        {
            return _hostelRegisterRepository.Query().Select();
        }
        public Task<List<HostelRegister>> GetListWTAsync(Expression<Func<HostelRegister, bool>> exp = null, Func<IQueryable<HostelRegister>, IOrderedQueryable<HostelRegister>> orderby = null)
        {
            return _hostelRegisterRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HostelRegister> GetListWT(Expression<Func<HostelRegister, bool>> exp = null, Func<IQueryable<HostelRegister>, IOrderedQueryable<HostelRegister>> orderby = null)
        {
            return _hostelRegisterRepository.GetListWithNoTracking(exp, orderby);
        }


        public HostelRegister GetUserById(int id)
        {
            return _hostelRegisterRepository.Query(u => u.HostelRegID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteHostelRegister(HostelRegister user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(HostelRegister cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   


    }
}
