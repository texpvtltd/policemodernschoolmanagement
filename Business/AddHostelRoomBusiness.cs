﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
 public   class AddHostelRoomBusiness
    {
        public AddHostelRoomRepository _addhostelroomRepository;
        private UnitOfWork _unitOfWork;

        public AddHostelRoomBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _addhostelroomRepository = new AddHostelRoomRepository(dfactory);
        }
        public AddHostalRoom Insert(AddHostalRoom cs)
        {
            _addhostelroomRepository .Insert(cs);
            return cs;
        }

        public AddHostalRoom Update(AddHostalRoom cs)
        {
            _addhostelroomRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _addhostelroomRepository.Delete(id);
        }

        public void Delete(AddHostalRoom cs)
        {
            _addhostelroomRepository.Delete(cs);
        }

        public AddHostalRoom Find(long? id)
        {
            return _addhostelroomRepository.Find(id);
        }

        public IEnumerable<AddHostalRoom> GetAllUsers()
        {
            return _addhostelroomRepository.Query().Select();
        }
        public Task<List<AddHostalRoom>> GetListWTAsync(Expression<Func<AddHostalRoom, bool>> exp = null, Func<IQueryable<AddHostalRoom>, IOrderedQueryable<AddHostalRoom>> orderby = null)
        {
            return _addhostelroomRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AddHostalRoom> GetListWT(Expression<Func<AddHostalRoom, bool>> exp = null, Func<IQueryable<AddHostalRoom>, IOrderedQueryable<AddHostalRoom>> orderby = null)
        {
            return _addhostelroomRepository.GetListWithNoTracking(exp, orderby);
        }


        public AddHostalRoom GetUserById(int id)
        {
            return _addhostelroomRepository.Query(u => u.RoomId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteAddHostalRoom(AddHostalRoom user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AddHostalRoom cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   






    }
}
