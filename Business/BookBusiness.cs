﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
   public class BookBusiness
    {
        public BookRepository _bookRepository;
        private UnitOfWork _unitOfWork;
         public BookBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _bookRepository = new BookRepository(dfactory);
        }
         public Book Insert(Book cs)
         {
             _bookRepository.Insert(cs);
             return cs;
         }

         public Book Update(Book cs)
         {
             _bookRepository.Update(cs);
             return cs;
         }
         public void Delete(long? id)
         {
             _bookRepository.Delete(id);
         }

         public void Delete(Book cs)
         {
             _bookRepository.Delete(cs);
         }

         public Book Find(long? id)
         {
             return _bookRepository.Find(id);
         }

         public IEnumerable<Book> GetAllUsers()
         {
             return _bookRepository.Query().Select();
         }
         public Task<List<Book>> GetListWTAsync(Expression<Func<Book, bool>> exp = null, Func<IQueryable<Book>, IOrderedQueryable<Book>> orderby = null)
         {
             return _bookRepository.GetListWithNoTrackingAsync(exp, orderby);
         }

         public List<Book> GetListWT(Expression<Func<Book, bool>> exp = null, Func<IQueryable<Book>, IOrderedQueryable<Book>> orderby = null)
         {
             return _bookRepository.GetListWithNoTracking(exp, orderby);
         }


         public Book GetUserById(int id)
         {
             return _bookRepository.Query(u => u.BookID == id).Select().FirstOrDefault();
         }


         public bool AddUpdateDeleteBook(Book user, string action)
         {
             bool isSuccess = true;
             try
             {
                 if (action == "I")
                 {
                     Insert(user);
                 }
                 else if (action == "U")
                 {

                     Update(user);
                 }
                 else if (action == "D")
                 {
                     Delete(user);
                 }
                 _unitOfWork.SaveChanges();
             }
             catch (Exception ex)
             {
                 isSuccess = false;
                 throw ex;
             }
             return isSuccess;
         }

         public bool ProfileUpdate(Book cs, string action, int vid)
         {
             bool isSuccess = true;
             try
             {
                 //  user.Password = Md5Encryption.Encrypt(user.Password);




                 if (action == "I")
                 {
                     Insert(cs);
                 }
                 else if (action == "U")
                 {
                     Update(cs);
                 }
                 else if (action == "D")
                 {
                     Delete(cs);
                 }
                 _unitOfWork.SaveChanges();
             }
             catch (Exception ex)
             {
                 isSuccess = false;
                 throw ex;
             }
             return isSuccess;
         }


   


    }
}
