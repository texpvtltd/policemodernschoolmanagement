﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class AssignTeacherBusiness
    {
        public AssignTeacherRepository _AssignTeacherRepository;
        private UnitOfWork _unitOfWork;
        public AssignTeacherBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _AssignTeacherRepository = new AssignTeacherRepository(dfactory);
        }

        public AssignTeacher Insert(AssignTeacher cs)
        {
            _AssignTeacherRepository.Insert(cs);
            return cs;
        }

        public AssignTeacher Update(AssignTeacher cs)
        {
            _AssignTeacherRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _AssignTeacherRepository.Delete(id);
        }

        public void Delete(AssignTeacher cs)
        {
            _AssignTeacherRepository.Delete(cs);
        }

        public AssignTeacher Find(long? id)
        {
            return _AssignTeacherRepository.Find(id);
        }

        public IEnumerable<AssignTeacher> GetAllUsers()
        {
            return _AssignTeacherRepository.Query().Select();
        }
        public Task<List<AssignTeacher>> GetListWTAsync(Expression<Func<AssignTeacher, bool>> exp = null, Func<IQueryable<AssignTeacher>, IOrderedQueryable<AssignTeacher>> orderby = null)
        {
            return _AssignTeacherRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AssignTeacher> GetListWT(Expression<Func<AssignTeacher, bool>> exp = null, Func<IQueryable<AssignTeacher>, IOrderedQueryable<AssignTeacher>> orderby = null)
        {
            return _AssignTeacherRepository.GetListWithNoTracking(exp, orderby);
        }


        public AssignTeacher GetUserById(int id)
        {
            return _AssignTeacherRepository.Query(u => u.AssignTeacherId == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteAssignTeacher(AssignTeacher user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AssignTeacher cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
