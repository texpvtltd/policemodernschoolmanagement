﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class StudentAdmissionBusiness
    {


        public StudentAdmissionRepository _StudentAdmissionRepository;
        private UnitOfWork _unitOfWork;
        public StudentAdmissionBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _StudentAdmissionRepository = new StudentAdmissionRepository(dfactory);
        }

        public StudentAdmission Insert(StudentAdmission cs)
        {
            _StudentAdmissionRepository.Insert(cs);
            return cs;
        }

        public StudentAdmission Update(StudentAdmission cs)
        {
            _StudentAdmissionRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _StudentAdmissionRepository.Delete(id);
        }

        public void Delete(StudentAdmission cs)
        {
            _StudentAdmissionRepository.Delete(cs);
        }

        public StudentAdmission Find(long? id)
        {
            return _StudentAdmissionRepository.Find(id);
        }

        public IEnumerable<StudentAdmission> GetAllUsers()
        {
            return _StudentAdmissionRepository.Query().Select();
        }
        public Task<List<StudentAdmission>> GetListWTAsync(Expression<Func<StudentAdmission, bool>> exp = null, Func<IQueryable<StudentAdmission>, IOrderedQueryable<StudentAdmission>> orderby = null)
        {
            return _StudentAdmissionRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<StudentAdmission> GetListWT(Expression<Func<StudentAdmission, bool>> exp = null, Func<IQueryable<StudentAdmission>, IOrderedQueryable<StudentAdmission>> orderby = null)
        {
            return _StudentAdmissionRepository.GetListWithNoTracking(exp, orderby);
        }


        public StudentAdmission GetUserById(int id)
        {
            return _StudentAdmissionRepository.Query(u => u.AddmissionID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(StudentAdmission user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(StudentAdmission cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
