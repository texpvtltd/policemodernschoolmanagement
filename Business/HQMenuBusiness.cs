﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace Business
{
    class HQMenuBusiness
    {

        private HQMenuRepository _HQRepository;
        private FormsAuthenticationFactory _formsAuthenticationFactory;
        private UnitOfWork _unitOfWork;
        public HQMenuBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _HQRepository = new HQMenuRepository(dfactory);

            this._formsAuthenticationFactory = new FormsAuthenticationFactory();
        }
        public HQMenu Insert(HQMenu hq)
        {
            _HQRepository.Insert(hq);
            return hq;
        }

        public HQMenu Update(HQMenu hq)
        {
            _HQRepository.Update(hq);
            return hq;
        }

        public void Deleteid(long? id)
        {
            _HQRepository.Delete(id);
        }
        public void Delete(HQMenu hq)
        {
            _HQRepository.Delete(hq);
        }

        public HQMenu Find(long? id)
        {
            return _HQRepository.Find(id);
        }

        public IEnumerable<HQMenu> GetAllUsers()
        {
            return _HQRepository.Query().Select();
        }

        public Task<List<HQMenu>> GetListWTAsync(Expression<Func<HQMenu, bool>> exp = null, Func<IQueryable<HQMenu>, IOrderedQueryable<HQMenu>> orderby = null)
        {
            return _HQRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HQMenu> GetListWT(Expression<Func<HQMenu, bool>> exp = null, Func<IQueryable<HQMenu>, IOrderedQueryable<HQMenu>> orderby = null)
        {
            return _HQRepository.GetListWithNoTracking(exp, orderby);
        }


        public HQMenu GetUserById(int id)
        {
            return _HQRepository.Query(u => u.MenuId== id).Select().FirstOrDefault();
        }
        

        public bool AddUpdateDeleteUser(HQMenu hq, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(hq);
                }
                else if (action == "U")
                {

                    Update(hq);
                }
                else if (action == "D")
                {
                    Delete(hq);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }
        private List<Menu> assignedMenuList = new List<Menu>();
        public List<Menu> GetAssignedMenu(string menuId)
        {
            //var menulist = menuId.Split(',');
            //var menuList = _HQRepository.GetNT<Menu>().Where(m => menulist.Contains(m.Id.ToString()));
            ////var menuList = _HQRepository.SelectAsync()(m => menulist.Contains(m.Id.ToString()));


            //foreach (var menu in menuList)
            //{
            //    var alreadyexistsinlist = assignedMenuList.Where(c => c.Id == menu.Id);
            //    if (alreadyexistsinlist.Count() <= 0)
            //    {
            //        assignedMenuList.Add(menu);
            //        var parentmenu = _repository.Select<Menu>(m => m.Id == menu.ParentId && menu.ParentId != 0).FirstOrDefault();
            //        if (parentmenu != null)
            //        {
            //            GetAssignedMenu(menu.ParentId.ToString());
            //        }
               // }
            //}
            return assignedMenuList;
        }   



    }
}
