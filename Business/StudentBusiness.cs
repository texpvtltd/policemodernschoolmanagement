﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

public class StudentBusiness
{

    public StudentRepository _StudentRepository;
    private UnitOfWork _unitOfWork;

    public StudentBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
    {
        DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
        _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
        _StudentRepository = new StudentRepository(dfactory);
    }

    public Student Insert(Student cs)
    {
        _StudentRepository.Insert(cs);
        return cs;
    }

    public Student Update(Student cs)
    {
        _StudentRepository.Update(cs);
        return cs;
    }
    public void Delete(long? id)
    {
        _StudentRepository.Delete(id);
    }

    public void Delete(Student cs)
    {
        _StudentRepository.Delete(cs);
    }

    public Student Find(long? id)
    {
        return _StudentRepository.Find(id);
    }

    public IEnumerable<Student> GetAllUsers()
    {
        return _StudentRepository.Query().Select();
    }
    public Task<List<Student>> GetListWTAsync(Expression<Func<Student, bool>> exp = null, Func<IQueryable<Student>, IOrderedQueryable<Student>> orderby = null)
    {
        return _StudentRepository.GetListWithNoTrackingAsync(exp, orderby);
    }

    public List<Student> GetListWT(Expression<Func<Student, bool>> exp = null, Func<IQueryable<Student>, IOrderedQueryable<Student>> orderby = null)
    {
        return _StudentRepository.GetListWithNoTracking(exp, orderby);
    }


    public Student GetUserById(int id)
    {
        return _StudentRepository.Query(u => u.StudentID == id).Select().FirstOrDefault();
    }


    public bool AddUpdateDeleteClass(Student user, string action)
    {
        bool isSuccess = true;
        try
        {
            if (action == "I")
            {
                Insert(user);
            }
            else if (action == "U")
            {

                Update(user);
            }
            else if (action == "D")
            {
                Delete(user);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }

    public bool ProfileUpdate(Student cs, string action, int vid)
    {
        bool isSuccess = true;
        try
        {
            //  user.Password = Md5Encryption.Encrypt(user.Password);




            if (action == "I")
            {
                Insert(cs);
            }
            else if (action == "U")
            {
                Update(cs);
            }
            else if (action == "D")
            {
                Delete(cs);
            }
            _unitOfWork.SaveChanges();
        }
        catch (Exception ex)
        {
            isSuccess = false;
            throw ex;
        }
        return isSuccess;
    }















}