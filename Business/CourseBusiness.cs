﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class CourseBusiness
    {
        public CourseRepository _CourseRepository;
        private UnitOfWork _unitOfWork;
        public CourseBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _CourseRepository = new CourseRepository(dfactory);
        }

        public Course Insert(Course cs)
        {
            _CourseRepository.Insert(cs);
            return cs;
        }

        public Course Update(Course cs)
        {
            _CourseRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _CourseRepository.Delete(id);
        }

        public void Delete(Course cs)
        {
            _CourseRepository.Delete(cs);
        }

        public Course Find(long? id)
        {
            return _CourseRepository.Find(id);
        }

        public IEnumerable<Course> GetAllUsers()
        {
            return _CourseRepository.Query().Select();
        }
        public Task<List<Course>> GetListWTAsync(Expression<Func<Course, bool>> exp = null, Func<IQueryable<Course>, IOrderedQueryable<Course>> orderby = null)
        {
            return _CourseRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Course> GetListWT(Expression<Func<Course, bool>> exp = null, Func<IQueryable<Course>, IOrderedQueryable<Course>> orderby = null)
        {
            return _CourseRepository.GetListWithNoTracking(exp, orderby);
        }


        public Course GetUserById(int id)
        {
            return _CourseRepository.Query(u => u.CourseId == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteCourse(Course user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Course cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
