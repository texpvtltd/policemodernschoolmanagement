﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
    public class PrincipalBusiness
    {


        public PrincipalRepository _princiRepository;
        private UnitOfWork _unitOfWork;
        public PrincipalBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _princiRepository = new PrincipalRepository(dfactory);
        }

        public Principal Insert(Principal cs)
        {
            _princiRepository.Insert(cs);
            return cs;
        }

        public Principal Update(Principal cs)
        {
            _princiRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _princiRepository.Delete(id);
        }

        public void Delete(Principal cs)
        {
            _princiRepository.Delete(cs);
        }

        public Principal Find(long? id)
        {
            return _princiRepository.Find(id);
        }

        public IEnumerable<Principal> GetAllUsers()
        {
            return _princiRepository.Query().Select();
        }
        public Task<List<Principal>> GetListWTAsync(Expression<Func<Principal, bool>> exp = null, Func<IQueryable<Principal>, IOrderedQueryable<Principal>> orderby = null)
        {
            return _princiRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<Principal> GetListWT(Expression<Func<Principal, bool>> exp = null, Func<IQueryable<Principal>, IOrderedQueryable<Principal>> orderby = null)
        {
            return _princiRepository.GetListWithNoTracking(exp, orderby);
        }


        public Principal GetUserById(int id)
        {
            return _princiRepository.Query(u => u.PrincipalID == id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(Principal user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(Principal cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
