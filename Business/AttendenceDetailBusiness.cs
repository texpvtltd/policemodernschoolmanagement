﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
  public  class AttendenceDetailBusiness
    {

      public  AttendanceDetailRepository _attendencedetailRepository;
        private UnitOfWork _unitOfWork;

        public AttendenceDetailBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _attendencedetailRepository = new AttendanceDetailRepository(dfactory);
        }
        public AttendanceDetail Insert(AttendanceDetail cs)
        {
            _attendencedetailRepository.Insert(cs);
            return cs;
        }

        public AttendanceDetail Update(AttendanceDetail cs)
        {
            _attendencedetailRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _attendencedetailRepository.Delete(id);
        }

        public void Delete(AttendanceDetail cs)
        {
            _attendencedetailRepository.Delete(cs);
        }

        public AttendanceDetail Find(long? id)
        {
            return _attendencedetailRepository.Find(id);
        }

        public IEnumerable<AttendanceDetail> GetAllUsers()
        {
            return _attendencedetailRepository.Query().Select();
        }
        public Task<List<AttendanceDetail>> GetListWTAsync(Expression<Func<AttendanceDetail, bool>> exp = null, Func<IQueryable<AttendanceDetail>, IOrderedQueryable<AttendanceDetail>> orderby = null)
        {
            return _attendencedetailRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AttendanceDetail> GetListWT(Expression<Func<AttendanceDetail, bool>> exp = null, Func<IQueryable<AttendanceDetail>, IOrderedQueryable<AttendanceDetail>> orderby = null)
        {
            return _attendencedetailRepository.GetListWithNoTracking(exp, orderby);
        }


        public AttendanceDetail GetUserById(int id)
        {
            return _attendencedetailRepository.Query(u => u.AttDetailID ==id).Select().FirstOrDefault();
        }







        public bool AddUpdateDeleteClass(AttendanceDetail user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AttendanceDetail cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }



    }
}
