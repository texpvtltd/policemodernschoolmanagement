﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
  public  class TransPortAllocationBusiness
    {

        public TransPortAllocationRepository _transportRepository;
        private UnitOfWork _unitOfWork;

        public TransPortAllocationBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _transportRepository = new TransPortAllocationRepository(dfactory);
        }

        public TransportAllocation Insert(TransportAllocation cs)
        {
            _transportRepository.Insert(cs);
            return cs;
        }

        public TransportAllocation Update(TransportAllocation cs)
        {
            _transportRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _transportRepository.Delete(id);
        }

        public void Delete(TransportAllocation cs)
        {
            _transportRepository.Delete(cs);
        }

        public TransportAllocation Find(long? id)
        {
            return _transportRepository.Find(id);
        }

        public IEnumerable<TransportAllocation> GetAllUsers()
        {
            return _transportRepository.Query().Select();
        }
        public Task<List<TransportAllocation>> GetListWTAsync(Expression<Func<TransportAllocation, bool>> exp = null, Func<IQueryable<TransportAllocation>, IOrderedQueryable<TransportAllocation>> orderby = null)
        {
            return _transportRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<TransportAllocation> GetListWT(Expression<Func<TransportAllocation, bool>> exp = null, Func<IQueryable<TransportAllocation>, IOrderedQueryable<TransportAllocation>> orderby = null)
        {
            return _transportRepository.GetListWithNoTracking(exp, orderby);
        }


        public TransportAllocation GetUserById(int id)
        {
            return _transportRepository.Query(u => u.AllocationId== id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteTransportAllocation(TransportAllocation user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(TransportAllocation cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   


















    }
}
