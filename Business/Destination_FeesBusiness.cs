﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
   public class Destination_FeesBusiness
    {

       public AddDestinationandFeesRepository _adddestinationfeesrepository;
        private UnitOfWork _unitOfWork;
        public Destination_FeesBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _adddestinationfeesrepository = new AddDestinationandFeesRepository(dfactory);
        }
        public AddDestination_Fees Insert(AddDestination_Fees cs)
        {
            _adddestinationfeesrepository.Insert(cs);
            return cs;
        }

        public AddDestination_Fees Update(AddDestination_Fees cs)
        {
            _adddestinationfeesrepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _adddestinationfeesrepository.Delete(id);
        }

        public void Delete(AddDestination_Fees cs)
        {
            _adddestinationfeesrepository.Delete(cs);
        }

        public AddDestination_Fees Find(long? id)
        {
            return _adddestinationfeesrepository.Find(id);
        }

        public IEnumerable<AddDestination_Fees> GetAllUsers()
        {
            return _adddestinationfeesrepository.Query().Select();
        }
        public Task<List<AddDestination_Fees>> GetListWTAsync(Expression<Func<AddDestination_Fees, bool>> exp = null, Func<IQueryable<AddDestination_Fees>, IOrderedQueryable<AddDestination_Fees>> orderby = null)
        {
            return _adddestinationfeesrepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AddDestination_Fees> GetListWT(Expression<Func<AddDestination_Fees, bool>> exp = null, Func<IQueryable<AddDestination_Fees>, IOrderedQueryable<AddDestination_Fees>> orderby = null)
        {
            return _adddestinationfeesrepository.GetListWithNoTracking(exp, orderby);
        }


        public AddDestination_Fees GetUserById(int id)
        {
            return _adddestinationfeesrepository.Query(u => u.DestinationId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteAddDestination_Fees(AddDestination_Fees user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AddDestination_Fees cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   








    }
}
