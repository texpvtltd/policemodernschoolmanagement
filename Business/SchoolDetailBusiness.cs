﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{



    public class SchoolDetailBusiness
    {
        public SchoolDetailRepository _SchoolDetailRepository;
        private UnitOfWork _unitOfWork;

        public SchoolDetailBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _SchoolDetailRepository = new SchoolDetailRepository(dfactory);
        }

        public SchoolDetail Insert(SchoolDetail cs)
        {
            _SchoolDetailRepository.Insert(cs);
            return cs;
        }

        public SchoolDetail Update(SchoolDetail cs)
        {
            _SchoolDetailRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _SchoolDetailRepository.Delete(id);
        }

        public void Delete(SchoolDetail cs)
        {
            _SchoolDetailRepository.Delete(cs);
        }

        public SchoolDetail Find(long? id)
        {
            return _SchoolDetailRepository.Find(id);
        }

        public IEnumerable<SchoolDetail> GetAllUsers()
        {
            return _SchoolDetailRepository.Query().Select();
        }
        public Task<List<SchoolDetail>> GetListWTAsync(Expression<Func<SchoolDetail, bool>> exp = null, Func<IQueryable<SchoolDetail>, IOrderedQueryable<SchoolDetail>> orderby = null)
        {
            return _SchoolDetailRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<SchoolDetail> GetListWT(Expression<Func<SchoolDetail, bool>> exp = null, Func<IQueryable<SchoolDetail>, IOrderedQueryable<SchoolDetail>> orderby = null)
        {
            return _SchoolDetailRepository.GetListWithNoTracking(exp, orderby);
        }


        public SchoolDetail GetUserById(int id)
        {
            return _SchoolDetailRepository.Query(u => u.SchoolDetID == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(SchoolDetail schooldet, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(schooldet);
                }
                else if (action == "U")
                {

                    Update(schooldet);
                }
                else if (action == "D")
                {
                    Delete(schooldet);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(SchoolDetail cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
