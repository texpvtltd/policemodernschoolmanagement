﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{
     public class TFeesCollection
    {
         public TransportFeesCollectionRepository _trepository;
        private UnitOfWork _unitOfWork;
        public TFeesCollection(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _trepository = new TransportFeesCollectionRepository(dfactory);
        }


        public TransprotFee Insert(TransprotFee cs)
        {
            _trepository.Insert(cs);
            return cs;
        }

        public TransprotFee Update(TransprotFee cs)
        {
            _trepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _trepository.Delete(id);
        }

        public void Delete(TransprotFee cs)
        {
            _trepository.Delete(cs);
        }

        public TransprotFee Find(long? id)
        {
            return _trepository.Find(id);
        }

        public IEnumerable<TransprotFee> GetAllUsers()
        {
            return _trepository.Query().Select();
        }
        public Task<List<TransprotFee>> GetListWTAsync(Expression<Func<TransprotFee, bool>> exp = null, Func<IQueryable<TransprotFee>, IOrderedQueryable<TransprotFee>> orderby = null)
        {
            return _trepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<TransprotFee> GetListWT(Expression<Func<TransprotFee, bool>> exp = null, Func<IQueryable<TransprotFee>, IOrderedQueryable<TransprotFee>> orderby = null)
        {
            return _trepository.GetListWithNoTracking(exp, orderby);
        }


        public TransprotFee GetUserById(int id)
        {
            return _trepository.Query(u => u.TransPortFeeCollId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(TransprotFee user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(TransprotFee cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

    }
}
