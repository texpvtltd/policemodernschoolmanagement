﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{



    public class AcademicSettingBusiness
    {
        public AcademicSettingRepository _academicsettingRepository;
        private UnitOfWork _unitOfWork;

        public AcademicSettingBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _academicsettingRepository = new AcademicSettingRepository(dfactory);
        }

        public academicsetting Insert(academicsetting cs)
        {
            _academicsettingRepository.Insert(cs);
            return cs;
        }

        public academicsetting Update(academicsetting cs)
        {
            _academicsettingRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _academicsettingRepository.Delete(id);
        }

        public void Delete(academicsetting cs)
        {
            _academicsettingRepository.Delete(cs);
        }

        public academicsetting Find(long? id)
        {
            return _academicsettingRepository.Find(id);
        }

        public IEnumerable<academicsetting> GetAllUsers()
        {
            return _academicsettingRepository.Query().Select();
        }
        public Task<List<academicsetting>> GetListWTAsync(Expression<Func<academicsetting, bool>> exp = null, Func<IQueryable<academicsetting>, IOrderedQueryable<academicsetting>> orderby = null)
        {
            return _academicsettingRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<academicsetting> GetListWT(Expression<Func<academicsetting, bool>> exp = null, Func<IQueryable<academicsetting>, IOrderedQueryable<academicsetting>> orderby = null)
        {
            return _academicsettingRepository.GetListWithNoTracking(exp, orderby);
        }


        public academicsetting GetUserById(int id)
        {
            return _academicsettingRepository.Query(u => u.Id == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(academicsetting schooldet, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(schooldet);
                }
                else if (action == "U")
                {

                    Update(schooldet);
                }
                else if (action == "D")
                {
                    Delete(schooldet);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(academicsetting cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }





    }
}
