﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
namespace Business
{
  public   class HostelAllocationBusiness
    {

        public HostelAllocationRepository _hostelalloRepository;
        private UnitOfWork _unitOfWork;


        public HostelAllocationBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _hostelalloRepository = new HostelAllocationRepository(dfactory);
        }



        public HostelAllocation Insert(HostelAllocation cs)
        {
            _hostelalloRepository.Insert(cs);
            return cs;
        }

        public HostelAllocation Update(HostelAllocation cs)
        {
            _hostelalloRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
            _hostelalloRepository.Delete(id);
        }

        public void Delete(HostelAllocation cs)
        {
            _hostelalloRepository.Delete(cs);
        }

        public HostelAllocation Find(long? id)
        {
            return _hostelalloRepository.Find(id);
        }

        public IEnumerable<HostelAllocation> GetAllUsers()
        {
            return _hostelalloRepository.Query().Select();
        }
        public Task<List<HostelAllocation>> GetListWTAsync(Expression<Func<HostelAllocation, bool>> exp = null, Func<IQueryable<HostelAllocation>, IOrderedQueryable<HostelAllocation>> orderby = null)
        {
            return _hostelalloRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<HostelAllocation> GetListWT(Expression<Func<HostelAllocation, bool>> exp = null, Func<IQueryable<HostelAllocation>, IOrderedQueryable<HostelAllocation>> orderby = null)
        {
            return _hostelalloRepository.GetListWithNoTracking(exp, orderby);
        }


        public HostelAllocation GetUserById(int id)
        {
            return _hostelalloRepository.Query(u => u.HostelAllocatedId == id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(HostelAllocation user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(HostelAllocation cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   







    }
}
