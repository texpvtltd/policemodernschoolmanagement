﻿using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Filters.AuthenticationCore;
using Filters.AuthenticationModel;
using Repository.RepositoryFactoryCore;
using Repository.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Business
{


      
  public  class AddDriverBusiness
    {
        public AddDriverRepository _adddriverRepository;
        private UnitOfWork _unitOfWork;

        public AddDriverBusiness(DatabaseFactory df = null, UnitOfWork uow = null)
        {
            DatabaseFactory dfactory = df == null ? new DatabaseFactory() : df;
            _unitOfWork = uow == null ? new UnitOfWork(dfactory) : uow;
            _adddriverRepository = new AddDriverRepository(dfactory);
        }

        public AddDriver Insert(AddDriver cs)
        {
           _adddriverRepository.Insert(cs);
            return cs;
        }

        public AddDriver Update(AddDriver cs)
        {
           _adddriverRepository.Update(cs);
            return cs;
        }
        public void Delete(long? id)
        {
           _adddriverRepository.Delete(id);
        }

        public void Delete(AddDriver cs)
        {
           _adddriverRepository.Delete(cs);
        }

        public AddDriver Find(long? id)
        {
            return _adddriverRepository.Find(id);
        }

        public IEnumerable<AddDriver> GetAllUsers()
        {
            return _adddriverRepository.Query().Select();
        }
        public Task<List<AddDriver>> GetListWTAsync(Expression<Func<AddDriver, bool>> exp = null, Func<IQueryable<AddDriver>, IOrderedQueryable<AddDriver>> orderby = null)
        {
            return _adddriverRepository.GetListWithNoTrackingAsync(exp, orderby);
        }

        public List<AddDriver> GetListWT(Expression<Func<AddDriver, bool>> exp = null, Func<IQueryable<AddDriver>, IOrderedQueryable<AddDriver>> orderby = null)
        {
            return _adddriverRepository.GetListWithNoTracking(exp, orderby);
        }


        public AddDriver GetUserById(int id)
        {
            return _adddriverRepository.Query(u => u.DriverId== id).Select().FirstOrDefault();
        }


        public bool AddUpdateDeleteClass(AddDriver user, string action)
        {
            bool isSuccess = true;
            try
            {
                if (action == "I")
                {
                    Insert(user);
                }
                else if (action == "U")
                {

                    Update(user);
                }
                else if (action == "D")
                {
                    Delete(user);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }

        public bool ProfileUpdate(AddDriver cs, string action, int vid)
        {
            bool isSuccess = true;
            try
            {
                //  user.Password = Md5Encryption.Encrypt(user.Password);




                if (action == "I")
                {
                    Insert(cs);
                }
                else if (action == "U")
                {
                    Update(cs);
                }
                else if (action == "D")
                {
                    Delete(cs);
                }
                _unitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                isSuccess = false;
                throw ex;
            }
            return isSuccess;
        }


   


    }
}
