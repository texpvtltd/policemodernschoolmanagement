﻿using Entities.Models;
using Repository.RepositoryFactoryBase;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Repository.RepositoryModel
{
   public class LeaveDetailRepository :RepositoryFactory<LeaveDetail>
    {

        public LeaveDetailRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) { }


    }
}
