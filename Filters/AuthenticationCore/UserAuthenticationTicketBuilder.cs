﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
using Filters.AuthenticationModel;
using Entities.Models;

namespace Filters.AuthenticationCore
{
    public class UserAuthenticationTicketBuilder
    {
        public static UserInfo CreateUserContextFromUser(UserLogin user)
        {
            var userContext = new UserInfo
            {
                Id = user.UserId,
                UserName = user.Username,
                SchoolName = user.SchoolName,
               UserRight=user.UserRights,
                UserType = user.UserType,
               Session = user.Session
            };
            return userContext;
        }

        public static FormsAuthenticationTicket CreateAuthenticationTicket(UserLogin user)
        {
            UserInfo userInfo = CreateUserContextFromUser(user);
            var ticket = new FormsAuthenticationTicket(
                1,
                user.Username,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                false,
                userInfo.ToString());
            return ticket;
        }

    }
}
