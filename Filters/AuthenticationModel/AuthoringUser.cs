﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace Filters.AuthenticationModel
{
    [Serializable]
    public class AuthoringUser : IIdentity
    {
        public Int64 UserId { get; set; }
        public Int64 SchoolID { get; set; }
        public string Name { get; private set; }
        public string  SchoolName{ get; set; }
        public string UserName { get; set; }
      public string Session { get; set; }
        public string UserType { get; set; }
        public string UserRight { get; set; }
        public string AuthenticationType { get { return "PacSchoolManagement"; } }
        public bool IsAuthenticated { get { return true; } }
        public AuthoringUser() { }
        public AuthoringUser(string name, Int64 userId, Int64 schoolID, string SchoolName, string UserName, string Session, string UserType, string UserRight)
        {
            this.Name = name;
            this.UserId = userId;
            this.SchoolName =SchoolName;
            this.UserName =UserName;
            this.SchoolID = schoolID;
           this.Session = Session;
            this.UserType = UserType;
            this.UserRight=UserRight;
        }

        public AuthoringUser(string name, UserInfo userInfo)
            : this(name, userInfo.Id,userInfo.SchoolID, userInfo.SchoolName, userInfo.UserName, userInfo.Session, userInfo.UserType,userInfo.UserRight)
        {
            this.UserId = userInfo.Id;
        }
        public AuthoringUser(FormsAuthenticationTicket ticket)
            : this(ticket.Name, UserInfo.FromString(ticket.UserData))
        {
            if (ticket == null) throw new ArgumentNullException("ticket");
        }
    }
}
