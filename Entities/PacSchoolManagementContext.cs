using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Entities.Models.Mapping;

namespace Entities.Models
{
    public partial class PacSchoolManagementContext : DbContext
    {
        static PacSchoolManagementContext()
        {
            Database.SetInitializer<PacSchoolManagementContext>(null);
        }

        public PacSchoolManagementContext()
            : base("Name=PacSchoolManagementContext")
        {
        }

        public DbSet<academicsetting> academicsettings { get; set; }
        public DbSet<AddDestination_Fees> AddDestination_Fees { get; set; }
        public DbSet<AddDriver> AddDrivers { get; set; }
        public DbSet<AddHostalRoom> AddHostalRooms { get; set; }
        public DbSet<AddRoute> AddRoutes { get; set; }
        public DbSet<AddTransport> AddTransports { get; set; }
        public DbSet<AssignTeacher> AssignTeachers { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<AttendanceDetail> AttendanceDetails { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookCategory> BookCategories { get; set; }
        public DbSet<Calender> Calenders { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<HostalDetail> HostalDetails { get; set; }
        public DbSet<HostalType> HostalTypes { get; set; }
        public DbSet<HostelAllocation> HostelAllocations { get; set; }
        public DbSet<HostelFeesCollection> HostelFeesCollections { get; set; }
        public DbSet<HostelRegister> HostelRegisters { get; set; }
        public DbSet<HostelTransfer> HostelTransfers { get; set; }
        public DbSet<HostelVisitor> HostelVisitors { get; set; }
        public DbSet<House> Houses { get; set; }
        public DbSet<HQMenu> HQMenus { get; set; }
        public DbSet<HQUser> HQUsers { get; set; }
        public DbSet<IssueBook> IssueBooks { get; set; }
        public DbSet<LeaveCarryForwardDetail> LeaveCarryForwardDetails { get; set; }
        public DbSet<LeaveCategory> LeaveCategories { get; set; }
        public DbSet<LeaveDetail> LeaveDetails { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Principal> Principals { get; set; }
        public DbSet<Returnbook> Returnbooks { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<SchoolDetail> SchoolDetails { get; set; }
        public DbSet<SchoolMenu> SchoolMenus { get; set; }
        public DbSet<SchoolRole> SchoolRoles { get; set; }
        public DbSet<SchoolUser> SchoolUsers { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentAdmission> StudentAdmissions { get; set; }
        public DbSet<StudentDocument> StudentDocuments { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<subjectnew> subjectnews { get; set; }
        public DbSet<TimeTable> TimeTables { get; set; }
        public DbSet<TransportAllocation> TransportAllocations { get; set; }
        public DbSet<TransPortFeesCollection> TransPortFeesCollections { get; set; }
        public DbSet<TransprotFee> TransprotFees { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<Visitor> Visitors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new academicsettingMap());
            modelBuilder.Configurations.Add(new AddDestination_FeesMap());
            modelBuilder.Configurations.Add(new AddDriverMap());
            modelBuilder.Configurations.Add(new AddHostalRoomMap());
            modelBuilder.Configurations.Add(new AddRouteMap());
            modelBuilder.Configurations.Add(new AddTransportMap());
            modelBuilder.Configurations.Add(new AssignTeacherMap());
            modelBuilder.Configurations.Add(new AttendanceMap());
            modelBuilder.Configurations.Add(new AttendanceDetailMap());
            modelBuilder.Configurations.Add(new BatchMap());
            modelBuilder.Configurations.Add(new BookMap());
            modelBuilder.Configurations.Add(new BookCategoryMap());
            modelBuilder.Configurations.Add(new CalenderMap());
            modelBuilder.Configurations.Add(new ClassMap());
            modelBuilder.Configurations.Add(new CourseMap());
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new EmployeeMap());
            modelBuilder.Configurations.Add(new EventMap());
            modelBuilder.Configurations.Add(new HostalDetailMap());
            modelBuilder.Configurations.Add(new HostalTypeMap());
            modelBuilder.Configurations.Add(new HostelAllocationMap());
            modelBuilder.Configurations.Add(new HostelFeesCollectionMap());
            modelBuilder.Configurations.Add(new HostelRegisterMap());
            modelBuilder.Configurations.Add(new HostelTransferMap());
            modelBuilder.Configurations.Add(new HostelVisitorMap());
            modelBuilder.Configurations.Add(new HouseMap());
            modelBuilder.Configurations.Add(new HQMenuMap());
            modelBuilder.Configurations.Add(new HQUserMap());
            modelBuilder.Configurations.Add(new IssueBookMap());
            modelBuilder.Configurations.Add(new LeaveCarryForwardDetailMap());
            modelBuilder.Configurations.Add(new LeaveCategoryMap());
            modelBuilder.Configurations.Add(new LeaveDetailMap());
            modelBuilder.Configurations.Add(new MenuMap());
            modelBuilder.Configurations.Add(new PrincipalMap());
            modelBuilder.Configurations.Add(new ReturnbookMap());
            modelBuilder.Configurations.Add(new SchoolMap());
            modelBuilder.Configurations.Add(new SchoolDetailMap());
            modelBuilder.Configurations.Add(new SchoolMenuMap());
            modelBuilder.Configurations.Add(new SchoolRoleMap());
            modelBuilder.Configurations.Add(new SchoolUserMap());
            modelBuilder.Configurations.Add(new SectionMap());
            modelBuilder.Configurations.Add(new SessionMap());
            modelBuilder.Configurations.Add(new StudentMap());
            modelBuilder.Configurations.Add(new StudentAdmissionMap());
            modelBuilder.Configurations.Add(new StudentDocumentMap());
            modelBuilder.Configurations.Add(new SubjectMap());
            modelBuilder.Configurations.Add(new subjectnewMap());
            modelBuilder.Configurations.Add(new TimeTableMap());
            modelBuilder.Configurations.Add(new TransportAllocationMap());
            modelBuilder.Configurations.Add(new TransPortFeesCollectionMap());
            modelBuilder.Configurations.Add(new TransprotFeeMap());
            modelBuilder.Configurations.Add(new UserLoginMap());
            modelBuilder.Configurations.Add(new UserTypeMap());
            modelBuilder.Configurations.Add(new VisitorMap());
        }
    }
}
