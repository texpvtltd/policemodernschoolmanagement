using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AttendanceDetail
    {
        public int AttDetailID { get; set; }
        public int SchoolID { get; set; }
        public Nullable<int> StudentID { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public Nullable<int> AttendanceID { get; set; }
        public string IsDelete { get; set; }
    }
}
