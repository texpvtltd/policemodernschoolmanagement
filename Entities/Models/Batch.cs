using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Batch
    {
        public int BatchId { get; set; }
        public string BatchName { get; set; }
        public Nullable<int> CourseId { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> SessionId { get; set; }
        public string IsDelete { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MaximumStudents { get; set; }
    }
}
