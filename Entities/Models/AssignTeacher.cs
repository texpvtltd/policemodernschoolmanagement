using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AssignTeacher
    {
        public int AssignTeacherId { get; set; }
        public Nullable<int> TeacherId { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public Nullable<int> CourseId { get; set; }
        public Nullable<int> BatchId { get; set; }
        public Nullable<int> ClassId { get; set; }
        public string IsDelete { get; set; }
        public string IsClassTeacher { get; set; }
        public Nullable<int> SubjectId { get; set; }
    }
}
