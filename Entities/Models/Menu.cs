using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Menu
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public string MenuClass { get; set; }
        public string Link { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string TokenKey { get; set; }
        public string IsDelete { get; set; }
    }
}
