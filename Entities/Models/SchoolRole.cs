using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class SchoolRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> Schoolid { get; set; }
        public string IsDelete { get; set; }
    }
}
