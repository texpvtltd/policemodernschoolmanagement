using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class IssueBook
    {
        public int BookIssuedID { get; set; }
        public string IssuedDate { get; set; }
        public string DueDate { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string UserCourse { get; set; }
        public string BookName { get; set; }
        public string BookNo { get; set; }
        public string BooKTitle { get; set; }
        public string UserSection { get; set; }
        public string SchoolID { get; set; }
        public string UserID { get; set; }
        public string IsDelete { get; set; }
    }
}
