using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostalType
    {
        public int HostalTypeID { get; set; }
        public string HostalType1 { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
