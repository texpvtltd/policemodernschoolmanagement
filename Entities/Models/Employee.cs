using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Employee
    {
        public int EmpId { get; set; }
        public string EmployeeSerialNo { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> DeptId { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string Joiningdate { get; set; }
        public string Address { get; set; }
        public string Salary { get; set; }
        public string Photo { get; set; }
        public string AdhaarCardNo { get; set; }
        public string PanCardNo { get; set; }
        public string DateofBirth { get; set; }
        public string HRA { get; set; }
        public string DA { get; set; }
        public string TA { get; set; }
        public string PaidLeaves { get; set; }
        public string Leavestaken { get; set; }
        public string Employeetype { get; set; }
        public string Employeecategory { get; set; }
        public int EmployeeID { get; set; }
    }
}
