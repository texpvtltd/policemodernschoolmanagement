using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostelRegister
    {
        public int HostelRegID { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string UStatus { get; set; }
        public string HosDate { get; set; }
        public string HosTime { get; set; }
        public string IsDelete { get; set; }
        public string SchoolID { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string RoomNo { get; set; }
        public string FloorName { get; set; }
    }
}
