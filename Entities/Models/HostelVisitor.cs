using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostelVisitor
    {
        public int VisitorId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string VisitDate { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
        public string VisitorName { get; set; }
        public string VisitTime { get; set; }
        public string Relation { get; set; }
        public string HostelName { get; set; }
        public string Hosteltype { get; set; }
    }
}
