using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostelTransfer
    {
        public int TransferId { get; set; }
        public string UsesType { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string HostelRoom { get; set; }
        public string IsDelete { get; set; }
        public string SchoolID { get; set; }
    }
}
