using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class subjectnew
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> SectionID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
