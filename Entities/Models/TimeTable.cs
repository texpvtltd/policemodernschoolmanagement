using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class TimeTable
    {
        public int TimeTableID { get; set; }
        public int ClassID { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<int> Session { get; set; }
        public int SchoolID { get; set; }
        public string Day { get; set; }
        public Nullable<int> SubjectID { get; set; }
        public string Time { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public string IsDelete { get; set; }
    }
}
