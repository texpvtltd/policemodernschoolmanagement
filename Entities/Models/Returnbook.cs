using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Returnbook
    {
        public int ReturnBookID { get; set; }
        public string BookNo { get; set; }
        public string BookName { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Remark { get; set; }
        public string ReturnDate { get; set; }
        public string FineAmount { get; set; }
        public string IsDelete { get; set; }
        public string UserClass { get; set; }
        public string SchoolId { get; set; }
        public string Section { get; set; }
        public string BookofId { get; set; }
    }
}
