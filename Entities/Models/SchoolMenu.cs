using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class SchoolMenu
    {
        public int MenuId { get; set; }
        public string MenuName { get; set; }
        public string MenuClass { get; set; }
        public string Link { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string TokenKey { get; set; }
        public string IsDelete { get; set; }
    }
}
