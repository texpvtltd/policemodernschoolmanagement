using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Department
    {
        public int DeptId { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
