using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Class
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public int SectionID { get; set; }
        public int SchoolID { get; set; }
        public Nullable<int> ClassTeacherID { get; set; }
        public string IsDelete { get; set; }
        public string DepartmentId { get; set; }
    }
}
