using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class House
    {
        public int HouseID { get; set; }
        public string HouseName { get; set; }
        public string HouseColor { get; set; }
        public int SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
