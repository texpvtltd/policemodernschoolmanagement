using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class LeaveCarryForwardDetail
    {
        public int LeaveCarryForwordId { get; set; }
        public string LeaveCategory { get; set; }
        public string Desitnation { get; set; }
        public string Employee { get; set; }
        public string LeaveCount { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
