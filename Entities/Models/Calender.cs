using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Calender
    {
        public int EventID { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Start { get; set; }
        public Nullable<System.DateTime> Enddate { get; set; }
        public string ThemeColor { get; set; }
        public string Type { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public Nullable<int> Session { get; set; }
    }
}
