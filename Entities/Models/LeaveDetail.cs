using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class LeaveDetail
    {
        public int LeaveDetailId { get; set; }
        public string LeaveCategory { get; set; }
        public string Degitnation { get; set; }
        public string LeaveCount { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
