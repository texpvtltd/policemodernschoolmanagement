using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Student
    {
        public int StudentID { get; set; }
        public string RegdNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<int> Session { get; set; }
        public int SchoolID { get; set; }
        public string SudentImage { get; set; }
        public string Attendance { get; set; }
        public string Grade { get; set; }
        public string Marksobtained { get; set; }
        public Nullable<int> UserID { get; set; }
        public string IsDelete { get; set; }
    }
}
