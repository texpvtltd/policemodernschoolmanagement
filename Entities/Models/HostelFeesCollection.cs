using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostelFeesCollection
    {
        public int FeesId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string RoomNo { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string FeeType { get; set; }
        public string ModeOfPay { get; set; }
        public string Amount { get; set; }
        public string Fine { get; set; }
        public string Discount { get; set; }
        public string TotalAmount { get; set; }
        public string Recipt { get; set; }
        public string ReciptNo { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string CheaqueDate { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
