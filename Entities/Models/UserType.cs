using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class UserType
    {
        public int UserTypeID { get; set; }
        public string UserType1 { get; set; }
        public string IsDelete { get; set; }
    }
}
