using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class LeaveCategory
    {
        public int LeaveCatId { get; set; }
        public string CategoryName { get; set; }
        public string CarryStatus { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
