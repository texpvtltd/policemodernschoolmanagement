using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HQUser
    {
        public int HQUserID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public Nullable<int> UserID { get; set; }
        public Nullable<int> UserType { get; set; }
        public string UserImage { get; set; }
        public string Contact { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string IsDelete { get; set; }
    }
}
