using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Attendance
    {
        public int AttendanceID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public Nullable<int> CourseID { get; set; }
        public string Date { get; set; }
        public Nullable<int> SessionID { get; set; }
        public string IsDelete { get; set; }
    }
}
