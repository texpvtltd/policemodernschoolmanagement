using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AddRoute
    {
        public int RouteID { get; set; }
        public string VehicleNo { get; set; }
        public string RouteCode { get; set; }
        public string RouteStartPlace { get; set; }
        public string RouteStopPlace { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
