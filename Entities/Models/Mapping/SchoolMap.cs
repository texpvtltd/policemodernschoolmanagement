using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class SchoolMap : EntityTypeConfiguration<School>
    {
        public SchoolMap()
        {
            // Primary Key
            this.HasKey(t => t.SchoolID);

            // Properties
            this.Property(t => t.SchoolName)
                .IsRequired();

            this.Property(t => t.PinCode)
                .HasMaxLength(10);

            this.Property(t => t.ContactNo)
                .HasMaxLength(15);

            this.Property(t => t.Logo)
                .HasMaxLength(50);

            this.Property(t => t.SchoolCode)
                .HasMaxLength(20);

            this.Property(t => t.City)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("School");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.SchoolName).HasColumnName("SchoolName");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.PinCode).HasColumnName("PinCode");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.Logo).HasColumnName("Logo");
            this.Property(t => t.SchoolCode).HasColumnName("SchoolCode");
            this.Property(t => t.SessionID).HasColumnName("SessionID");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
