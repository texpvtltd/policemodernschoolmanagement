using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class BookCategoryMap : EntityTypeConfiguration<BookCategory>
    {
        public BookCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.BookCategoryID);

            // Properties
            this.Property(t => t.SchoolID)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("BookCategory");
            this.Property(t => t.BookCategoryID).HasColumnName("BookCategoryID");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.SectionCode).HasColumnName("SectionCode");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
