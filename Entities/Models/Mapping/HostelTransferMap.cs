using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostelTransferMap : EntityTypeConfiguration<HostelTransfer>
    {
        public HostelTransferMap()
        {
            // Primary Key
            this.HasKey(t => t.TransferId);

            // Properties
            this.Property(t => t.UsesType)
                .HasMaxLength(50);

            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.Status)
                .HasMaxLength(50);

            this.Property(t => t.HostelType)
                .HasMaxLength(50);

            this.Property(t => t.HostelName)
                .HasMaxLength(50);

            this.Property(t => t.HostelRoom)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolID)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostelTransfer");
            this.Property(t => t.TransferId).HasColumnName("TransferId");
            this.Property(t => t.UsesType).HasColumnName("UsesType");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.HostelType).HasColumnName("HostelType");
            this.Property(t => t.HostelName).HasColumnName("HostelName");
            this.Property(t => t.HostelRoom).HasColumnName("HostelRoom");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
        }
    }
}
