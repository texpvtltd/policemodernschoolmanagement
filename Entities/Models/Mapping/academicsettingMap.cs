using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class academicsettingMap : EntityTypeConfiguration<academicsetting>
    {
        public academicsettingMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Startdate)
                .HasMaxLength(50);

            this.Property(t => t.Sessionenddate)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("academicsetting");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.Startdate).HasColumnName("Startdate");
            this.Property(t => t.Sessionenddate).HasColumnName("Sessionenddate");
        }
    }
}
