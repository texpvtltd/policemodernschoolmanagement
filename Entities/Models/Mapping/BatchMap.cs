using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class BatchMap : EntityTypeConfiguration<Batch>
    {
        public BatchMap()
        {
            // Primary Key
            this.HasKey(t => t.BatchId);

            // Properties
            this.Property(t => t.BatchName)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.StartDate)
                .HasMaxLength(50);

            this.Property(t => t.EndDate)
                .HasMaxLength(50);

            this.Property(t => t.MaximumStudents)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Batch");
            this.Property(t => t.BatchId).HasColumnName("BatchId");
            this.Property(t => t.BatchName).HasColumnName("BatchName");
            this.Property(t => t.CourseId).HasColumnName("CourseId");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.MaximumStudents).HasColumnName("MaximumStudents");
        }
    }
}
