using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostelAllocationMap : EntityTypeConfiguration<HostelAllocation>
    {
        public HostelAllocationMap()
        {
            // Primary Key
            this.HasKey(t => t.HostelAllocatedId);

            // Properties
            this.Property(t => t.UserType)
                .HasMaxLength(50);

            this.Property(t => t.UsarName)
                .HasMaxLength(50);

            this.Property(t => t.HostelName)
                .HasMaxLength(50);

            this.Property(t => t.HostalType)
                .HasMaxLength(50);

            this.Property(t => t.HostelRegdate)
                .HasMaxLength(50);

            this.Property(t => t.VacatingDate)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostelAllocation");
            this.Property(t => t.HostelAllocatedId).HasColumnName("HostelAllocatedId");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.UsarName).HasColumnName("UsarName");
            this.Property(t => t.HostelName).HasColumnName("HostelName");
            this.Property(t => t.HostalType).HasColumnName("HostalType");
            this.Property(t => t.HostelRoom).HasColumnName("HostelRoom");
            this.Property(t => t.HostelRegdate).HasColumnName("HostelRegdate");
            this.Property(t => t.VacatingDate).HasColumnName("VacatingDate");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
