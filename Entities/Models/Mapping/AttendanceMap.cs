using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AttendanceMap : EntityTypeConfiguration<Attendance>
    {
        public AttendanceMap()
        {
            // Primary Key
            this.HasKey(t => t.AttendanceID);

            // Properties
            this.Property(t => t.Date)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Attendance");
            this.Property(t => t.AttendanceID).HasColumnName("AttendanceID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.ClassID).HasColumnName("ClassID");
            this.Property(t => t.SectionId).HasColumnName("SectionId");
            this.Property(t => t.TeacherID).HasColumnName("TeacherID");
            this.Property(t => t.CourseID).HasColumnName("CourseID");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.SessionID).HasColumnName("SessionID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
