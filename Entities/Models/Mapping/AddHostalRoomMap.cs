using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AddHostalRoomMap : EntityTypeConfiguration<AddHostalRoom>
    {
        public AddHostalRoomMap()
        {
            // Primary Key
            this.HasKey(t => t.RoomId);

            // Properties
            this.Property(t => t.HostalfloorName)
                .HasMaxLength(50);

            this.Property(t => t.RoomNo)
                .HasMaxLength(50);

            this.Property(t => t.NoOfBed)
                .HasMaxLength(50);

            this.Property(t => t.FeeType)
                .HasMaxLength(50);

            this.Property(t => t.BedNo)
                .HasMaxLength(50);

            this.Property(t => t.Amount)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AddHostalRoom");
            this.Property(t => t.RoomId).HasColumnName("RoomId");
            this.Property(t => t.HostalType).HasColumnName("HostalType");
            this.Property(t => t.HostalName).HasColumnName("HostalName");
            this.Property(t => t.HostalfloorName).HasColumnName("HostalfloorName");
            this.Property(t => t.RoomNo).HasColumnName("RoomNo");
            this.Property(t => t.NoOfBed).HasColumnName("NoOfBed");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.BedNo).HasColumnName("BedNo");
            this.Property(t => t.RoomRent).HasColumnName("RoomRent");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
