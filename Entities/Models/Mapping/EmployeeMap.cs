using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class EmployeeMap : EntityTypeConfiguration<Employee>
    {
        public EmployeeMap()
        {
            // Primary Key
            this.HasKey(t => t.EmployeeID);

            // Properties
            this.Property(t => t.EmployeeSerialNo)
                .HasMaxLength(50);

            this.Property(t => t.Firstname)
                .HasMaxLength(50);

            this.Property(t => t.Lastname)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Contact)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(10);

            this.Property(t => t.Joiningdate)
                .HasMaxLength(50);

            this.Property(t => t.Salary)
                .HasMaxLength(10);

            this.Property(t => t.AdhaarCardNo)
                .HasMaxLength(20);

            this.Property(t => t.PanCardNo)
                .HasMaxLength(20);

            this.Property(t => t.DateofBirth)
                .HasMaxLength(20);

            this.Property(t => t.HRA)
                .HasMaxLength(50);

            this.Property(t => t.DA)
                .HasMaxLength(50);

            this.Property(t => t.TA)
                .HasMaxLength(50);

            this.Property(t => t.PaidLeaves)
                .HasMaxLength(10);

            this.Property(t => t.Leavestaken)
                .HasMaxLength(10);

            this.Property(t => t.Employeetype)
                .HasMaxLength(20);

            this.Property(t => t.Employeecategory)
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("Employee");
            this.Property(t => t.EmpId).HasColumnName("EmpId");
            this.Property(t => t.EmployeeSerialNo).HasColumnName("EmployeeSerialNo");
            this.Property(t => t.Firstname).HasColumnName("Firstname");
            this.Property(t => t.Lastname).HasColumnName("Lastname");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.DeptId).HasColumnName("DeptId");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.Joiningdate).HasColumnName("Joiningdate");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Salary).HasColumnName("Salary");
            this.Property(t => t.Photo).HasColumnName("Photo");
            this.Property(t => t.AdhaarCardNo).HasColumnName("AdhaarCardNo");
            this.Property(t => t.PanCardNo).HasColumnName("PanCardNo");
            this.Property(t => t.DateofBirth).HasColumnName("DateofBirth");
            this.Property(t => t.HRA).HasColumnName("HRA");
            this.Property(t => t.DA).HasColumnName("DA");
            this.Property(t => t.TA).HasColumnName("TA");
            this.Property(t => t.PaidLeaves).HasColumnName("PaidLeaves");
            this.Property(t => t.Leavestaken).HasColumnName("Leavestaken");
            this.Property(t => t.Employeetype).HasColumnName("Employeetype");
            this.Property(t => t.Employeecategory).HasColumnName("Employeecategory");
            this.Property(t => t.EmployeeID).HasColumnName("EmployeeID");
        }
    }
}
