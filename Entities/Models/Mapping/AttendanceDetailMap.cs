using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AttendanceDetailMap : EntityTypeConfiguration<AttendanceDetail>
    {
        public AttendanceDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.AttDetailID);

            // Properties
            this.Property(t => t.Date)
                .HasMaxLength(20);

            this.Property(t => t.Status)
                .HasMaxLength(5);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AttendanceDetail");
            this.Property(t => t.AttDetailID).HasColumnName("AttDetailID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.StudentID).HasColumnName("StudentID");
            this.Property(t => t.Date).HasColumnName("Date");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.AttendanceID).HasColumnName("AttendanceID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
