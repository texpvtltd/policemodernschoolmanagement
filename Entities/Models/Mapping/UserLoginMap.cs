using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class UserLoginMap : EntityTypeConfiguration<UserLogin>
    {
        public UserLoginMap()
        {
            // Primary Key
            this.HasKey(t => t.UserId);

            // Properties
            this.Property(t => t.Username)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.Password)
                .IsRequired()
                .HasMaxLength(30);

            this.Property(t => t.UserType)
                .IsRequired()
                .HasMaxLength(15);

            this.Property(t => t.TokenId)
                .HasMaxLength(50);

            this.Property(t => t.SchoolName)
                .HasMaxLength(50);

            this.Property(t => t.Session)
                .HasMaxLength(10);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.IsConfirm)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UserLogin");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Username).HasColumnName("Username");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.TokenId).HasColumnName("TokenId");
            this.Property(t => t.UserRights).HasColumnName("UserRights");
            this.Property(t => t.SchoolName).HasColumnName("SchoolName");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.Session).HasColumnName("Session");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.IsConfirm).HasColumnName("IsConfirm");
        }
    }
}
