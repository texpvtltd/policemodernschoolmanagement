using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AddDriverMap : EntityTypeConfiguration<AddDriver>
    {
        public AddDriverMap()
        {
            // Primary Key
            this.HasKey(t => t.DriverId);

            // Properties
            this.Property(t => t.LicenseNumber)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AddDriver");
            this.Property(t => t.DriverId).HasColumnName("DriverId");
            this.Property(t => t.VehicleNo).HasColumnName("VehicleNo");
            this.Property(t => t.DriverName).HasColumnName("DriverName");
            this.Property(t => t.PresentAddress).HasColumnName("PresentAddress");
            this.Property(t => t.PermanentAddress).HasColumnName("PermanentAddress");
            this.Property(t => t.DateofBirth).HasColumnName("DateofBirth");
            this.Property(t => t.ContactNo).HasColumnName("ContactNo");
            this.Property(t => t.LicenseNumber).HasColumnName("LicenseNumber");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
