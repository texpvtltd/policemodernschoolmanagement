using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HouseMap : EntityTypeConfiguration<House>
    {
        public HouseMap()
        {
            // Primary Key
            this.HasKey(t => t.HouseID);

            // Properties
            this.Property(t => t.HouseName)
                .HasMaxLength(50);

            this.Property(t => t.HouseColor)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("House");
            this.Property(t => t.HouseID).HasColumnName("HouseID");
            this.Property(t => t.HouseName).HasColumnName("HouseName");
            this.Property(t => t.HouseColor).HasColumnName("HouseColor");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
