using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AddDestination_FeesMap : EntityTypeConfiguration<AddDestination_Fees>
    {
        public AddDestination_FeesMap()
        {
            // Primary Key
            this.HasKey(t => t.DestinationId);

            // Properties
            // Table & Column Mappings
            this.ToTable("AddDestination&Fees");
            this.Property(t => t.DestinationId).HasColumnName("DestinationId");
            this.Property(t => t.RouteCode).HasColumnName("RouteCode");
            this.Property(t => t.PickupDrop).HasColumnName("PickupDrop");
            this.Property(t => t.StopTime).HasColumnName("StopTime");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
