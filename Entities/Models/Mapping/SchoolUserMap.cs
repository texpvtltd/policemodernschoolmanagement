using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class SchoolUserMap : EntityTypeConfiguration<SchoolUser>
    {
        public SchoolUserMap()
        {
            // Primary Key
            this.HasKey(t => t.SchoolUserID);

            // Properties
            this.Property(t => t.FName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.LName)
                .HasMaxLength(50);

            this.Property(t => t.UserImage)
                .HasMaxLength(50);

            this.Property(t => t.AdhaarImage)
                .HasMaxLength(50);

            this.Property(t => t.UserType)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SchoolUser");
            this.Property(t => t.SchoolUserID).HasColumnName("SchoolUserID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.FName).HasColumnName("FName");
            this.Property(t => t.LName).HasColumnName("LName");
            this.Property(t => t.UserImage).HasColumnName("UserImage");
            this.Property(t => t.AdhaarImage).HasColumnName("AdhaarImage");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.PassWord).HasColumnName("PassWord");
        }
    }
}
