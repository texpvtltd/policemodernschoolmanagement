using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class subjectnewMap : EntityTypeConfiguration<subjectnew>
    {
        public subjectnewMap()
        {
            // Primary Key
            this.HasKey(t => t.SubjectID);

            // Properties
            this.Property(t => t.SubjectName)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("subjectnew");
            this.Property(t => t.SubjectID).HasColumnName("SubjectID");
            this.Property(t => t.SubjectName).HasColumnName("SubjectName");
            this.Property(t => t.TeacherID).HasColumnName("TeacherID");
            this.Property(t => t.ClassID).HasColumnName("ClassID");
            this.Property(t => t.SectionID).HasColumnName("SectionID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
