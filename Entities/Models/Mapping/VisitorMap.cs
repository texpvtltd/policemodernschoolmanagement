using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class VisitorMap : EntityTypeConfiguration<Visitor>
    {
        public VisitorMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.TokenNo)
                .HasMaxLength(10);

            this.Property(t => t.Category)
                .HasMaxLength(10);

            this.Property(t => t.Name)
                .HasMaxLength(20);

            this.Property(t => t.Purpose)
                .HasMaxLength(50);

            this.Property(t => t.MobileNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Visitor");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.TokenNo).HasColumnName("TokenNo");
            this.Property(t => t.Category).HasColumnName("Category");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Purpose).HasColumnName("Purpose");
            this.Property(t => t.MobileNo).HasColumnName("MobileNo");
            this.Property(t => t.Remarks).HasColumnName("Remarks");
        }
    }
}
