using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class StudentAdmissionMap : EntityTypeConfiguration<StudentAdmission>
    {
        public StudentAdmissionMap()
        {
            // Primary Key
            this.HasKey(t => t.AddmissionID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.Gender)
                .HasMaxLength(10);

            this.Property(t => t.DateofBirth)
                .HasMaxLength(20);

            this.Property(t => t.FatherName)
                .HasMaxLength(50);

            this.Property(t => t.MobileNo)
                .HasMaxLength(15);

            this.Property(t => t.AppliedDate)
                .HasMaxLength(20);

            this.Property(t => t.Status)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("StudentAdmission");
            this.Property(t => t.AddmissionID).HasColumnName("AddmissionID");
            this.Property(t => t.RegdNo).HasColumnName("RegdNo");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.DateofBirth).HasColumnName("DateofBirth");
            this.Property(t => t.Class).HasColumnName("Class");
            this.Property(t => t.FatherName).HasColumnName("FatherName");
            this.Property(t => t.MobileNo).HasColumnName("MobileNo");
            this.Property(t => t.AppliedDate).HasColumnName("AppliedDate");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
