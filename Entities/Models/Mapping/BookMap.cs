using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class BookMap : EntityTypeConfiguration<Book>
    {
        public BookMap()
        {
            // Primary Key
            this.HasKey(t => t.BookID);

            // Properties
            this.Property(t => t.NoofCopies)
                .HasMaxLength(50);

            this.Property(t => t.ShelfNo)
                .HasMaxLength(50);

            this.Property(t => t.BookPosition)
                .HasMaxLength(50);

            this.Property(t => t.Language)
                .HasMaxLength(50);

            this.Property(t => t.BookCondition)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Book");
            this.Property(t => t.BookID).HasColumnName("BookID");
            this.Property(t => t.PurchaseDate).HasColumnName("PurchaseDate");
            this.Property(t => t.BillNo).HasColumnName("BillNo");
            this.Property(t => t.BookISBNNo).HasColumnName("BookISBNNo");
            this.Property(t => t.BookNo).HasColumnName("BookNo");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Author).HasColumnName("Author");
            this.Property(t => t.Edition).HasColumnName("Edition");
            this.Property(t => t.BookCategory).HasColumnName("BookCategory");
            this.Property(t => t.Publisher).HasColumnName("Publisher");
            this.Property(t => t.NoofCopies).HasColumnName("NoofCopies");
            this.Property(t => t.ShelfNo).HasColumnName("ShelfNo");
            this.Property(t => t.BookPosition).HasColumnName("BookPosition");
            this.Property(t => t.BookCost).HasColumnName("BookCost");
            this.Property(t => t.Language).HasColumnName("Language");
            this.Property(t => t.BookCondition).HasColumnName("BookCondition");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
