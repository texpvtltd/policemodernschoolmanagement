using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AddRouteMap : EntityTypeConfiguration<AddRoute>
    {
        public AddRouteMap()
        {
            // Primary Key
            this.HasKey(t => t.RouteID);

            // Properties
            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AddRoute");
            this.Property(t => t.RouteID).HasColumnName("RouteID");
            this.Property(t => t.VehicleNo).HasColumnName("VehicleNo");
            this.Property(t => t.RouteCode).HasColumnName("RouteCode");
            this.Property(t => t.RouteStartPlace).HasColumnName("RouteStartPlace");
            this.Property(t => t.RouteStopPlace).HasColumnName("RouteStopPlace");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
