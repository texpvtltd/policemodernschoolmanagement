using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class SchoolRoleMap : EntityTypeConfiguration<SchoolRole>
    {
        public SchoolRoleMap()
        {
            // Primary Key
            this.HasKey(t => t.RoleId);

            // Properties
            this.Property(t => t.RoleName)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("SchoolRole");
            this.Property(t => t.RoleId).HasColumnName("RoleId");
            this.Property(t => t.RoleName).HasColumnName("RoleName");
            this.Property(t => t.Schoolid).HasColumnName("Schoolid");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
