using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class ReturnbookMap : EntityTypeConfiguration<Returnbook>
    {
        public ReturnbookMap()
        {
            // Primary Key
            this.HasKey(t => t.ReturnBookID);

            // Properties
            this.Property(t => t.Status)
                .HasMaxLength(50);

            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.UserId)
                .HasMaxLength(50);

            this.Property(t => t.ReturnDate)
                .HasMaxLength(50);

            this.Property(t => t.FineAmount)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.UserClass)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.Section)
                .HasMaxLength(50);

            this.Property(t => t.BookofId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Returnbook");
            this.Property(t => t.ReturnBookID).HasColumnName("ReturnBookID");
            this.Property(t => t.BookNo).HasColumnName("BookNo");
            this.Property(t => t.BookName).HasColumnName("BookName");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.Remark).HasColumnName("Remark");
            this.Property(t => t.ReturnDate).HasColumnName("ReturnDate");
            this.Property(t => t.FineAmount).HasColumnName("FineAmount");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.UserClass).HasColumnName("UserClass");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.BookofId).HasColumnName("BookofId");
        }
    }
}
