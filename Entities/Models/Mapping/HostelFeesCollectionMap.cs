using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostelFeesCollectionMap : EntityTypeConfiguration<HostelFeesCollection>
    {
        public HostelFeesCollectionMap()
        {
            // Primary Key
            this.HasKey(t => t.FeesId);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.UserType)
                .HasMaxLength(50);

            this.Property(t => t.RoomNo)
                .HasMaxLength(50);

            this.Property(t => t.HostelType)
                .HasMaxLength(50);

            this.Property(t => t.HostelName)
                .HasMaxLength(50);

            this.Property(t => t.FeeType)
                .HasMaxLength(50);

            this.Property(t => t.ModeOfPay)
                .HasMaxLength(50);

            this.Property(t => t.Amount)
                .HasMaxLength(50);

            this.Property(t => t.Fine)
                .HasMaxLength(50);

            this.Property(t => t.Discount)
                .HasMaxLength(50);

            this.Property(t => t.TotalAmount)
                .HasMaxLength(50);

            this.Property(t => t.Recipt)
                .HasMaxLength(50);

            this.Property(t => t.ReciptNo)
                .HasMaxLength(50);

            this.Property(t => t.BankAccountNo)
                .HasMaxLength(50);

            this.Property(t => t.BankName)
                .HasMaxLength(50);

            this.Property(t => t.ChequeNo)
                .HasMaxLength(50);

            this.Property(t => t.CheaqueDate)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostelFeesCollection");
            this.Property(t => t.FeesId).HasColumnName("FeesId");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.RoomNo).HasColumnName("RoomNo");
            this.Property(t => t.HostelType).HasColumnName("HostelType");
            this.Property(t => t.HostelName).HasColumnName("HostelName");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.ModeOfPay).HasColumnName("ModeOfPay");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Fine).HasColumnName("Fine");
            this.Property(t => t.Discount).HasColumnName("Discount");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.Recipt).HasColumnName("Recipt");
            this.Property(t => t.ReciptNo).HasColumnName("ReciptNo");
            this.Property(t => t.BankAccountNo).HasColumnName("BankAccountNo");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.ChequeNo).HasColumnName("ChequeNo");
            this.Property(t => t.CheaqueDate).HasColumnName("CheaqueDate");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
