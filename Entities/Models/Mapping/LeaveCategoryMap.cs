using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class LeaveCategoryMap : EntityTypeConfiguration<LeaveCategory>
    {
        public LeaveCategoryMap()
        {
            // Primary Key
            this.HasKey(t => t.LeaveCatId);

            // Properties
            this.Property(t => t.CategoryName)
                .HasMaxLength(50);

            this.Property(t => t.CarryStatus)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LeaveCategory");
            this.Property(t => t.LeaveCatId).HasColumnName("LeaveCatId");
            this.Property(t => t.CategoryName).HasColumnName("CategoryName");
            this.Property(t => t.CarryStatus).HasColumnName("CarryStatus");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
