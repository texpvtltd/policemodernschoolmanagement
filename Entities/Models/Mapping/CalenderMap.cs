using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class CalenderMap : EntityTypeConfiguration<Calender>
    {
        public CalenderMap()
        {
            // Primary Key
            this.HasKey(t => t.EventID);

            // Properties
            this.Property(t => t.Subject)
                .HasMaxLength(50);

            this.Property(t => t.ThemeColor)
                .HasMaxLength(50);

            this.Property(t => t.Type)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Calender");
            this.Property(t => t.EventID).HasColumnName("EventID");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Start).HasColumnName("Start");
            this.Property(t => t.Enddate).HasColumnName("Enddate");
            this.Property(t => t.ThemeColor).HasColumnName("ThemeColor");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.Session).HasColumnName("Session");
        }
    }
}
