using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class LeaveDetailMap : EntityTypeConfiguration<LeaveDetail>
    {
        public LeaveDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.LeaveDetailId);

            // Properties
            this.Property(t => t.LeaveCategory)
                .HasMaxLength(50);

            this.Property(t => t.Degitnation)
                .HasMaxLength(50);

            this.Property(t => t.LeaveCount)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LeaveDetail");
            this.Property(t => t.LeaveDetailId).HasColumnName("LeaveDetailId");
            this.Property(t => t.LeaveCategory).HasColumnName("LeaveCategory");
            this.Property(t => t.Degitnation).HasColumnName("Degitnation");
            this.Property(t => t.LeaveCount).HasColumnName("LeaveCount");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
