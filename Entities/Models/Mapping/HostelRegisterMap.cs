using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostelRegisterMap : EntityTypeConfiguration<HostelRegister>
    {
        public HostelRegisterMap()
        {
            // Primary Key
            this.HasKey(t => t.HostelRegID);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.UStatus)
                .HasMaxLength(50);

            this.Property(t => t.HosDate)
                .HasMaxLength(50);

            this.Property(t => t.HosTime)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.HostelType)
                .HasMaxLength(50);

            this.Property(t => t.HostelName)
                .HasMaxLength(50);

            this.Property(t => t.RoomNo)
                .HasMaxLength(50);

            this.Property(t => t.FloorName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostelRegister");
            this.Property(t => t.HostelRegID).HasColumnName("HostelRegID");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UStatus).HasColumnName("UStatus");
            this.Property(t => t.HosDate).HasColumnName("HosDate");
            this.Property(t => t.HosTime).HasColumnName("HosTime");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.HostelType).HasColumnName("HostelType");
            this.Property(t => t.HostelName).HasColumnName("HostelName");
            this.Property(t => t.RoomNo).HasColumnName("RoomNo");
            this.Property(t => t.FloorName).HasColumnName("FloorName");
        }
    }
}
