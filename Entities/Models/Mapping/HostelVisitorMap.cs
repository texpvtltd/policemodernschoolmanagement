using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostelVisitorMap : EntityTypeConfiguration<HostelVisitor>
    {
        public HostelVisitorMap()
        {
            // Primary Key
            this.HasKey(t => t.VisitorId);

            // Properties
            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.VisitorName)
                .HasMaxLength(50);

            this.Property(t => t.VisitTime)
                .HasMaxLength(50);

            this.Property(t => t.HostelName)
                .HasMaxLength(50);

            this.Property(t => t.Hosteltype)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostelVisitor");
            this.Property(t => t.VisitorId).HasColumnName("VisitorId");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.VisitDate).HasColumnName("VisitDate");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.VisitorName).HasColumnName("VisitorName");
            this.Property(t => t.VisitTime).HasColumnName("VisitTime");
            this.Property(t => t.Relation).HasColumnName("Relation");
            this.Property(t => t.HostelName).HasColumnName("HostelName");
            this.Property(t => t.Hosteltype).HasColumnName("Hosteltype");
        }
    }
}
