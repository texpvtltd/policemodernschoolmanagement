using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostalTypeMap : EntityTypeConfiguration<HostalType>
    {
        public HostalTypeMap()
        {
            // Primary Key
            this.HasKey(t => t.HostalTypeID);

            // Properties
            this.Property(t => t.HostalType1)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostalType");
            this.Property(t => t.HostalTypeID).HasColumnName("HostalTypeID");
            this.Property(t => t.HostalType1).HasColumnName("HostalType");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
