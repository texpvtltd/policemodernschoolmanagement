using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class CourseMap : EntityTypeConfiguration<Course>
    {
        public CourseMap()
        {
            // Primary Key
            this.HasKey(t => t.CourseId);

            // Properties
            this.Property(t => t.SubId)
                .HasMaxLength(5);

            this.Property(t => t.ClassId)
                .HasMaxLength(5);

            this.Property(t => t.SessionId)
                .HasMaxLength(5);

            this.Property(t => t.SchoolId)
                .HasMaxLength(5);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.Code)
                .HasMaxLength(50);

            this.Property(t => t.MinAttendance)
                .HasMaxLength(50);

            this.Property(t => t.Attendancetype)
                .HasMaxLength(50);

            this.Property(t => t.Totalworkingdays)
                .HasMaxLength(50);

            this.Property(t => t.SyllabusName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Course");
            this.Property(t => t.CourseId).HasColumnName("CourseId");
            this.Property(t => t.CourseName).HasColumnName("CourseName");
            this.Property(t => t.SubId).HasColumnName("SubId");
            this.Property(t => t.ClassId).HasColumnName("ClassId");
            this.Property(t => t.SessionId).HasColumnName("SessionId");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.MinAttendance).HasColumnName("MinAttendance");
            this.Property(t => t.Attendancetype).HasColumnName("Attendancetype");
            this.Property(t => t.Totalworkingdays).HasColumnName("Totalworkingdays");
            this.Property(t => t.SyllabusName).HasColumnName("SyllabusName");
        }
    }
}
