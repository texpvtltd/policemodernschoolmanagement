using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class TimeTableMap : EntityTypeConfiguration<TimeTable>
    {
        public TimeTableMap()
        {
            // Primary Key
            this.HasKey(t => t.TimeTableID);

            // Properties
            this.Property(t => t.Day)
                .HasMaxLength(50);

            this.Property(t => t.Time)
                .HasMaxLength(20);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TimeTable");
            this.Property(t => t.TimeTableID).HasColumnName("TimeTableID");
            this.Property(t => t.ClassID).HasColumnName("ClassID");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.Session).HasColumnName("Session");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.Day).HasColumnName("Day");
            this.Property(t => t.SubjectID).HasColumnName("SubjectID");
            this.Property(t => t.Time).HasColumnName("Time");
            this.Property(t => t.TeacherID).HasColumnName("TeacherID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
