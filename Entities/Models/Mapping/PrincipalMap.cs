using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class PrincipalMap : EntityTypeConfiguration<Principal>
    {
        public PrincipalMap()
        {
            // Primary Key
            this.HasKey(t => t.PrincipalID);

            // Properties
            this.Property(t => t.PrincipalName)
                .HasMaxLength(20);

            this.Property(t => t.Contact)
                .HasMaxLength(15);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Principal");
            this.Property(t => t.PrincipalID).HasColumnName("PrincipalID");
            this.Property(t => t.PrincipalName).HasColumnName("PrincipalName");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.PrincipalImage).HasColumnName("PrincipalImage");
            this.Property(t => t.Session).HasColumnName("Session");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Email).HasColumnName("Email");
        }
    }
}
