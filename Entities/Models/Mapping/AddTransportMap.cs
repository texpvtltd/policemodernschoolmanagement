using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AddTransportMap : EntityTypeConfiguration<AddTransport>
    {
        public AddTransportMap()
        {
            // Primary Key
            this.HasKey(t => t.VehicleID);

            // Properties
            this.Property(t => t.SchoolID)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .IsFixedLength()
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("AddTransport");
            this.Property(t => t.VehicleID).HasColumnName("VehicleID");
            this.Property(t => t.VehicleNo).HasColumnName("VehicleNo");
            this.Property(t => t.NoofSeats).HasColumnName("NoofSeats");
            this.Property(t => t.MaximumAllowed).HasColumnName("MaximumAllowed");
            this.Property(t => t.VehicleType).HasColumnName("VehicleType");
            this.Property(t => t.ContactPerson).HasColumnName("ContactPerson");
            this.Property(t => t.InsuranceRenewalDate).HasColumnName("InsuranceRenewalDate");
            this.Property(t => t.RenewalDate).HasColumnName("RenewalDate");
            this.Property(t => t.TrackID).HasColumnName("TrackID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
