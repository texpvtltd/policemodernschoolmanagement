using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class TransportAllocationMap : EntityTypeConfiguration<TransportAllocation>
    {
        public TransportAllocationMap()
        {
            // Primary Key
            this.HasKey(t => t.AllocationId);

            // Properties
            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TransportAllocation");
            this.Property(t => t.AllocationId).HasColumnName("AllocationId");
            this.Property(t => t.Route).HasColumnName("Route");
            this.Property(t => t.Destination).HasColumnName("Destination");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.StartFrequency).HasColumnName("StartFrequency");
            this.Property(t => t.EndFrequency).HasColumnName("EndFrequency");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
