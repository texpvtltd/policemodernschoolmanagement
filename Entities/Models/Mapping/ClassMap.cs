using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class ClassMap : EntityTypeConfiguration<Class>
    {
        public ClassMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ClassID, t.SectionID, t.SchoolID });

            // Properties
            this.Property(t => t.ClassID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ClassName)
                .HasMaxLength(6);

            this.Property(t => t.SectionID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.SchoolID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Class");
            this.Property(t => t.ClassID).HasColumnName("ClassID");
            this.Property(t => t.ClassName).HasColumnName("ClassName");
            this.Property(t => t.SectionID).HasColumnName("SectionID");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.ClassTeacherID).HasColumnName("ClassTeacherID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.DepartmentId).HasColumnName("DepartmentId");
        }
    }
}
