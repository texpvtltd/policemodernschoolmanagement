using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class AssignTeacherMap : EntityTypeConfiguration<AssignTeacher>
    {
        public AssignTeacherMap()
        {
            // Primary Key
            this.HasKey(t => t.AssignTeacherId);

            // Properties
            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.IsClassTeacher)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("AssignTeacher");
            this.Property(t => t.AssignTeacherId).HasColumnName("AssignTeacherId");
            this.Property(t => t.TeacherId).HasColumnName("TeacherId");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.CourseId).HasColumnName("CourseId");
            this.Property(t => t.BatchId).HasColumnName("BatchId");
            this.Property(t => t.ClassId).HasColumnName("ClassId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.IsClassTeacher).HasColumnName("IsClassTeacher");
            this.Property(t => t.SubjectId).HasColumnName("SubjectId");
        }
    }
}
