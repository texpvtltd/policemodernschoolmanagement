using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HostalDetailMap : EntityTypeConfiguration<HostalDetail>
    {
        public HostalDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.HostalId);

            // Properties
            this.Property(t => t.HostalName)
                .HasMaxLength(50);

            this.Property(t => t.HostalType)
                .HasMaxLength(50);

            this.Property(t => t.HostalAddress)
                .IsRequired();

            this.Property(t => t.HostalContact)
                .HasMaxLength(50);

            this.Property(t => t.WardenName)
                .HasMaxLength(50);

            this.Property(t => t.WardenPhoneNo)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HostalDetail");
            this.Property(t => t.HostalId).HasColumnName("HostalId");
            this.Property(t => t.HostalName).HasColumnName("HostalName");
            this.Property(t => t.HostalType).HasColumnName("HostalType");
            this.Property(t => t.HostalAddress).HasColumnName("HostalAddress");
            this.Property(t => t.HostalContact).HasColumnName("HostalContact");
            this.Property(t => t.WardenName).HasColumnName("WardenName");
            this.Property(t => t.WardenPhoneNo).HasColumnName("WardenPhoneNo");
            this.Property(t => t.WardanAddress).HasColumnName("WardanAddress");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
