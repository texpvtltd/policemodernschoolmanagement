using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class TransPortFeesCollectionMap : EntityTypeConfiguration<TransPortFeesCollection>
    {
        public TransPortFeesCollectionMap()
        {
            // Primary Key
            this.HasKey(t => t.TransPortFeeCollId);

            // Properties
            this.Property(t => t.ChequeDate)
                .IsRequired();

            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.UserType)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("TransPortFeesCollection");
            this.Property(t => t.TransPortFeeCollId).HasColumnName("TransPortFeeCollId");
            this.Property(t => t.FeeType).HasColumnName("FeeType");
            this.Property(t => t.ActualAmount).HasColumnName("ActualAmount");
            this.Property(t => t.AmountTobePaid).HasColumnName("AmountTobePaid");
            this.Property(t => t.Fine).HasColumnName("Fine");
            this.Property(t => t.Discount).HasColumnName("Discount");
            this.Property(t => t.ModeOfPay).HasColumnName("ModeOfPay");
            this.Property(t => t.Remark).HasColumnName("Remark");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.ReciptNo).HasColumnName("ReciptNo");
            this.Property(t => t.BankName).HasColumnName("BankName");
            this.Property(t => t.ChequeNo).HasColumnName("ChequeNo");
            this.Property(t => t.ChequeDate).HasColumnName("ChequeDate");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
