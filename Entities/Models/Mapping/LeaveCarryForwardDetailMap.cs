using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class LeaveCarryForwardDetailMap : EntityTypeConfiguration<LeaveCarryForwardDetail>
    {
        public LeaveCarryForwardDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.LeaveCarryForwordId);

            // Properties
            this.Property(t => t.LeaveCategory)
                .HasMaxLength(50);

            this.Property(t => t.Desitnation)
                .HasMaxLength(50);

            this.Property(t => t.Employee)
                .HasMaxLength(50);

            this.Property(t => t.LeaveCount)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LeaveCarryForwardDetail");
            this.Property(t => t.LeaveCarryForwordId).HasColumnName("LeaveCarryForwordId");
            this.Property(t => t.LeaveCategory).HasColumnName("LeaveCategory");
            this.Property(t => t.Desitnation).HasColumnName("Desitnation");
            this.Property(t => t.Employee).HasColumnName("Employee");
            this.Property(t => t.LeaveCount).HasColumnName("LeaveCount");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
        }
    }
}
