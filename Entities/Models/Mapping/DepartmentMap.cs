using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            // Primary Key
            this.HasKey(t => t.DeptId);

            // Properties
            this.Property(t => t.DepartmentName)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(10);

            // Table & Column Mappings
            this.ToTable("Department");
            this.Property(t => t.DeptId).HasColumnName("DeptId");
            this.Property(t => t.DepartmentName).HasColumnName("DepartmentName");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
