using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class StudentMap : EntityTypeConfiguration<Student>
    {
        public StudentMap()
        {
            // Primary Key
            this.HasKey(t => t.StudentID);

            // Properties
            this.Property(t => t.RegdNo)
                .HasMaxLength(50);

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Gender)
                .HasMaxLength(10);

            this.Property(t => t.FatherName)
                .HasMaxLength(50);

            this.Property(t => t.MotherName)
                .HasMaxLength(50);

            this.Property(t => t.Attendance)
                .HasMaxLength(10);

            this.Property(t => t.Grade)
                .HasMaxLength(10);

            this.Property(t => t.Marksobtained)
                .HasMaxLength(10);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Student");
            this.Property(t => t.StudentID).HasColumnName("StudentID");
            this.Property(t => t.RegdNo).HasColumnName("RegdNo");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.FatherName).HasColumnName("FatherName");
            this.Property(t => t.MotherName).HasColumnName("MotherName");
            this.Property(t => t.ClassID).HasColumnName("ClassID");
            this.Property(t => t.Section).HasColumnName("Section");
            this.Property(t => t.Session).HasColumnName("Session");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.SudentImage).HasColumnName("SudentImage");
            this.Property(t => t.Attendance).HasColumnName("Attendance");
            this.Property(t => t.Grade).HasColumnName("Grade");
            this.Property(t => t.Marksobtained).HasColumnName("Marksobtained");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
