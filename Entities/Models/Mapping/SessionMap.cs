using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class SessionMap : EntityTypeConfiguration<Session>
    {
        public SessionMap()
        {
            // Primary Key
            this.HasKey(t => t.SessionID);

            // Properties
            this.Property(t => t.Session1)
                .IsRequired()
                .HasMaxLength(10);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Session");
            this.Property(t => t.SessionID).HasColumnName("SessionID");
            this.Property(t => t.Session1).HasColumnName("Session");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
