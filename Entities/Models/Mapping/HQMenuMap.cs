using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HQMenuMap : EntityTypeConfiguration<HQMenu>
    {
        public HQMenuMap()
        {
            // Primary Key
            this.HasKey(t => t.MenuId);

            // Properties
            this.Property(t => t.MenuName)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.MenuClass)
                .HasMaxLength(50);

            this.Property(t => t.Link)
                .IsRequired();

            this.Property(t => t.TokenKey)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HQMenu");
            this.Property(t => t.MenuId).HasColumnName("MenuId");
            this.Property(t => t.MenuName).HasColumnName("MenuName");
            this.Property(t => t.MenuClass).HasColumnName("MenuClass");
            this.Property(t => t.Link).HasColumnName("Link");
            this.Property(t => t.ParentId).HasColumnName("ParentId");
            this.Property(t => t.TokenKey).HasColumnName("TokenKey");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
