using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class StudentDocumentMap : EntityTypeConfiguration<StudentDocument>
    {
        public StudentDocumentMap()
        {
            // Primary Key
            this.HasKey(t => t.DocID);

            // Properties
            this.Property(t => t.SchoolId)
                .HasMaxLength(50);

            this.Property(t => t.SchoolName)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("StudentDocument");
            this.Property(t => t.DocID).HasColumnName("DocID");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.SchoolName).HasColumnName("SchoolName");
            this.Property(t => t.StudentId).HasColumnName("StudentId");
            this.Property(t => t.FileName).HasColumnName("FileName");
            this.Property(t => t.FilePath).HasColumnName("FilePath");
        }
    }
}
