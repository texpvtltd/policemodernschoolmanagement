using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class IssueBookMap : EntityTypeConfiguration<IssueBook>
    {
        public IssueBookMap()
        {
            // Primary Key
            this.HasKey(t => t.BookIssuedID);

            // Properties
            this.Property(t => t.IssuedDate)
                .HasMaxLength(50);

            this.Property(t => t.DueDate)
                .HasMaxLength(50);

            this.Property(t => t.UserType)
                .HasMaxLength(50);

            this.Property(t => t.UserName)
                .HasMaxLength(50);

            this.Property(t => t.BookNo)
                .HasMaxLength(50);

            this.Property(t => t.BooKTitle)
                .HasMaxLength(50);

            this.Property(t => t.UserSection)
                .HasMaxLength(50);

            this.Property(t => t.SchoolID)
                .HasMaxLength(50);

            this.Property(t => t.UserID)
                .HasMaxLength(50);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("IssueBook");
            this.Property(t => t.BookIssuedID).HasColumnName("BookIssuedID");
            this.Property(t => t.IssuedDate).HasColumnName("IssuedDate");
            this.Property(t => t.DueDate).HasColumnName("DueDate");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.UserName).HasColumnName("UserName");
            this.Property(t => t.UserCourse).HasColumnName("UserCourse");
            this.Property(t => t.BookName).HasColumnName("BookName");
            this.Property(t => t.BookNo).HasColumnName("BookNo");
            this.Property(t => t.BooKTitle).HasColumnName("BooKTitle");
            this.Property(t => t.UserSection).HasColumnName("UserSection");
            this.Property(t => t.SchoolID).HasColumnName("SchoolID");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
