using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class HQUserMap : EntityTypeConfiguration<HQUser>
    {
        public HQUserMap()
        {
            // Primary Key
            this.HasKey(t => t.HQUserID);

            // Properties
            this.Property(t => t.Name)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.Contact)
                .HasMaxLength(15);

            this.Property(t => t.Gender)
                .HasMaxLength(2);

            this.Property(t => t.IsDelete)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("HQUser");
            this.Property(t => t.HQUserID).HasColumnName("HQUserID");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.UserID).HasColumnName("UserID");
            this.Property(t => t.UserType).HasColumnName("UserType");
            this.Property(t => t.UserImage).HasColumnName("UserImage");
            this.Property(t => t.Contact).HasColumnName("Contact");
            this.Property(t => t.Gender).HasColumnName("Gender");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.IsDelete).HasColumnName("IsDelete");
        }
    }
}
