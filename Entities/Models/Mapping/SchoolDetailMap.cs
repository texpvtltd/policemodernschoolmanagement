using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Entities.Models.Mapping
{
    public class SchoolDetailMap : EntityTypeConfiguration<SchoolDetail>
    {
        public SchoolDetailMap()
        {
            // Primary Key
            this.HasKey(t => t.SchoolDetID);

            // Properties
            this.Property(t => t.Email)
                .HasMaxLength(50);

            this.Property(t => t.Phone)
                .HasMaxLength(50);

            this.Property(t => t.Mobile)
                .HasMaxLength(50);

            this.Property(t => t.Fax)
                .HasMaxLength(50);

            this.Property(t => t.Contactperson)
                .HasMaxLength(50);

            this.Property(t => t.City)
                .HasMaxLength(50);

            this.Property(t => t.InstitutionCode)
                .HasMaxLength(50);

            this.Property(t => t.Openedon)
                .HasMaxLength(20);

            this.Property(t => t.Landtype)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SchoolDetail");
            this.Property(t => t.SchoolDetID).HasColumnName("SchoolDetID");
            this.Property(t => t.SchoolId).HasColumnName("SchoolId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Address).HasColumnName("Address");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Phone).HasColumnName("Phone");
            this.Property(t => t.Mobile).HasColumnName("Mobile");
            this.Property(t => t.Fax).HasColumnName("Fax");
            this.Property(t => t.Contactperson).HasColumnName("Contactperson");
            this.Property(t => t.City).HasColumnName("City");
            this.Property(t => t.InstitutionCode).HasColumnName("InstitutionCode");
            this.Property(t => t.Openedon).HasColumnName("Openedon");
            this.Property(t => t.Landtype).HasColumnName("Landtype");
            this.Property(t => t.Landdoc).HasColumnName("Landdoc");
        }
    }
}
