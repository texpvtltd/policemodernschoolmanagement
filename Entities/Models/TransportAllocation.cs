using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class TransportAllocation
    {
        public int AllocationId { get; set; }
        public string Route { get; set; }
        public string Destination { get; set; }
        public string Type { get; set; }
        public string StartFrequency { get; set; }
        public string EndFrequency { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
