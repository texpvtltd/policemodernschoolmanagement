using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class SchoolUser
    {
        public int SchoolUserID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string UserImage { get; set; }
        public string AdhaarImage { get; set; }
        public string UserType { get; set; }
        public int SchoolID { get; set; }
        public string IsDelete { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}
