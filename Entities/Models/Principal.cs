using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Principal
    {
        public int PrincipalID { get; set; }
        public string PrincipalName { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string PrincipalImage { get; set; }
        public Nullable<int> Session { get; set; }
        public string Contact { get; set; }
        public string IsDelete { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
