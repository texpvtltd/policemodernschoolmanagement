using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AddDestination_Fees
    {
        public int DestinationId { get; set; }
        public string RouteCode { get; set; }
        public string PickupDrop { get; set; }
        public string StopTime { get; set; }
        public string Amount { get; set; }
        public string FeeType { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
