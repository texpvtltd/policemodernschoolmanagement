using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class academicsetting
    {
        public int Id { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public Nullable<int> SessionId { get; set; }
        public string Startdate { get; set; }
        public string Sessionenddate { get; set; }
    }
}
