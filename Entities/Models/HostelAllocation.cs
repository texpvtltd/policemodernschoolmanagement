using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostelAllocation
    {
        public int HostelAllocatedId { get; set; }
        public string UserType { get; set; }
        public string UsarName { get; set; }
        public string HostelName { get; set; }
        public string HostalType { get; set; }
        public string HostelRoom { get; set; }
        public string HostelRegdate { get; set; }
        public string VacatingDate { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
