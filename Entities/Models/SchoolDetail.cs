using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class SchoolDetail
    {
        public int SchoolDetID { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Contactperson { get; set; }
        public string City { get; set; }
        public string InstitutionCode { get; set; }
        public string Openedon { get; set; }
        public string Landtype { get; set; }
        public string Landdoc { get; set; }
    }
}
