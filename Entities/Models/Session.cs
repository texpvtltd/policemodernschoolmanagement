using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Session
    {
        public int SessionID { get; set; }
        public string Session1 { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
