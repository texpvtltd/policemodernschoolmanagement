using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AddHostalRoom
    {
        public int RoomId { get; set; }
        public string HostalType { get; set; }
        public string HostalName { get; set; }
        public string HostalfloorName { get; set; }
        public string RoomNo { get; set; }
        public string NoOfBed { get; set; }
        public string FeeType { get; set; }
        public string BedNo { get; set; }
        public string RoomRent { get; set; }
        public string Amount { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
