using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class StudentDocument
    {
        public int DocID { get; set; }
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string StudentId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
