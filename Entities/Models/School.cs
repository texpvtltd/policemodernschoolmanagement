using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class School
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string ContactNo { get; set; }
        public string Logo { get; set; }
        public string SchoolCode { get; set; }
        public Nullable<int> SessionID { get; set; }
        public string City { get; set; }
        public string IsDelete { get; set; }
    }
}
