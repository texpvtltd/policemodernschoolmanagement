using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class TransprotFee
    {
        public int TransPortFeeCollId { get; set; }
        public string FeeType { get; set; }
        public string ActualAmount { get; set; }
        public string AmountTobePaid { get; set; }
        public string Fine { get; set; }
        public string Discount { get; set; }
        public string ModeOfPay { get; set; }
        public string Remark { get; set; }
        public string TotalAmount { get; set; }
        public string ReciptNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
