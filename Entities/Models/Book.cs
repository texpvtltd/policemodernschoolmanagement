using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Book
    {
        public int BookID { get; set; }
        public string PurchaseDate { get; set; }
        public string BillNo { get; set; }
        public string BookISBNNo { get; set; }
        public string BookNo { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Edition { get; set; }
        public string BookCategory { get; set; }
        public string Publisher { get; set; }
        public string NoofCopies { get; set; }
        public string ShelfNo { get; set; }
        public string BookPosition { get; set; }
        public string BookCost { get; set; }
        public string Language { get; set; }
        public string BookCondition { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
