using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AddDriver
    {
        public int DriverId { get; set; }
        public string VehicleNo { get; set; }
        public string DriverName { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DateofBirth { get; set; }
        public string ContactNo { get; set; }
        public string LicenseNumber { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}
