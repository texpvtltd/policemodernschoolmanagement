using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class BookCategory
    {
        public int BookCategoryID { get; set; }
        public string Category { get; set; }
        public string SectionCode { get; set; }
        public string SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
