using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class AddTransport
    {
        public int VehicleID { get; set; }
        public string VehicleNo { get; set; }
        public string NoofSeats { get; set; }
        public string MaximumAllowed { get; set; }
        public string VehicleType { get; set; }
        public string ContactPerson { get; set; }
        public string InsuranceRenewalDate { get; set; }
        public string RenewalDate { get; set; }
        public string TrackID { get; set; }
        public string SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
