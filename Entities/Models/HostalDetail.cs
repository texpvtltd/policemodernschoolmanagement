using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class HostalDetail
    {
        public int HostalId { get; set; }
        public string HostalName { get; set; }
        public string HostalType { get; set; }
        public string HostalAddress { get; set; }
        public string HostalContact { get; set; }
        public string WardenName { get; set; }
        public string WardenPhoneNo { get; set; }
        public string WardanAddress { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
    }
}
