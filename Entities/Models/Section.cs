using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Section
    {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public int SchoolID { get; set; }
        public string IsDelete { get; set; }
    }
}
