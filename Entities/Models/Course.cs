using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class Course
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public string SubId { get; set; }
        public string ClassId { get; set; }
        public string SessionId { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string MinAttendance { get; set; }
        public string Attendancetype { get; set; }
        public string Totalworkingdays { get; set; }
        public string SyllabusName { get; set; }
    }
}
