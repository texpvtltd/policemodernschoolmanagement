using System;
using System.Collections.Generic;

namespace Entities.Models
{
    public partial class UserLogin
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string TokenId { get; set; }
        public string UserRights { get; set; }
        public string SchoolName { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string Session { get; set; }
        public string IsDelete { get; set; }
        public string IsConfirm { get; set; }
    }
}
