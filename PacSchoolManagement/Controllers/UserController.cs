﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class UserController : Controller
    {

          private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
         public UserLoginBusiness _userBusiness;
         public UserController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

        }
        // GET: User
         [HttpGet]
        public ActionResult UserList()
        {
            UserLoginViewModel ViewModel = new UserLoginViewModel();
            var list = _userBusiness.GetListWT();
          
            return View();
        }
         [HttpPost]
         public ActionResult UserList(string s)
         {

             return View();

         }

         public JsonResult SaveDataInDatabase(UserLoginViewModel model)
         {



             if (model.SchoolID > 0)
             {

                 Mapper.CreateMap<UserLoginViewModel, UserLogin>();
                 UserLogin mod = Mapper.Map<UserLoginViewModel, UserLogin>(model);

                 mod.SchoolID = 0;
                 mod.SchoolName = "Headquater";
                 mod.Username = model.Username;
                 mod.Password = Md5Encryption.Encrypt(model.Password);
                 mod.UserType = "Admin";
                 mod.Session = "0";
                 mod.IsDelete = "false";
                 mod.IsConfirm = "true";
                 bool isSuccess = _userBusiness.AddUpdateDeleteUser(mod, "U");
                 if (isSuccess)
                 {
                     TempData["Success"] = "Updated Successfully!!";
                     TempData["isSuccess"] = "true";
                     // return RedirectToAction("Index");
                 }
                 else
                 {
                     TempData["Success"] = "Failed to create Brand!!";
                     TempData["isSuccess"] = "false";
                 }
             }



             else
             {

                 var currentUserId = GlobalUser.getGlobalUser().UserId;

                 Mapper.CreateMap<UserLoginViewModel, UserLogin>();
                 UserLogin mod = Mapper.Map<UserLoginViewModel, UserLogin>(model);
                // model.SchoolID = Convert.ToInt32(currentUserId);
                 mod.SchoolID = 0;
                 mod.SchoolName = "Headquater";
                 mod.Username = model.Username;
                 mod.Password = Md5Encryption.Encrypt(model.Password);
                 mod.UserType = "Admin";
                 mod.Session = "0";
                 mod.IsDelete = "false";
                 mod.IsConfirm = "true";
                 var userconf = _userBusiness.ValidateUser( mod ,"I");
                 if (userconf != "Email already exists!")
                 {
                     // cls.TokenKey = GlobalMethods.GetToken();
                     bool isSuccess = _userBusiness.AddUpdateDeleteUser(mod, "I");
                     if (isSuccess)
                     {
                         TempData["Success"] = "User Created Successfully!!";
                         TempData["isSuccess"] = "true";
                         // return RedirectToAction("Index");
                     }
                     else
                     {
                         TempData["Success"] = "Failed to create User!!";
                         TempData["isSuccess"] = "false";
                     }
                 }
                 else
                 {
                     TempData["Success"] = "Username already exist!!";
                     TempData["isSuccess"] = "false";
                 }
                 // result = true;

                 //  }
             }
             return Json(model, JsonRequestBehavior.AllowGet);
         }


         public JsonResult GetStudentList()  //Gets the todo Lists.
         {


             var userList = _userBusiness.GetListWT();
             List<UserLoginViewModel> userViewModelList = new List<UserLoginViewModel>();

             var records = (from p in userList
                            select new UserLoginViewModel
                            {
                                Username = p.Username,
                                Password = p.Password,
                                UserRights = p.UserRights,
                                UserType = p.UserType,
                                
                                SchoolID = p.SchoolID,
                                SchoolName = p.SchoolName,
                             

                            }).AsQueryable();


             return Json(records, JsonRequestBehavior.AllowGet);
         }

         public JsonResult DeleteStudentRecord(int StudentId)
         {
             string JsonStr = "";
             bool isSuccess = true;
             string message = "Delete Successful!";
             _unitOfWork.BeginTransaction();

             //var Stu = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();
             if (StudentId != null)
             {
                 var br = _userBusiness.Find(StudentId);
                 _userBusiness.Delete(StudentId);
                 _unitOfWork.SaveChanges();

             }
             //List<Class> userList = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList();
             //foreach (Class usr in userList)
             //{
             //    int deleteId = Convert.ToInt32(usr.ClassID);
             //   // var br = _classBusiness.Find(deleteId);
             //    _classBusiness.Deleteid(deleteId);
             //    _unitOfWork.SaveChanges();
             //}

             _unitOfWork.Commit();



             TempData["Success"] = message;
             TempData["isSuccess"] = isSuccess.ToString();

             JsonStr = "{\"message\":\"" + message + "\",\"isSuccess\":\"" + isSuccess + "\"}";
             return Json(JsonStr, JsonRequestBehavior.AllowGet);
         }

         public JsonResult GetStudentById(int StudentId)
         {
             var model = _userBusiness.GetListWT(x => x.UserId == StudentId).ToList().FirstOrDefault();
             string value = string.Empty;
             value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
             {
                 ReferenceLoopHandling = ReferenceLoopHandling.Ignore
             });
             return Json(value, JsonRequestBehavior.AllowGet);
         }

    }
}