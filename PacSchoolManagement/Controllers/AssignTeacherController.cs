﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class AssignTeacherController : Controller
    {
         private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public ClassBusiness _schoolBusiness;
        public TeacherBusiness _teacherBusiness;
        public SubjectBusiness _subjectBusiness;
        private UserLoginBusiness _userBusiness;
       private CourseBusiness _courseBusiness;
       private BatchBusiness _batchBusiness;
       private AssignTeacherBusiness _assignteacherBusiness;

       public AssignTeacherController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._subjectBusiness = new SubjectBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._courseBusiness = new CourseBusiness(_df, this._unitOfWork);
            this._batchBusiness = new BatchBusiness(_df, this._unitOfWork);
            this._assignteacherBusiness = new AssignTeacherBusiness(_df, this._unitOfWork);


        }
        //
        // GET: /AssignTeacher/
    
        [HttpGet]
        public ActionResult Index()
        {

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var schid1 = _userBusiness.Find(currentUserId).SchoolID;
            var schid = _userBusiness.Find(currentUserId).SchoolID.ToString();
            AssignTeacherViewModel viewmodel = new AssignTeacherViewModel();

            var schoolList1 = _courseBusiness.GetListWT().Where(x => x.SchoolId == schid);
            viewmodel.CourseList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.CourseName.ToString(),
                Value = x.CourseId.ToString()
            }).ToList();
            var batchList = _batchBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.BatchList = batchList.Select(x => new SelectListItem
            {
                Text = x.BatchName.ToString(),
                Value = x.BatchId.ToString()
            }).ToList();
            var teacherList = _teacherBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.TeacherList = teacherList.Select(x => new SelectListItem
            {
                Text = x.FName.ToString(),
                Value = x.SchoolUserID.ToString()
            }).ToList();
            var classList = _schoolBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.ClassList = classList.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();

            var subjectList = _subjectBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.SubjectList = subjectList.Select(x => new SelectListItem
            {
                Text = x.SubjectName.ToString(),
                Value = x.SubjectID.ToString()
            }).ToList();


            return View(viewmodel);


        }
        [HttpPost]
        public ActionResult Index(string s)
        {

            return View();

        }



        public JsonResult SaveDataInDatabase(AssignTeacherViewModel clsmodel)
        {
            //HttpPostedFileBase fu = Request.Files["file"];
            //HttpPostedFileBase file1 = clsmodel.Logo;

            // file = clsmodel.ImageUpload;
            int schoolid = Convert.ToInt32(clsmodel.SchoolId);
            if (schoolid > 0)
            {
                var CourseList = _assignteacherBusiness.GetListWT().ToList().FirstOrDefault();

                CourseList.SchoolId = clsmodel.SchoolId;
                CourseList.BatchId = clsmodel.BatchId;
                CourseList.ClassId = clsmodel.ClassId;
                CourseList.SubjectId = clsmodel.SubjectId;
                CourseList.TeacherId = clsmodel.TeacherId;
                CourseList.IsClassTeacher = clsmodel.IsClassTeacher;
                CourseList.IsDelete = "false";

                bool isSuccess = _assignteacherBusiness.AddUpdateDeleteAssignTeacher(CourseList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }



            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                var schid = _userBusiness.Find(currentUserId).SchoolID;

                Mapper.CreateMap<AssignTeacherViewModel, AssignTeacher>();
                AssignTeacher cls = Mapper.Map<AssignTeacherViewModel, AssignTeacher>(clsmodel);
                cls.SchoolId = schid;
                cls.IsDelete = "false";
                //cls.SessionId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().Session;
                //  cls.SessionID = Convert.ToInt32(currentUserId);
                // cls.TokenKey = GlobalMethods.GetToken();

                bool isSuccess = _assignteacherBusiness.AddUpdateDeleteAssignTeacher(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "School Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create School!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

                //  }
            }
            return Json(JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetStudentList()  //Gets the todo Lists.
        {


            var courseList = _assignteacherBusiness.GetListWT(x => x.IsDelete == "false");
            List<AssignTeacherViewModel> courseViewModelList = new List<AssignTeacherViewModel>();

            var records = (from p in courseList
                           select new AssignTeacherViewModel
                           {
                               AssignTeacherId = p.AssignTeacherId,
                               BatchId = p.BatchId,
                               ClassId = p.ClassId,
                               CourseId = p.CourseId,

                               // ReadConfigData.GetAppSettingsValue("CMSUrl") + "/SliderImage/" + c.SliderImage
                               IsClassTeacher = p.IsClassTeacher,
                               SubjectId = p.SubjectId,
                               SchoolId = p.SchoolId,
                               TeacherId = p.TeacherId
                             

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _assignteacherBusiness.GetListWT(x => x.AssignTeacherId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _assignteacherBusiness.AddUpdateDeleteAssignTeacher(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Updated Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to create Brand!!";
                TempData["isSuccess"] = "false";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _assignteacherBusiness.GetListWT(x => x.BatchId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult SubjectTeacher()
        {

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var schid1 = _userBusiness.Find(currentUserId).SchoolID;
            var schid = _userBusiness.Find(currentUserId).SchoolID.ToString();
            AssignTeacherViewModel viewmodel = new AssignTeacherViewModel();

            var schoolList1 = _courseBusiness.GetListWT().Where(x => x.SchoolId == schid);
            viewmodel.CourseList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.CourseName.ToString(),
                Value = x.CourseId.ToString()
            }).ToList();
            var batchList = _batchBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.BatchList = batchList.Select(x => new SelectListItem
            {
                Text = x.BatchName.ToString(),
                Value = x.BatchId.ToString()
            }).ToList();
            var teacherList = _teacherBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.TeacherList = teacherList.Select(x => new SelectListItem
            {
                Text = x.FName.ToString(),
                Value = x.SchoolUserID.ToString()
            }).ToList();
            var classList = _schoolBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.ClassList = classList.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();

            var subjectList = _subjectBusiness.GetListWT().Where(x => x.SchoolID == schid1);
            viewmodel.SubjectList = subjectList.Select(x => new SelectListItem
            {
                Text = x.SubjectName.ToString(),
                Value = x.SubjectID.ToString()
            }).ToList();


            return View(viewmodel);


        }
        [HttpPost]
        public ActionResult SubjectTeacher(string s)
        {

            return View();

        }


	}
}