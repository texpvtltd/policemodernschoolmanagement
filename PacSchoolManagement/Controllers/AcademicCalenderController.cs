﻿using AutoMapper;
using Business;
using Entities.Models;
using PacSchoolManagement.Models;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PacSchoolManagement.Controllers
{
    public class AcademicCalenderController : Controller
    {

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public UserLoginBusiness _userBusiness;
        public AcademicSettingBusiness _academicBusiness;
        public AcademicCalenderController()
        {
            this._unitOfWork = new UnitOfWork(_df);
        
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._academicBusiness = new AcademicSettingBusiness(_df, this._unitOfWork);

        }
        //
        // GET: /AcademicCalender/
        public ActionResult Index()
        {
            var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;
            var ac = _academicBusiness.GetListWT(x => x.SchoolId == currentUserId).ToList().FirstOrDefault();
            //var detail = _schooldetailBusiness.GetListWT(x => x.SchoolId == currentUserId).FirstOrDefault();

            
            AcademicsettingViewModel acviewmodel = new AcademicsettingViewModel();

            acviewmodel.Id = ac.Id;
            acviewmodel.SchoolId = ac.SchoolId;
            acviewmodel.SessionId = ac.SessionId;
            acviewmodel.Sessionenddate = ac.Sessionenddate;
            acviewmodel.Startdate = ac.Startdate;
            

            return View(acviewmodel);
        }
        [HttpPost]
        public ActionResult Index(AcademicsettingViewModel acviewmodel)
        {
            Mapper.CreateMap<AcademicsettingViewModel, academicsetting>();
            academicsetting cls = Mapper.Map<AcademicsettingViewModel, academicsetting>(acviewmodel);
            var upload = _academicBusiness.AddUpdateDeleteClass(cls, "U");

            return View("Index");
          
        }

        //public JsonResult GetEvents()
        //{
        //    using (MyDatabaseEntities dc = new MyDatabaseEntities())
        //    {
        //        var events = dc.Events.ToList();
        //        return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        //    }
        //}
	}
}