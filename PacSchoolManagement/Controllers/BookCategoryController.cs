﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class BookCategoryController : Controller
    {

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        private BookBusiness _bookbusiness;
        private BookCategoryBusiness _bookcategorybusiness;
        // GET: BookCategory



        public BookCategoryController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._bookbusiness = new BookBusiness(_df, this._unitOfWork);
            this._bookcategorybusiness = new BookCategoryBusiness(_df, this._unitOfWork);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BookCategoryList()
        {





            return View();
        }

        public JsonResult SaveDataInDatabase(BookCategoryViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.BookCategoryID > 0)
            {
                var userList = _bookcategorybusiness.GetListWT(x => x.BookCategoryID == clsmodel.BookCategoryID).ToList().FirstOrDefault();

                userList.BookCategoryID = clsmodel.BookCategoryID;
                userList.Category = clsmodel.Category;
                userList.SectionCode = clsmodel.SectionCode;
                bool isSuccess = _bookcategorybusiness.AddUpdateDeleteBookCategory(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;

                Mapper.CreateMap<BookCategoryViewModel, BookCategory>();
                BookCategory cls = Mapper.Map<BookCategoryViewModel, BookCategory>(clsmodel);
                cls.SchoolID = Convert.ToInt32(currentUserId).ToString();
                cls.IsDelete = "false";

                bool isSuccess = _bookcategorybusiness.AddUpdateDeleteBookCategory(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {

            int SchoolId = Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID);
            var userList = _bookcategorybusiness.GetListWT(x => x.IsDelete == "false");
            List<BookCategoryViewModel> userViewModelList = new List<BookCategoryViewModel>();

            var records = (from p in userList
                           select new BookCategoryViewModel
                           {
                               BookCategoryID = p.BookCategoryID,
                               Category = p.Category,
                               SchoolID = p.SchoolID,
                               IsDelete = p.IsDelete,
                               SectionCode=p.SectionCode,
                           }).Where(i => i.SchoolID == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _bookcategorybusiness.GetListWT(x => x.BookCategoryID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _bookcategorybusiness.AddUpdateDeleteBookCategory(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _bookcategorybusiness.GetListWT(x => x.BookCategoryID== StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }






    }
}