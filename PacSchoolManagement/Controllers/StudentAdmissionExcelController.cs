﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlClient;
using Repository.RepositoryFactoryCore;
using Business;
using PacSchoolManagement.Models;
using Newtonsoft.Json;

namespace PacSchoolManagement.Controllers
{
    public class StudentAdmissionExcelController : Controller
    {
          private DatabaseFactory _df = new DatabaseFactory();
            private UnitOfWork _unitOfWork;
            public StudentBusiness _saBusiness;
            public StudentAdmissionExcelController()
            {
                this._unitOfWork = new UnitOfWork(_df);
                this._saBusiness = new StudentBusiness(_df, this._unitOfWork);

            }
        //
        // GET: /StudentAdmissionExcel/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase postedFile)
        {
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                string conString = string.Empty;
                switch (extension)
                {
                    case ".xls": //Excel 97-03.
                        conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07 and above.
                        conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }

                DataTable dt = new DataTable();
                conString = string.Format(conString, filePath);

                using (OleDbConnection connExcel = new OleDbConnection(conString))
                {
                    using (OleDbCommand cmdExcel = new OleDbCommand())
                    {
                        using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                        {
                            cmdExcel.Connection = connExcel;

                            //Get the name of First Sheet.
                            connExcel.Open();
                            DataTable dtExcelSchema;
                            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                            string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                            connExcel.Close();

                            //Read Data from First Sheet.
                            connExcel.Open();
                            cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                            odaExcel.SelectCommand = cmdExcel;
                            odaExcel.Fill(dt);
                            connExcel.Close();
                        }
                    }
                }

                conString = ConfigurationManager.ConnectionStrings["PacSchoolManagementContext"].ConnectionString;
                using (SqlConnection con = new SqlConnection(conString))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "dbo.Student";

                        //[OPTIONAL]: Map the Excel columns with that of the database table
                        sqlBulkCopy.ColumnMappings.Add("StudentID", "StudentID");
                        sqlBulkCopy.ColumnMappings.Add("RegdNo", "RegdNo");
                        sqlBulkCopy.ColumnMappings.Add("FirstName", "FirstName");
                          sqlBulkCopy.ColumnMappings.Add("LastName", "LastName");
                        sqlBulkCopy.ColumnMappings.Add("Gender", "Gender");
                        sqlBulkCopy.ColumnMappings.Add("FatherName", "FatherName");
                          sqlBulkCopy.ColumnMappings.Add("MotherName", "MotherName");
                          sqlBulkCopy.ColumnMappings.Add("ClassID", "ClassID");
                          sqlBulkCopy.ColumnMappings.Add("Session", "Session");
                        sqlBulkCopy.ColumnMappings.Add("Section", "Section");
                        sqlBulkCopy.ColumnMappings.Add("SchoolID", "SchoolID");
                        sqlBulkCopy.ColumnMappings.Add("SudentImage", "SudentImage");
                        sqlBulkCopy.ColumnMappings.Add("Attendance", "Attendance");
                        sqlBulkCopy.ColumnMappings.Add("Grade", "Grade");   
                        sqlBulkCopy.ColumnMappings.Add("Marksobtained", "Marksobtained");
                        sqlBulkCopy.ColumnMappings.Add("UserID", "UserID");
                        sqlBulkCopy.ColumnMappings.Add("IsDelete", "IsDelete");
               
                      


                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }

            return View();
        }


        public ActionResult StudentList()
        {
            
            var classList1 = _saBusiness.GetListWT();
           
            return View();
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {


            var userList = _saBusiness.GetListWT();
            List<StudentViewModel> userViewModelList = new List<StudentViewModel>();

            var records = (from p in userList
                           select new StudentViewModel
                           {
                               ClassID = p.ClassID,
                               StudentID = p.StudentID,
                               SchoolID = p.SchoolID,
                               FirstName = p.FirstName,
                               LastName = p.LastName
                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetStudentById(int SubjectID)
        {
            var model = _saBusiness.GetListWT(x => x.StudentID == SubjectID).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TransferCertificate(int StudentID)
        {

            var model = _saBusiness.GetListWT(x => x.StudentID == StudentID).ToList().FirstOrDefault();

            return View(model);
        }
        public ActionResult StudentIDCard(int StudentId)
        {
            var model = _saBusiness.GetListWT(x => x.StudentID == StudentId).ToList().FirstOrDefault();
            StudentViewModel svm = new StudentViewModel();
            svm.StudentID = model.StudentID;
            svm.FirstName = model.FirstName;
            svm.LastName = model.LastName;
            return View(svm);
            // return View();

        }
        public JsonResult GetStudentIdCard(int StudentId)
        {
            var model1 = _saBusiness.GetListWT(x => x.StudentID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model1, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);




            //var userList = _saBusiness.GetListWT();
            // List<StudentAdmissionViewModel> userViewModelList = new List<StudentAdmissionViewModel>();



            //  return Json(model1, JsonRequestBehavior.AllowGet);

        }
    
    
    }
}