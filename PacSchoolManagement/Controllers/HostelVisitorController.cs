﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;

namespace PacSchoolManagement.Controllers
{
    public class HostelVisitorController : Controller
    {
        // GET: HostelVisitor

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostelVisitorBusiness _hostelvisitorBusiness;
        private StudentBusiness _studentBusiness;
        private AddHostelRoomBusiness _roomBusiness;
        private HostelAllocationBusiness _allocationBusiness;
        public TeacherBusiness _teacherBusiness;
        public HostelVisitorController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._hostelvisitorBusiness = new HostelVisitorBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._roomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
            this._allocationBusiness = new HostelAllocationBusiness(_df, this._unitOfWork);

            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);

        }




        public ActionResult Index()
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _hostelvisitorBusiness.GetListWT();
            List<DeptViewModel> userViewModelList = new List<DeptViewModel>();

            var records = (from p in userList
                           select new HostelVisitorViewModel
                           {
                               VisitorId = p.VisitorId,
                               Hosteltype = p.Hosteltype,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,
                               Relation = p.Relation,
                               UserName = p.UserName,
                               VisitTime=p.VisitTime,
                               VisitorName=p.VisitorName,
                               VisitDate=p.VisitDate,
             
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


         


            return View(records);
        }

        public ActionResult HostelVisitorList()
        {
            HostelVisitorViewModel hostelvisitor = new HostelVisitorViewModel();
            var student = _studentBusiness.GetListWT();
            hostelvisitor.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();
            var Emploeyee = _teacherBusiness.GetListWT();
            hostelvisitor.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName + "" + x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();

            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            hostelvisitor.UserTypeList = userytype;


            return View(hostelvisitor);
        }

        public JsonResult SearchData(string prefix)
        {
            List<HostelAllocation> allUser = new List<HostelAllocation>();



            allUser = _allocationBusiness.GetListWT(c => c.UsarName==prefix).ToList();

            //allUser = dc.UserMasters.Where(a => a.Username.Contains(prefix)).ToList();
            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _allocationBusiness.GetListWT(x => x.IsDelete == "false" && x.UsarName == prefix);
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostelAllocationViewModel
                           {
                               SchoolId = p.SchoolId,
                               // HostalType = p.HostalType,
                               HostelAllocatedId = p.HostelAllocatedId,
                               HostalType = _allocationBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType,
                               HostelName = _allocationBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostelName,
                               HostelRegdate = p.HostelRegdate,
                               HostelRoom = _allocationBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).HostelRoom,
                               // HostelRoom = p.HostelRoom,
                               VacatingDate = p.VacatingDate,
                              // FloorName = _allocationBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)),
                               UserType = p.UserType,
                               UsarName = p.UsarName,

                           });

            return new JsonResult { Data = records, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }


        public JsonResult SaveDataInDatabase(HostelVisitorViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.VisitorId > 0)
            {
                var userList = _hostelvisitorBusiness.GetListWT(x => x.VisitorId == clsmodel.VisitorId).ToList().FirstOrDefault();
                userList.Relation = clsmodel.Relation;
                userList.SchoolId = clsmodel.SchoolId;
                userList.VisitorId = clsmodel.VisitorId;
                userList.VisitDate = clsmodel.VisitDate;
                userList.VisitorName = clsmodel.VisitorName;
                userList.VisitTime = clsmodel.VisitTime;
                userList.Hosteltype = clsmodel.Hosteltype;
                userList.UserType = clsmodel.UserType;
                userList.HostelName = clsmodel.HostelName;
              userList.UserName = clsmodel.UserName;

                bool isSuccess = _hostelvisitorBusiness.AddUpdateDeleteHostelVisitor(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<HostelVisitorViewModel, HostelVisitor>();
                HostelVisitor cls = Mapper.Map<HostelVisitorViewModel, HostelVisitor>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _hostelvisitorBusiness.AddUpdateDeleteHostelVisitor(cls, "I");
                if (isSuccess)

                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _hostelvisitorBusiness.GetListWT();
            List<DeptViewModel> userViewModelList = new List<DeptViewModel>();

            var records = (from p in userList
                           select new HostelVisitorViewModel
                           {
                               VisitorId = p.VisitorId,
                               Hosteltype = p.Hosteltype,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,
                               Relation=p.Relation,
                               UserName=p.UserName,

                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Delete(int txtval)
        {
            var userList = _hostelvisitorBusiness.GetListWT(x => x.VisitorId == txtval).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _hostelvisitorBusiness.AddUpdateDeleteHostelVisitor(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return RedirectToAction("Index", "HostelVisitor");
        }
        [HttpGet]
        public ActionResult EditVisitor(int txtval)
        {
            HostelVisitorViewModel clsmodel = new HostelVisitorViewModel();


            var student = _studentBusiness.GetListWT();
            clsmodel.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();
            var Emploeyee = _teacherBusiness.GetListWT();
            clsmodel.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName + "" + x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            clsmodel.UserTypeList = userytype;


            var userList = _hostelvisitorBusiness.GetListWT(x => x.VisitorId == txtval).ToList().FirstOrDefault();
            clsmodel.Relation = userList.Relation;
            clsmodel.SchoolId = userList.SchoolId;
            clsmodel.VisitorId = userList.VisitorId;
            clsmodel.VisitDate = userList.VisitDate;
            clsmodel.VisitorName = userList.VisitorName;
            clsmodel.VisitTime = userList.VisitTime;
            clsmodel.Hosteltype = userList.Hosteltype;
            clsmodel.UserType = userList.UserType;





            return View(clsmodel);

        }
        [HttpPost]
        public ActionResult EditVisitor()
        {
            HostelVisitorViewModel clsmodel = new HostelVisitorViewModel();


            if (clsmodel.VisitorId > 0)
            {
                var userList = _hostelvisitorBusiness.GetListWT(x => x.VisitorId == clsmodel.VisitorId).ToList().FirstOrDefault();

                
                bool isSuccess = _hostelvisitorBusiness.AddUpdateDeleteHostelVisitor(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<HostelVisitorViewModel, HostelVisitor>();
                HostelVisitor cls = Mapper.Map<HostelVisitorViewModel, HostelVisitor>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _hostelvisitorBusiness.AddUpdateDeleteHostelVisitor(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }























            return View();

        }











    }
}