﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;

namespace PacSchoolManagement.Controllers
{
    public class EmployeeController : Controller
    { 
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private SchoolRoleBusiness _roleBusiness;
        private EmployeeBusiness _employeeBusiness;
        private SchoolBusiness _schoolBusiness;
        public EmployeeController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._roleBusiness = new SchoolRoleBusiness(_df, this._unitOfWork);
            this._employeeBusiness = new EmployeeBusiness(_df, this._unitOfWork);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);

        }


        //
        // GET: /Employee/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Employee()
        {
            EmployeeViewModel empViewModel = new EmployeeViewModel();
            var schoolList1 = _deptBusiness.GetListWT();
            empViewModel.deptList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.DepartmentName.ToString(),
                Value = x.DeptId.ToString()
            }).ToList();

            var schoolList2 = _roleBusiness.GetListWT();
            empViewModel.roleList = schoolList2.Select(x => new SelectListItem
            {
                Text = x.RoleName.ToString(),
                Value = x.RoleId.ToString()
            }).ToList();
            var schoolList3 = _schoolBusiness.GetListWT();
            empViewModel.schoollList = schoolList3.Select(x => new SelectListItem
            {
                Text = x.SchoolName.ToString(),
                Value = x.SchoolID.ToString()
            }).ToList();

            List<SelectListItem> ListUtyp = new List<SelectListItem>();
            ListUtyp.Add(new SelectListItem { Text = "Permanent", Value = "Permanent" });
            ListUtyp.Add(new SelectListItem { Text = "Contract", Value = "Contract" });

            empViewModel.typeList = ListUtyp;

            return View(empViewModel);

        }

        public JsonResult SaveDataInDatabase(EmployeeViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.EmpId > 0)
            {
                var userList = _employeeBusiness.GetListWT(x => x.EmployeeID == clsmodel.EmpId).ToList().FirstOrDefault();

               // userList.DeptId = clsmodel.DeptId;
                //userList.DepartmentName = clsmodel.DepartmentName;

                bool isSuccess = _employeeBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<EmployeeViewModel, Employee>();
                Employee cls = Mapper.Map<EmployeeViewModel, Employee>(clsmodel);
               // cls.SchoolId = 0;
                cls.IsDelete = "false";
                bool isSuccess = _employeeBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetallStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _employeeBusiness.GetListWT();
            List<EmployeeViewModel> userViewModelList = new List<EmployeeViewModel>();
            var dept = _deptBusiness.GetListWT();
            var school = _schoolBusiness.GetListWT();
            var records = (from p in userList 
                           join d in dept on p.DeptId equals d.DeptId 
                           join s in school on p.SchoolId equals s.SchoolID
                           select new EmployeeViewModel
                           {
                               Department = d.DepartmentName,
                               Firstname = p.Firstname,
                               EmployeeType = p.Employeetype,
                               School = s.SchoolName,
                               IsDelete = p.IsDelete,

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _employeeBusiness.GetListWT();
            List<EmployeeViewModel> userViewModelList = new List<EmployeeViewModel>();
            var dept = _deptBusiness.GetListWT();
            //var school = _schoolBusiness.GetListWT();
            var records = (from p in userList
                           join d in dept on p.DeptId equals d.DeptId
                           
                           select new EmployeeViewModel
                           {
                               Department = d.DepartmentName,
                               Firstname = p.Firstname,
                               EmployeeType = p.Employeetype,
                               //School = s.SchoolName,
                              // IsDelete = p.IsDelete,

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _employeeBusiness.GetListWT(x => x.EmpId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _employeeBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _employeeBusiness.GetListWT(x => x.EmployeeID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }
	}
}