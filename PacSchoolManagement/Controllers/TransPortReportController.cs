﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class TransPortReportController : Controller
    {
        // GET: TransPortReport



        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private StudentBusiness _studentBusiness;
        private ClassBusiness _classBusiness;
        private SectionBusiness _sectionBusiness;
        private StudentDocumentBusiness _studentdocumentBusiness;



        public TransPortReportController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._studentdocumentBusiness = new StudentDocumentBusiness(_df, this._unitOfWork);
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult TransPortReport()
        {
            TransPortReportViewModel report=new TransPortReportViewModel();

            var classList1 = _classBusiness.GetListWT();
           report .ClassList = classList1.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();

           var section = _sectionBusiness.GetListWT();
           report.SectionList = section.Select(x => new SelectListItem
           {
               Text = x.SectionName.ToString(),
               Value = x.SectionID.ToString()
           }).ToList();




            return View(report);
       
        
        
        
        }
    }
}