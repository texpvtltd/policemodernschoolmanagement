﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class LeaveDetailController : Controller
    {

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private LeaveCategoryBusiness _categorybusiness;
        // GET: LeaveCategory
        private LeaveDetailBusiness _leavedetailbusiness;
        public LeaveDetailController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._categorybusiness = new LeaveCategoryBusiness(_df,this._unitOfWork);
            this._leavedetailbusiness = new LeaveDetailBusiness(_df, this._unitOfWork);
        }



        // GET: LeaveDetail
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeaveDetailList()
        {
           LeaveDetailViewModel  leavedetail = new LeaveDetailViewModel();
            List<SelectListItem> degitnation = new List<SelectListItem>();
            degitnation.Add(new SelectListItem { Text = "Teacher", Value = "Teacher" });
            degitnation.Add(new SelectListItem { Text = "Worker", Value = "Bad" });
            degitnation.Add(new SelectListItem { Text = "Clark", Value = "Clark" });



            leavedetail.DegitnationList = degitnation;
            var category = _categorybusiness.GetListWT();
            leavedetail.LeaveCategoryList = category.Select(x => new SelectListItem
            {
                Text = x.CategoryName.ToString(),
                Value = x.LeaveCatId.ToString()
            }).ToList();



            return View(leavedetail);
        }




        public JsonResult SaveDataInDatabase(LeaveDetailViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.LeaveDetailId > 0)
            {
                var userList = _leavedetailbusiness.GetListWT(x => x.LeaveDetailId == clsmodel.LeaveDetailId).ToList().FirstOrDefault();

                userList.LeaveDetailId = clsmodel.LeaveDetailId;
                userList.LeaveCategory = clsmodel.LeaveCategory;
                userList.LeaveCount = clsmodel.LeaveCount;
                userList.Degitnation = clsmodel.Degitnation;
                

                bool isSuccess = _leavedetailbusiness.AddUpdateDeleteAddLeaveDetail(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<LeaveDetailViewModel, LeaveDetail>();
                LeaveDetail cls = Mapper.Map<LeaveDetailViewModel, LeaveDetail>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _leavedetailbusiness.AddUpdateDeleteAddLeaveDetail(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _leavedetailbusiness.GetListWT();

            var records = (from p in userList
                           select new LeaveDetailViewModel
                           {
                               LeaveDetailId = p.LeaveDetailId,
                               Degitnation = p.Degitnation,
                               LeaveCategory = p.LeaveCategory,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,
                            LeaveCount=p.LeaveCount,
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _leavedetailbusiness.GetListWT(x => x.LeaveDetailId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _leavedetailbusiness.AddUpdateDeleteAddLeaveDetail(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _leavedetailbusiness.GetListWT(x => x.LeaveDetailId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }














    }
}