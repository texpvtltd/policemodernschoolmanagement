﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class BatchController : Controller
    {

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
   
        private UserLoginBusiness _userBusiness;
        private CourseBusiness _courseBusiness;
        private BatchBusiness _batchbusiness;

          public BatchController()
        {
            this._unitOfWork = new UnitOfWork(_df);
          
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._courseBusiness = new CourseBusiness(_df, this._unitOfWork);
            this._batchbusiness = new BatchBusiness(_df, this._unitOfWork);


        }
        //
        // GET: /Batch/
          [HttpGet]
          public ActionResult Batch()
          {
              var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
               var schid = _userBusiness.Find(currentUserId).SchoolID.ToString();
              BatchViewModel viewmodel = new BatchViewModel();
           
              var schoolList1 = _courseBusiness.GetListWT().Where(x=>x.SchoolId== schid);
              viewmodel.CourseList = schoolList1.Select(x => new SelectListItem
              {
                  Text = x.CourseName.ToString(),
                  Value = x.CourseId.ToString()
              }).ToList();

              return View(viewmodel);


          }
          [HttpPost]
          public ActionResult Batch(string s)
          {

              return View();

          }
        // POST: /Class/Create
          // POST: /Class/Create
          public JsonResult SaveDataInDatabase(BatchViewModel clsmodel)
          {
              //HttpPostedFileBase fu = Request.Files["file"];
              //HttpPostedFileBase file1 = clsmodel.Logo;

              // file = clsmodel.ImageUpload;
              int schoolid = Convert.ToInt32(clsmodel.SchoolID);
              if (schoolid > 0)
              {
                  var BatchList = _batchbusiness.GetListWT().ToList().FirstOrDefault();

                  BatchList.SchoolID = clsmodel.SchoolID;
                  BatchList.CourseId = clsmodel.CourseId;
                  BatchList.BatchName = clsmodel.BatchName;
                  BatchList.SessionId = clsmodel.SessionId;
                  BatchList.MaximumStudents = clsmodel.MaximumStudents;
                  BatchList.EndDate = clsmodel.EndDate;
                  BatchList.StartDate = clsmodel.StartDate;
                
                  BatchList.IsDelete = "false";

                  bool isSuccess = _batchbusiness.AddUpdateDeleteClass(BatchList, "U");
                  if (isSuccess)
                  {
                      TempData["Success"] = "Updated Successfully!!";
                      TempData["isSuccess"] = "true";
                      // return RedirectToAction("Index");
                  }
                  else
                  {
                      TempData["Success"] = "Failed to create Brand!!";
                      TempData["isSuccess"] = "false";
                  }
              }



              else
              {

                  var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                  var schid = _userBusiness.Find(currentUserId).SchoolID;

                  Mapper.CreateMap<BatchViewModel, Batch>();
                  Batch cls = Mapper.Map<BatchViewModel, Batch>(clsmodel);
                  cls.SchoolID = schid;
                  cls.IsDelete = "false";
                  cls.SessionId = Convert.ToInt32( Filters.AuthenticationModel.GlobalUser.getGlobalUser().Session);
                  //  cls.SessionID = Convert.ToInt32(currentUserId);
                  // cls.TokenKey = GlobalMethods.GetToken();

                  bool isSuccess = _batchbusiness.AddUpdateDeleteClass(cls, "I");
                  if (isSuccess)
                  {
                      TempData["Success"] = "School Created Successfully!!";
                      TempData["isSuccess"] = "true";
                      // return RedirectToAction("Index");
                  }
                  else
                  {
                      TempData["Success"] = "Failed to create School!!";
                      TempData["isSuccess"] = "false";
                  }
                  // result = true;

                  //  }
              }
              return Json(JsonRequestBehavior.AllowGet);
          }
          public JsonResult GetStudentList()  //Gets the todo Lists.
          {
                 var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                  var schid = _userBusiness.Find(currentUserId).SchoolID.ToString();

              var BatchList = _batchbusiness.GetListWT(x => x.IsDelete == "false");
              List<BatchViewModel> courseViewModelList = new List<BatchViewModel>();
              var course = _courseBusiness.GetListWT().Where(x => x.SchoolId == schid);
              var records = (from p in BatchList

                             join r in course on p.CourseId equals r.CourseId
                             select new BatchViewModel
                             {
                                 BatchName = p.BatchName,
                                 BatchId = p.BatchId,
                                 CourseId = p.CourseId,
                                 CourseName = r.CourseName,
                                 EndDate = p.EndDate,

                                 // ReadConfigData.GetAppSettingsValue("CMSUrl") + "/SliderImage/" + c.SliderImage
                                 MaximumStudents = p.MaximumStudents,
                                 StartDate = p.StartDate,
                                 SchoolID = p.SchoolID,
                               
                                 SessionId = p.SessionId,
                                 

                             }).AsQueryable();


              return Json(records, JsonRequestBehavior.AllowGet);
          }

          public JsonResult DeleteStudentRecord(int StudentId)
          {
              var userList = _batchbusiness.GetListWT(x => x.BatchId == StudentId).ToList().FirstOrDefault();

              userList.IsDelete = "false";

              bool isSuccess = _batchbusiness.AddUpdateDeleteClass(userList, "U");
              if (isSuccess)
              {
                  TempData["Success"] = "Updated Successfully!!";
                  TempData["isSuccess"] = "true";
                  // return RedirectToAction("Index");
              }
              else
              {
                  TempData["Success"] = "Failed to create Brand!!";
                  TempData["isSuccess"] = "false";
              }
              return Json(userList, JsonRequestBehavior.AllowGet);
          }

          public JsonResult GetStudentById(int StudentId)
          {
              var model = _batchbusiness.GetListWT(x => x.BatchId == StudentId).ToList().FirstOrDefault();
              string value = string.Empty;
              value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
              {
                  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
              });
              return Json(value, JsonRequestBehavior.AllowGet);
          }


	}
}