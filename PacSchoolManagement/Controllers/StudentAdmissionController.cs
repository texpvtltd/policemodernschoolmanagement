﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class StudentAdmissionController : Controller
    {
        // GET: StudentAdmission

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public StudentAdmissionBusiness _saBusiness;
        public StudentAdmissionController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._saBusiness = new StudentAdmissionBusiness(_df, this._unitOfWork);

        }

        // GET: Teacher
        [HttpGet]
        public ActionResult StudentAdmission()
        {
            StudentAdmissionViewModel schooluserViewModel = new StudentAdmissionViewModel();
            var StudentAdmissionList1 = _saBusiness.GetListWT();
            schooluserViewModel.StudentAdmissionList = StudentAdmissionList1.Select(x => new SelectListItem
            {
                Text = x.Name.ToString(),
                Value = x.AddmissionID.ToString()
            }).ToList();
            return View();

        }
        [HttpPost]
        public ActionResult StudentAdmission(string s)
        {

            return View();

        }


        public ActionResult GetStudentIdCard1(int StudentId)
        {
            StudentAdmissionViewModel stu = new StudentAdmissionViewModel();
            var model1 = _saBusiness.GetListWT(x => x.AddmissionID == StudentId).ToList().FirstOrDefault();

            stu.SchoolID = model1.SchoolID;
            stu.Name = model1.Name;
            stu.SessionId = model1.SessionId;
            stu.FatherName = model1.FatherName;
            stu.Gender = model1.Gender;

            return View(stu);

            // return View(stu);
        }





        //
        // POST: /Class/Create
        public JsonResult SaveDataInDatabase(StudentAdmissionViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.AddmissionID > 0)
            {
                var userList = _saBusiness.GetListWT(x => x.AddmissionID == clsmodel.AddmissionID).ToList().FirstOrDefault();

                userList.SchoolID = clsmodel.SchoolID;
                userList.FatherName = clsmodel.FatherName;
                userList.Name = clsmodel.Name;
                userList.AppliedDate = clsmodel.AppliedDate;
                userList.Class = userList.Class;
                userList.DateofBirth = userList.DateofBirth;
                userList.Gender = userList.Gender;
                userList.MobileNo = userList.MobileNo;
                userList.SchoolID = userList.SchoolID;
                userList.SessionId = userList.SessionId;
                userList.Status = userList.Status;

                bool isSuccess = _saBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;

                Mapper.CreateMap<StudentAdmissionViewModel, StudentAdmission>();
                StudentAdmission cls = Mapper.Map<StudentAdmissionViewModel, StudentAdmission>(clsmodel);
                clsmodel.SchoolID = Convert.ToInt32(currentUserId);
                // cls.TokenKey = GlobalMethods.GetToken();
                bool isSuccess = _saBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {


            var userList = _saBusiness.GetListWT();
            List<StudentAdmissionViewModel> userViewModelList = new List<StudentAdmissionViewModel>();
            int SchoolId = Convert.ToInt32(GlobalUser.getGlobalUser().SchoolID);
            var records = (from p in userList
                           select new StudentAdmissionViewModel
                           {
                               AddmissionID = p.AddmissionID,
                               FatherName = p.FatherName,
                               AppliedDate = p.AppliedDate,
                               Class = p.Class,
                               DateofBirth = p.DateofBirth,
                               Name = p.Name,
                               Gender = p.Gender,
                               MobileNo = p.MobileNo,
                               RegdNo = p.RegdNo,
                               SessionId = p.SessionId,
                               Status = p.Status

                           }).Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }



        public JsonResult DeleteStudentRecord(int StudentId)
        {
            string JsonStr = "";
            bool isSuccess = true;
            string message = "Delete Successful!";
            _unitOfWork.BeginTransaction();

            //var Stu = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();
            if (StudentId != null)
            {
                var br = _saBusiness.Find(StudentId);
                _saBusiness.Delete(StudentId);
                _unitOfWork.SaveChanges();

            }
            //List<Class> userList = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList();
            //foreach (Class usr in userList)
            //{
            //    int deleteId = Convert.ToInt32(usr.ClassID);
            //   // var br = _classBusiness.Find(deleteId);
            //    _classBusiness.Deleteid(deleteId);
            //    _unitOfWork.SaveChanges();
            //}

            _unitOfWork.Commit();



            TempData["Success"] = message;
            TempData["isSuccess"] = isSuccess.ToString();

            JsonStr = "{\"message\":\"" + message + "\",\"isSuccess\":\"" + isSuccess + "\"}";
            return Json(JsonStr, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _saBusiness.GetListWT(x => x.AddmissionID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetStudentIdCard(int StudentId)
        {
            var model1 = _saBusiness.GetListWT(x => x.AddmissionID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model1, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);




            //var userList = _saBusiness.GetListWT();
            // List<StudentAdmissionViewModel> userViewModelList = new List<StudentAdmissionViewModel>();



            //  return Json(model1, JsonRequestBehavior.AllowGet);

        }


       








    }
}
