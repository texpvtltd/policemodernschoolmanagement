﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;


namespace PacSchoolManagement.Controllers
{
    public class TransportAllocationController : Controller
    {
        // GET: TransportAllocation

           private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private AddTransportBusiness _addtranport;
        // GET: AddDriver
        private AddDriverBusiness _adddriverbusiness;
        private TransPortAllocationBusiness _transportallocationBusiness;
        //private AddRauteBusiness _addrautebusiness;
        public TransportAllocationController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._addtranport = new AddTransportBusiness(_df, this._unitOfWork);
            this._adddriverbusiness = new AddDriverBusiness(_df, this._unitOfWork);
            this._transportallocationBusiness = new TransPortAllocationBusiness(_df, this._unitOfWork);
        //    this._addrautebusiness = new AddRauteBusiness(_df, this._unitOfWork);
        }





        public ActionResult Index()
        {
            return View();
        }
        public ActionResult  TransportAllocationList()
        {




            TransportAllocationViewModel transpotAllocationviewmodel = new TransportAllocationViewModel();
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            transpotAllocationviewmodel.UserTypeList = userytype;


          //  var raute = _addrautebusiness.GetListWT();
            //transpotAllocationviewmodel.RouteCodeList = raute.Select(x => new SelectListItem
            //{
            //    Text = x.RouteCode.ToString(),
            //    Value = x.RouteID.ToString()
            //}).ToList();

            return View(transpotAllocationviewmodel);
        }

        public JsonResult SaveDataInDatabase(TransportAllocationViewModel clsmodel)
        {
            // var result = false;
            //if (clsmodel.ClassID > 0)
            //{  

            if (clsmodel.AllocationId > 0)
            {
                var userList = _transportallocationBusiness.GetListWT(x => x.AllocationId == clsmodel.AllocationId).ToList().FirstOrDefault();
                userList.AllocationId = clsmodel.AllocationId;
                userList.Destination = clsmodel.Destination;
                userList.EndFrequency = clsmodel.EndFrequency;

                userList.Route = clsmodel.Route;
                userList.Type= clsmodel.Type;


                bool isSuccess = _transportallocationBusiness.AddUpdateDeleteTransportAllocation(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create updated!!";
                    TempData["isSuccess"] = "false";
                }
            }


            else
            {


                var currentUserId = GlobalUser.getGlobalUser().UserId;

                Mapper.CreateMap<TransportAllocationViewModel, TransportAllocation>();
                TransportAllocation cls = Mapper.Map<TransportAllocationViewModel, TransportAllocation>(clsmodel);
                cls.IsDelete = "false";
                cls.SchoolId = currentUserId.ToString();


                bool isSuccess = _transportallocationBusiness.AddUpdateDeleteTransportAllocation(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Transport allocation Added Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Transport allocation to add principal!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;
            }
            //  }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrinciList()  //Gets the todo Lists.
        {


            var userList = _transportallocationBusiness.GetListWT(x => x.IsDelete == "false"); ;
            List<DriverViewModel> userViewModelList = new List<DriverViewModel>();

            var records = (from p in userList
                           select new TransportAllocationViewModel
                           {
                               Destination = p.Destination,
                               AllocationId = p.AllocationId,
                               EndFrequency = p.EndFrequency,
                               Route = p.Route,
                               StartFrequency = p.StartFrequency,
                               Type=p.Type,
                               
                              
                              

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _transportallocationBusiness.GetListWT(x => x.AllocationId== StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _transportallocationBusiness.GetListWT(x => x.AllocationId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _transportallocationBusiness.AddUpdateDeleteTransportAllocation(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }







    }
}