﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class LeaveApplicationController : Controller
    {
        // GET: LeaveApplication




          private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private LeaveCategoryBusiness _categorybusiness;
        // GET: LeaveCategory
        private LeaveDetailBusiness _leavedetailbusiness;
        public TeacherBusiness _teacherBusiness;
        private LeaveCarryForwordDetailBusiness _carryforwordbusiness;
        private LeaveApplicationBusiness _leaveapplicationbusiness;
        public LeaveApplicationController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._categorybusiness = new LeaveCategoryBusiness(_df,this._unitOfWork);
            this._leavedetailbusiness = new LeaveDetailBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._carryforwordbusiness = new LeaveCarryForwordDetailBusiness(_df, this._unitOfWork);
            this._leaveapplicationbusiness = new LeaveApplicationBusiness(_df, this._unitOfWork);
        }








        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ApplicationList()
        {
            LeaveApplicationViewModel leaveappliction=new LeaveApplicationViewModel();
            var category = _categorybusiness.GetListWT();
            leaveappliction.LeaveCategoryList = category.Select(x => new SelectListItem
            {
                Text = x.CategoryName.ToString(),
                Value = x.LeaveCatId.ToString()
            }).ToList();
            List<SelectListItem> status = new List<SelectListItem>();
            status.Add(new SelectListItem { Text = "Approved", Value = "Approved" });
            status.Add(new SelectListItem { Text = "Rejected", Value = "Rejected" });


            leaveappliction.LeaveStatusList = status;
            return View(leaveappliction);
        }

        public JsonResult SaveDataInDatabase(LeaveApplicationViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.ApplicatId > 0)
            {
                var userList = _leaveapplicationbusiness.GetListWT(x => x.ApplicatId == clsmodel.ApplicatId).ToList().FirstOrDefault();

                userList.ApplicatId = clsmodel.ApplicatId;
                userList.appduty = clsmodel.appduty;
                userList.LeaveType = clsmodel.LeaveType;
                userList.FormDate = clsmodel.FormDate;
                userList.Reason = clsmodel.Reason;
                userList.UaserName = clsmodel.UaserName;
                userList.Status = clsmodel.Status;



                bool isSuccess = _leaveapplicationbusiness.AddUpdateDeleteAddAddLeaveApplication(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<LeaveApplicationViewModel, AddLeaveApplication>();
                AddLeaveApplication cls = Mapper.Map<LeaveApplicationViewModel, AddLeaveApplication>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
               
                cls.ISDelete = "false";
                if(clsmodel.appduty=="null")
                {

                    cls.appduty = "Off";
                }
                else
                {
                    cls.appduty = clsmodel.appduty;
                }
                bool isSuccess = _leaveapplicationbusiness.AddUpdateDeleteAddAddLeaveApplication(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _leaveapplicationbusiness.GetListWT();

            var records = (from p in userList
                           select new LeaveApplicationViewModel
                           {
                               ApplicatId = p.ApplicatId,
                               appduty = p.appduty,
                               LeaveType = _categorybusiness.GetUserById(Convert.ToInt32(p.LeaveType)).CategoryName,
                               SchoolId = p.SchoolId,
                               ISDelete = p.ISDelete,
                               Reason = p.Reason,
                               Status = p.Status,
                               FormDate=p.FormDate,
                              ToDate=p.ToDate,
                               // Employee = _teacherBusiness.GetUserById(Convert.ToInt32(p.Employee)).FName,
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _leaveapplicationbusiness.GetListWT(x => x.ApplicatId == StudentId).ToList().FirstOrDefault();

            userList.ISDelete = "fales";

            bool isSuccess = _leaveapplicationbusiness.AddUpdateDeleteAddAddLeaveApplication(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _leaveapplicationbusiness.GetListWT(x => x.ApplicatId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OneDutyList()
        {


            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _leaveapplicationbusiness.GetListWT(x => x.appduty =="on").ToList();

            var records = (from p in userList
                           select new LeaveApplicationViewModel
                           {
                               ApplicatId = p.ApplicatId,
                               appduty = p.appduty,
                               LeaveType = _categorybusiness.GetUserById(Convert.ToInt32(p.LeaveType)).CategoryName,
                               SchoolId = p.SchoolId,
                               ISDelete = p.ISDelete,
                               Reason = p.Reason,
                               Status = p.Status,
                               FormDate = p.FormDate,
                               ToDate = p.ToDate,
                               // Employee = _teacherBusiness.GetUserById(Convert.ToInt32(p.Employee)).FName,
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();





            return View(records);
        }
        public ActionResult ApprovelList()
        {


            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _leaveapplicationbusiness.GetListWT(x => x.appduty == "Approved").ToList();

            var records = (from p in userList
                           select new LeaveApplicationViewModel
                           {
                               ApplicatId = p.ApplicatId,
                               appduty = p.appduty,
                               LeaveType = _categorybusiness.GetUserById(Convert.ToInt32(p.LeaveType)).CategoryName,
                               SchoolId = p.SchoolId,
                               ISDelete = p.ISDelete,
                               Reason = p.Reason,
                               Status = p.Status,
                               FormDate = p.FormDate,
                               ToDate = p.ToDate,
                               // Employee = _teacherBusiness.GetUserById(Convert.ToInt32(p.Employee)).FName,
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();





            return View(records);
        }
















    }
}