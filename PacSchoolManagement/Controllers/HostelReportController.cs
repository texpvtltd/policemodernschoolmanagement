﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class HostelReportController : Controller
    {


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostalTypeBusiness _hostaltypeBusiness;
        private HostalDetailBusiness _hostaldetailBusiness;
        private HostelAllocationBusiness _hosteAllbusiness;
        private AddHostelRoomBusiness _hostelRoomBusiness;
        public TeacherBusiness _teacherBusiness;
        private StudentBusiness _studentBusiness;
        public HostelTransferBusiness _hosteltransferbusiness;
        private TFeesCollection _tfeesCollection;
        public HostelFeesCollectionBusiness _feescollectionbusiness;
        private HostelVisitorBusiness _hostelvisitorBusiness;
       // public HostelAllocationBusiness _hosteAllbusiness;
        public HostelReportController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this ._hostaltypeBusiness=new HostalTypeBusiness (_df,this._unitOfWork );
            this._hostaldetailBusiness = new HostalDetailBusiness(_df, this._unitOfWork);
        //    this._hosteAllbusiness = new HostelAllocationBusiness(_df, this._unitOfWork);
            this._hostelRoomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);

            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._hosteltransferbusiness = new HostelTransferBusiness(_df, this._unitOfWork);
            this._tfeesCollection = new TFeesCollection(_df, this._unitOfWork);
            this._feescollectionbusiness = new HostelFeesCollectionBusiness(_df, this._unitOfWork);
            this._hostelvisitorBusiness = new HostelVisitorBusiness(_df, this._unitOfWork);

            this._hosteAllbusiness = new HostelAllocationBusiness(_df, this._unitOfWork);
        }





        // GET: HostelReport
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HostelDetailReport()
        {


            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _hostaldetailBusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostalDetailViewModel
                           {
                               SchoolId = p.SchoolId,
                               HostalAddress = p.HostalAddress,
                               HostalContact = p.HostalContact,
                               // Hostaltyp = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostalId = p.HostalId,
                               HostalType = p.HostalType,
                               HostalName = p.HostalName,
                               WardanAddress = p.WardanAddress,

                               WardenName = p.WardenName,
                               WardenPhoneNo = p.WardenPhoneNo,

                           });
            // .Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


         

            return View(records);
        }


       


        public ActionResult HostelfeesCollectionReport()
        {

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _feescollectionbusiness.GetListWT();

            var records = (from p in userList
                           select new HostelFeeCollectionViewModel
                           {
                               Amount = p.Amount,
                               HostelType = p.HostelType,
                               // SchoolId = p.SchoolId,
                               // IsDelete = p.IsDelete,
                               Recipt = p.Recipt,
                               UserName = p.UserName,
                               BankAccountNo = p.BankAccountNo,
                               BankName = p.BankName,
                               CheaqueDate = p.CheaqueDate,

                               ChequeNo = p.ChequeNo,
                               Discount = p.Discount,
                               FeesId = p.FeesId,
                               Fine = p.Fine,

                               ModeOfPay = p.ModeOfPay,
                               ReciptNo = p.ReciptNo,
                               TotalAmount = p.TotalAmount,


                           }).AsQueryable();
            //.Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();





            return View(records);



            
        }

        public ActionResult HostelTransferReport()
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _hosteltransferbusiness.GetListWT();


            var records = (from p in userList
                           select new HostelTransferViewModel
                           {
                               HostelRoom = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).RoomNo,
                               HostelType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostelType)).HostalType1,
                               SchoolID = p.SchoolID,
                               IsDelete = p.IsDelete,
                               HostelName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostalName,

                               UserName = p.UserName,
                               TransferId = p.TransferId,
                               Status = p.Status,

                           }).Where(i => i.SchoolID == SchoolId.ToString()).AsQueryable();





            return View(records);
        }


        public ActionResult HostelVisitorReport()
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _hostelvisitorBusiness.GetListWT();
            List<DeptViewModel> userViewModelList = new List<DeptViewModel>();

            var records = (from p in userList
                           select new HostelVisitorViewModel
                           {
                               VisitorId = p.VisitorId,
                               Hosteltype = p.Hosteltype,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,
                               Relation = p.Relation,
                               UserName = p.UserName,
                               VisitTime = p.VisitTime,
                               VisitorName = p.VisitorName,
                               VisitDate = p.VisitDate,

                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();





            return View(records);


            
        }

        public ActionResult HostelAllocationReport()
        {






            // int SchoolId = Convert.ToInt32(GlobalUser.getGlobalUser().SchoolID);
            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _hosteAllbusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostelAllocationViewModel
                           {
                               SchoolId = p.SchoolId,
                               // HostalType = p.HostalType,
                               HostelAllocatedId = p.HostelAllocatedId,
                               HostalType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostelName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostalName,
                               HostelRegdate = p.HostelRegdate,
                               HostelRoom = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).RoomNo,
                               // HostelRoom = p.HostelRoom,
                               VacatingDate = p.VacatingDate,
                               FloorName = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).HostalfloorName,
                               UserType = p.UserType,
                               UsarName = p.UsarName,

                           });
            // .Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return View(records);

           
        }


    }
}