﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class LeaveCategoryController : Controller
    {


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private LeaveCategoryBusiness _categorybusiness;
        // GET: LeaveCategory

        public LeaveCategoryController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._categorybusiness = new LeaveCategoryBusiness(_df,this._unitOfWork);
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeaveCategoryList()
        {
           

            return View();
        }
        public JsonResult SaveDataInDatabase(LeaveCategoryViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.LeaveCatId > 0)
            {
                var userList = _categorybusiness.GetListWT(x => x.LeaveCatId == clsmodel.LeaveCatId).ToList().FirstOrDefault();

                userList.LeaveCatId = clsmodel.LeaveCatId;
                userList.CategoryName = clsmodel.CategoryName;

                bool isSuccess = _categorybusiness.AddUpdateDeleteAddLeaveCategory(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<LeaveCategoryViewModel, LeaveCategory>();
                LeaveCategory cls = Mapper.Map<LeaveCategoryViewModel, LeaveCategory>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _categorybusiness.AddUpdateDeleteAddLeaveCategory(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _categorybusiness.GetListWT().Where(x=>x.IsDelete== "false");
           
            var records = (from p in userList
                           select new  LeaveCategoryViewModel
                           {
                              LeaveCatId = p.LeaveCatId,
                               CategoryName = p.CategoryName,
                               CarryStatus=p.CarryStatus,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,

                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _categorybusiness.GetListWT(x => x.LeaveCatId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _categorybusiness.AddUpdateDeleteAddLeaveCategory(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _categorybusiness.GetListWT(x => x.LeaveCatId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }




    }
}