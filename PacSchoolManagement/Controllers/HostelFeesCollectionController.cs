﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class HostelFeesCollectionController : Controller
    {
        // GET: HostelFeesCollection



        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostelVisitorBusiness _hostelvisitorBusiness;
        private StudentBusiness _studentBusiness;
        private AddHostelRoomBusiness _roomBusiness;
        private HostelAllocationBusiness _allocationBusiness;
        public TeacherBusiness _teacherBusiness;
        public HostelFeesCollectionBusiness _feescollectionbusiness;
        public HostelFeesCollectionController()
        {
           


            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._hostelvisitorBusiness = new HostelVisitorBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._roomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
            this._allocationBusiness = new HostelAllocationBusiness(_df, this._unitOfWork);

            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);

            this._feescollectionbusiness = new HostelFeesCollectionBusiness(_df, this._unitOfWork);
        }













        public ActionResult Index()
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _feescollectionbusiness.GetListWT();

            var records = (from p in userList
                           select new HostelFeeCollectionViewModel
                           {
                               Amount = p.Amount,
                               HostelType = p.HostelType,
                               // SchoolId = p.SchoolId,
                               // IsDelete = p.IsDelete,
                               Recipt = p.Recipt,
                               UserName = p.UserName,
                               BankAccountNo = p.BankAccountNo,
                               BankName = p.BankName,
                               CheaqueDate = p.CheaqueDate,

                               ChequeNo = p.ChequeNo,
                               Discount = p.Discount,
                               FeesId = p.FeesId,
                               Fine = p.Fine,

                               ModeOfPay = p.ModeOfPay,
                               ReciptNo = p.ReciptNo,
                               TotalAmount = p.TotalAmount,


                           }).AsQueryable();
                           //.Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();





            return View(records);











          
        }


           public ActionResult HostelFeeCollectionList()
        {
            HostelFeeCollectionViewModel collection = new HostelFeeCollectionViewModel();
            List<SelectListItem> mode = new List<SelectListItem>();
            mode.Add(new SelectListItem { Text = "Online", Value = "OnLine" });
            mode.Add(new SelectListItem { Text = "Cheque", Value = "Cheque" });
            mode.Add(new SelectListItem { Text = "Case", Value = "Case" });



            collection.ModeOfPayList = mode;
            var student = _studentBusiness.GetListWT();
            collection.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();
            var Emploeyee = _teacherBusiness.GetListWT();
            collection.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName + "" + x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();

            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            collection.UserTypeList = userytype;


            return View(collection);

          
        }


           public JsonResult SaveDataInDatabase(HostelFeeCollectionViewModel clsmodel)
           {
               // var result = false;
               if (clsmodel.FeesId > 0)
               {
                   var userList = _feescollectionbusiness.GetListWT(x => x.FeesId == clsmodel.FeesId).ToList().FirstOrDefault();
                   userList.Recipt = clsmodel.Recipt;
                   //userList. = clsmodel.SchoolId;
                   userList.Amount = clsmodel.Amount;
                   userList.BankAccountNo = clsmodel.BankAccountNo;
                   userList.BankName = clsmodel.BankName;
                   userList.CheaqueDate = clsmodel.CheaqueDate;
                   userList.HostelType = clsmodel.HostelType;
                   userList.UserType = clsmodel.UserType;
                   userList.HostelName = clsmodel.HostelName;
                   userList.UserName = clsmodel.UserName;

                   bool isSuccess = _feescollectionbusiness.AddUpdateDeleteClass(userList, "U");
                   if (isSuccess)
                   {
                       TempData["Success"] = "Updated Successfully!!";
                       TempData["isSuccess"] = "true";
                       // return RedirectToAction("Index");
                   }
                   else
                   {
                       TempData["Success"] = "Failed to create Brand!!";
                       TempData["isSuccess"] = "false";
                   }
               }
               else
               {


                   var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                   Mapper.CreateMap<HostelFeeCollectionViewModel,HostelFeesCollection>();
                   HostelFeesCollection cls = Mapper.Map<HostelFeeCollectionViewModel,HostelFeesCollection>(clsmodel);
                 //  cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                  // cls.IsDelete = "false";
                   bool isSuccess = _feescollectionbusiness.AddUpdateDeleteClass(cls, "I");
                   if (isSuccess)
                   {
                       TempData["Success"] = "Update Created Successfully!!";
                       TempData["isSuccess"] = "flase";
                       // return RedirectToAction("Index");
                   }
                   else
                   {
                       TempData["Success"] = "Failed to create Brand!!";
                       TempData["isSuccess"] = "false";
                   }
                   // result = true;

               }

               return Json(clsmodel, JsonRequestBehavior.AllowGet);
           }

           public ActionResult EditFees(int txtval)
           {
               HostelFeeCollectionViewModel clsmodel = new HostelFeeCollectionViewModel();


               var student = _studentBusiness.GetListWT();
               clsmodel.StudentList = student.Select(x => new SelectListItem
               {
                   Text = x.FirstName + "" + x.LastName.ToString(),
                   Value = x.StudentID.ToString(),
               }).ToList();
               var Emploeyee = _teacherBusiness.GetListWT();
               clsmodel.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
               {
                   Text = x.FName + "" + x.LName.ToString(),
                   Value = x.UserID.ToString(),
               }).ToList();
               List<SelectListItem> userytype = new List<SelectListItem>();
               userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
               userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

               clsmodel.UserTypeList = userytype;


               var userList = _feescollectionbusiness.GetListWT(x => x.FeesId == txtval).ToList().FirstOrDefault();
               clsmodel.TotalAmount = userList.TotalAmount;
               clsmodel.SchoolId = userList.SchoolId;
               clsmodel.UserName = userList.UserName;
               clsmodel.UserType = userList.UserType;
               clsmodel.RoomNo = userList.RoomNo;
               clsmodel.ReciptNo = userList.ReciptNo;
               clsmodel.Discount = userList.Discount;
               clsmodel.FeeType = userList.FeeType;
               clsmodel.Fine = userList.Fine;
               clsmodel.HostelName = userList.HostelName;
               clsmodel.ModeOfPay = userList.ModeOfPay;
               clsmodel.Recipt = userList.Recipt;
               //clsmodel.Remark = userList.Rma;
               clsmodel.Recipt = userList.Recipt;



               return View(clsmodel);

           }

           public ActionResult Delete(int txtval)
           {
               var userList = _feescollectionbusiness.GetListWT(x => x.FeesId == txtval).ToList().FirstOrDefault();

            //   userList.IsDelete = "fales";

               bool isSuccess = _feescollectionbusiness.AddUpdateDeleteClass(userList, "U");
               if (isSuccess)
               {
                   TempData["Success"] = "Delete Successfully!!";
                   TempData["isSuccess"] = "true";
                   // return RedirectToAction("Index");
               }
               else
               {
                   TempData["Success"] = "Not Delete Successfully!!";
                   TempData["isSuccess"] = "true";
               }
               return RedirectToAction("Index", "HostelFeesCollection");
           }

     






















    }
}