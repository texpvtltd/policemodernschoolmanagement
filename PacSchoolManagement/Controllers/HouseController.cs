﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class HouseController : Controller
    {
        //
        // GET: /House/

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private HouseBusiness _houseBusiness;
          public HouseController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._houseBusiness = new HouseBusiness(_df, this._unitOfWork);

        }
  

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult House()
        {





            return View();
        }
        public JsonResult SaveDataInDatabase(HouseViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.HouseID > 0)
            {
                var userList = _houseBusiness.GetListWT(x => x.HouseID== clsmodel.HouseID).ToList().FirstOrDefault();

                userList.HouseID = clsmodel.HouseID;
                userList.HouseName = clsmodel.HouseName;
                bool isSuccess = _houseBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;

                Mapper.CreateMap<HouseViewModel, House>();
               House cls = Mapper.Map<HouseViewModel, House>(clsmodel);
                clsmodel.SchoolID = Convert.ToInt32(currentUserId);
                // cls.TokenKey = GlobalMethods.GetToken();
                bool isSuccess = _houseBusiness.AddUpdateDeleteClass(cls,"I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {

            int SchoolId = Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID);
            var userList = _houseBusiness.GetListWT();
            List<HouseViewModel> userViewModelList = new List<HouseViewModel>();

            var records = (from p in userList
                           select new HouseViewModel
                           {
                              HouseID = p.HouseID,
                             HouseName = p.HouseName,
                               SchoolID = p.SchoolID,
                              HouseColor = p.HouseColor,

                           }).Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
























	}
}