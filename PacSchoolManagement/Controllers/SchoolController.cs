﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;
namespace PacSchoolManagement.Controllers
{
     //[PacSchoolManagementAuthorize("admin")]
    public class SchoolController : Controller
    {
         //GET: School
        public ActionResult Index()
        {
            return View();
        }
         private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
         public SchoolBusiness _schoolBusiness;
         public SchoolDetailBusiness _schooldetailBusiness;
         public UserLoginBusiness _userBusiness;
         public SessionBusiness _sessionBusiness;
         public AcademicSettingBusiness _academicBusiness;
         public SchoolController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._schooldetailBusiness = new SchoolDetailBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._academicBusiness = new AcademicSettingBusiness(_df, this._unitOfWork);

        }
  
        // GET: /MasterDataManagement/
        //public ActionResult Index()
        //{



        //    return View();
        //}

       
       
        public ActionResult School()
        {
            SchoolViewModel schoolViewModel = new SchoolViewModel();
        var schoolList1 = _schoolBusiness.GetListWT();
        schoolViewModel.schoolList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.SchoolName.ToString(),
                Value = x.SchoolID.ToString()
           }).ToList();
            return View();

        }
        [HttpPost]
        public ActionResult School(string s)
        {

            return View();

        }

        //
        // POST: /Class/Create
        public JsonResult SaveDataInDatabase(SchoolViewModel clsmodel,HttpPostedFileBase file)
        {
            //HttpPostedFileBase fu = Request.Files["file"];
            //HttpPostedFileBase file1 = clsmodel.Logo;

            file = clsmodel.ImageUpload;

            if (clsmodel.SchoolID> 0)
            {
                var userList = _schoolBusiness.GetListWT(x => x.SchoolID == clsmodel.SchoolID).ToList().FirstOrDefault();

                userList.SchoolID = clsmodel.SchoolID;
                userList.SchoolName= clsmodel.SchoolName;
                userList.SchoolCode = clsmodel.SchoolCode;
                userList.SessionID = clsmodel.SessionID;
                userList.PinCode = clsmodel.PinCode;
                userList.ContactNo = clsmodel.ContactNo;
                userList.City = clsmodel.City;
                userList.Address = clsmodel.Address;
                userList.IsDelete = "true";
         
                bool isSuccess = _schoolBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }



            else { 

            var currentUserId = GlobalUser.getGlobalUser().Session;
                var session = _sessionBusiness.GetUserById(Convert.ToInt32(currentUserId));
               string sub =  session.Session1.Substring(0,4);
            SchoolDetailViewModel schooldet = new SchoolDetailViewModel();
            Mapper.CreateMap<SchoolViewModel, School>();
            School cls = Mapper.Map<SchoolViewModel, School>(clsmodel);
                cls.SessionID = Convert.ToInt32(currentUserId);
                var count = _schoolBusiness.GetAllUsers().Count();
                cls.SchoolCode = "PMS" + sub + count;
                AcademicsettingViewModel Academic = new AcademicsettingViewModel();
               
                // cls.TokenKey = GlobalMethods.GetToken();
                //if (clsmodel.ImageUpload.ContentLength > 0)
                //{    


                //    var fileName = Path.GetFileName(clsmodel.ImageUpload.FileName);
                //    var path = Path.Combine(Server.MapPath("~/SchoolImage"), fileName);
                //    var fileName1 = Path.GetFileName(file.FileName);
                //    clsmodel.ImageUpload.SaveAs(path);
                //    //  stumodel.SchoolId = Convert.ToInt32(GlobalUser.GlobalData.SchoolName).ToString();


                //    //  clsmodel.Logo = path;
                //    cls.Logo = fileName;
                //    cls.IsDelete = "true";
                //}

              
                bool isSuccess = _schoolBusiness.AddUpdateDeleteClass(cls, "I");



              // _schooldetailBusiness.AddUpdateDeleteClass(schooldet, "I");
                if (isSuccess)
                {
                    schooldet.SchoolId = cls.SchoolID;
                    schooldet.Address = cls.Address;
                    schooldet.City = cls.City;
                    schooldet.InstitutionCode = cls.SchoolCode;                 
                    schooldet.Name = cls.SchoolName;
                    schooldet.Phone = cls.ContactNo;


                    Mapper.CreateMap<SchoolDetailViewModel, SchoolDetail>();
                    SchoolDetail cls1 = Mapper.Map<SchoolDetailViewModel, SchoolDetail>(schooldet);
                    _schooldetailBusiness.AddUpdateDeleteClass(cls1, "I");

                    Academic.SchoolId = cls.SchoolID;
                    Academic.SessionId = cls.SessionID;

                    Mapper.CreateMap<AcademicsettingViewModel, academicsetting>();
                    academicsetting cls2 = Mapper.Map<AcademicsettingViewModel, academicsetting>(Academic);

                    _academicBusiness.AddUpdateDeleteClass(cls2, "I");

                    TempData["Success"] = "School Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create School!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

          //  }
            }
            return Json( JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetStudentList()  //Gets the todo Lists.
        {


            var userList = _schoolBusiness.GetListWT(x => x.IsDelete == "true");
            List<SchoolViewModel> userViewModelList = new List<SchoolViewModel>();

            var records = (from p in userList
                           select new SchoolViewModel
                           {
                              Address  = p.Address,
                               City = p.City,
                               ContactNo = p.ContactNo,
                              Logo = "/SchoolImage/" + p.Logo,
                              // ReadConfigData.GetAppSettingsValue("CMSUrl") + "/SliderImage/" + c.SliderImage
                               PinCode = p.PinCode,
                               SchoolCode=p.SchoolCode,
                               SchoolID=p.SchoolID,
                               SchoolName=p.SchoolName,
                               SessionID=p.SessionID
                               
                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _schoolBusiness.GetListWT(x => x.SchoolID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _schoolBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Updated Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to create Brand!!";
                TempData["isSuccess"] = "false";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _schoolBusiness.GetListWT(x => x.SchoolID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SchoolDetail()
        {
            var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;

            var model = _schoolBusiness.GetListWT(x => x.SchoolID == currentUserId).ToList().FirstOrDefault();
            var detail = _schooldetailBusiness.GetListWT(x => x.SchoolId == currentUserId).FirstOrDefault();
            SchoolDetailViewModel schooldet = new SchoolDetailViewModel();
            schooldet.Address = detail.Address;
            schooldet.City = detail.City;
            schooldet.Email = detail.Email;
            schooldet.Fax = detail.Fax;
            schooldet.InstitutionCode = model.SchoolCode;
            schooldet.Landdoc = detail.Landdoc;
            schooldet.Landtype = detail.Landtype;
            schooldet.Mobile = detail.Mobile;
            schooldet.Name = model.SchoolName;
            schooldet.Openedon = detail.Openedon;
            schooldet.Phone = detail.Phone;
            schooldet.SchoolId = detail.SchoolId;
            schooldet.SchoolDetID = detail.SchoolDetID;

            return View(schooldet);
        }
    [HttpPost]
        public ActionResult SchoolDetail(SchoolDetailViewModel sd)
        {
            var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;
            Mapper.CreateMap<SchoolDetailViewModel, SchoolDetail>();
            SchoolDetail cls = Mapper.Map<SchoolDetailViewModel, SchoolDetail>(sd);
          
            var upload = _schooldetailBusiness.AddUpdateDeleteClass(cls, "U");
            
            return View("SchoolDetail",sd);
        }



    }
}