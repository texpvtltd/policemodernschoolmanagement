﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class ReportBookController : Controller
    {



        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        private BookBusiness _bookbusiness;
        private BookCategoryBusiness _bookcategorybusiness;
        private IssueBookBusiness _issuebookbusiness;


        // GET: ReportBook

        public ReportBookController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._bookbusiness = new BookBusiness(_df, this._unitOfWork);
            this._bookcategorybusiness = new BookCategoryBusiness(_df, this._unitOfWork);
            this._issuebookbusiness = new IssueBookBusiness(_df, this._unitOfWork);
        }


        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Reportbook()
        {

            
      ReportBookViewModel reportbookmodel= new ReportBookViewModel();
            List<SelectListItem>  reportbook= new List<SelectListItem>();
            reportbook.Add(new SelectListItem { Text = "Availablebook", Value = "Availablebook" });
            reportbook.Add(new SelectListItem { Text = "IssuedBook", Value = "IssuedBook" });
           // reportbook.Add(new SelectListItem { Text = "IssuedBook", Value = "IssuedBook" });
            reportbook.Add(new SelectListItem { Text = "RecoveryBook", Value = "IssuedBook" });
            reportbook.Add(new SelectListItem { Text = "ConditionWise", Value = "ConditionWise" });
            reportbook.Add(new SelectListItem { Text = "StudentWise", Value = "StudentWise" });
            reportbook.Add(new SelectListItem { Text = "EmployeeWise", Value = "EmployeeWise" });

            reportbookmodel.Reoprtsforbook = reportbook;

            List<SelectListItem> reporttyp = new List<SelectListItem>();
            reporttyp.Add(new SelectListItem { Text = "Date Wise", Value = "Date Wise" });
            reporttyp.Add(new SelectListItem { Text = "Batch Wise", Value = "Batch Wise" });


            reportbookmodel.ReportTypeList = reporttyp;

            return View(reportbookmodel);
               

           
        }
        public JsonResult SearchBook(string prefix)
        {
            List<Book> allUser = new List<Book>();


       //allUser = _bookbusiness.GetListWT(c => c.BookNo.Contains(prefix) || c.Title.Contains(prefix) || c.BookCategory.Contains(prefix)).ToList();
            if (prefix == "IssuedBook")
            {
                List<IssueBook> allUser1 = new List<IssueBook>();

                allUser1 = _issuebookbusiness.GetListWT();
                return new JsonResult { Data = allUser1, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else if (prefix == "Availablebook")
            {
                allUser = _bookbusiness.GetListWT();

            }
        
            

            return new JsonResult { Data = allUser, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            
        }


        public ActionResult Reportbook( string m)
        {
            return View();
        }
    }
}