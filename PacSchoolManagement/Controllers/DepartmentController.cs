﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;
namespace PacSchoolManagement.Controllers
{
        //[PacSchoolManagementAuthorize("Principle")]
    public class DepartmentController : Controller
    {
          private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        public DepartmentController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

        }
        //
        // GET: /Department/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Department()
        {

            return View();
        }
        public JsonResult SaveDataInDatabase(DeptViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.DeptId > 0)
            {
                var userList = _deptBusiness.GetListWT(x => x.DeptId == clsmodel.DeptId).ToList().FirstOrDefault();

                userList.DeptId = clsmodel.DeptId;
                userList.DepartmentName = clsmodel.DepartmentName;
                
                bool isSuccess = _deptBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {
                

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                
               

                Mapper.CreateMap<DeptViewModel, Department>();
                Department cls = Mapper.Map<DeptViewModel, Department>(clsmodel);
                cls.SchoolId = 0;
                cls.IsDelete ="false";
                bool isSuccess = _deptBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _deptBusiness.GetListWT(); 
            List<DeptViewModel> userViewModelList = new List<DeptViewModel>();

            var records = (from p in userList
                           select new DeptViewModel
                           {
                               DeptId = p.DeptId,
                               DepartmentName = p.DepartmentName,
                               SchoolId = p.SchoolId,
                               IsDelete = p.IsDelete,

                           }).Where(i => i.SchoolId == 0).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
       




        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _deptBusiness.GetListWT(x => x.DeptId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _deptBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _deptBusiness.GetListWT(x => x.DeptId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }




	}
}