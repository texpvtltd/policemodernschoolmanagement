﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Models
{
    public class PrincipleController : Controller
    {
   
        //
        // GET: /Principle/


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;

        public PrincipleController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);


        }

        public ActionResult Dashboard (int SchoolId)
        {
            return View();
        }
    
        public ActionResult AddPrincipal( string StudentId)
        {
            PrincipalViewModel principleViewModel = new PrincipalViewModel();
            var schoolList1 = _sessionBusiness.GetListWT();
            principleViewModel.PricipalList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.Session1.ToString(),
                Value = x.SessionID.ToString()
            }).ToList();

            var schoolList2 = _schoolBusiness.GetListWT();
            principleViewModel.schoollList = schoolList2.Select(x => new SelectListItem
            {
                Text = x.SchoolName.ToString(),
                Value = x.SchoolID.ToString()
            }).ToList();
            return View(principleViewModel);

        }



            [HttpPost]
        public ActionResult AddPrincipal()
        {
            return View();
        }

            public JsonResult SaveDataInDatabase(PrincipalViewModel clsmodel, HttpPostedFileBase file)
            {
                // var result = false;
                //if (clsmodel.ClassID > 0)
                //{  

                if (clsmodel.PrincipalID > 0)
                {
                    var userList = _princiBusiness.GetListWT(x => x.PrincipalID == clsmodel.PrincipalID).ToList().FirstOrDefault();
                    userList.Contact = clsmodel.Contact;
                    userList.SchoolID = clsmodel.SchoolID;
                    userList.PrincipalName = clsmodel.PrincipalName;
                //    userList.SchoolID = Convert.ToInt32(clsmodel.SchoolID);
                    userList.Session = clsmodel.Session;
                    userList.Email = clsmodel.Email;
                    if (clsmodel.ImageUpload.ContentLength > 0)
                    {
                        var fileName = Path.GetFileName(clsmodel.ImageUpload.FileName);
                        var path = Path.Combine(Server.MapPath("~/SchoolImage"), fileName);
                      //  var fileName1 = Path.GetFileName(file.FileName);
                        clsmodel.ImageUpload.SaveAs(path);
                        //  stumodel.SchoolId = Convert.ToInt32(GlobalUser.GlobalData.SchoolName).ToString();

                       
                        userList.PrincipalImage = fileName;
                        userList.IsDelete = "false";
                    }
                    userList.Session = clsmodel.Session;
                    bool isSuccess = _princiBusiness.AddUpdateDeleteClass(userList, "U");
                    if (isSuccess)
                    {
                        TempData["Success"] = "Updated Successfully!!";
                        TempData["isSuccess"] = "true";
                        // return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Success"] = "Failed to create updated!!";
                        TempData["isSuccess"] = "false";
                    }
                }


                else
                {

                    file = clsmodel.ImageUpload;
                    var currentUserId = GlobalUser.getGlobalUser().UserId;

                    Mapper.CreateMap<PrincipalViewModel, Principal>();
                    Principal cls = Mapper.Map<PrincipalViewModel, Principal>(clsmodel);

                      if (clsmodel.ImageUpload.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(clsmodel.ImageUpload.FileName);
                    var path = Path.Combine(Server.MapPath("~/SchoolImage"), fileName);
                    var fileName1 = Path.GetFileName(file.FileName);
                    clsmodel.ImageUpload.SaveAs(path);
                    //  stumodel.SchoolId = Convert.ToInt32(GlobalUser.GlobalData.SchoolName).ToString();


                    //  clsmodel.Logo = path;
                    cls.PrincipalImage = fileName;
                    cls.IsDelete = "false";
                }
                    //clsmodel.PrincipalID = Convert.ToInt32(currentUserId);
                      var email = clsmodel.Email;
                    var password = Md5Encryption.Encrypt(clsmodel.Password);
                    UserLogin userList = new UserLogin();
                    userList.Password = password;
                    userList.UserType = "Principle";
                    userList.Username = email;
                    //userList.Username = email;
                    userList.IsDelete = "false";
                    userList.IsConfirm = "false";
                    userList.SchoolID = clsmodel.SchoolID;
                    userList.Session = clsmodel.Session.ToString();
                    cls.Password = password;
                    cls.Email = clsmodel.Email;
                    bool isSuccess1 = _userBusiness.AddUpdateDeleteUser(userList, "I");
                    // cls.TokenKey = GlobalMethods.GetToken();
                    bool isSuccess = _princiBusiness.AddUpdateDeleteClass(cls, "I");
                    if (isSuccess)
                    {
                        TempData["Success"] = "Principal Added Successfully!!";
                        TempData["isSuccess"] = "true";
                        // return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["Success"] = "Failed to add principal!!";
                        TempData["isSuccess"] = "false";
                    }
                    // result = true;
                }
                //  }

                return Json( JsonRequestBehavior.AllowGet);
            }


        public JsonResult GetPrinciList()  //Gets the todo Lists.
        {


            var userList = _princiBusiness.GetListWT(x => x.IsDelete == "false"); ;
            List<PrincipalViewModel> userViewModelList = new List<PrincipalViewModel>();

            var records = (from p in userList
                           select new PrincipalViewModel
                           {
                               PrincipalID=p.PrincipalID,
                               PrincipalName = p.PrincipalName,
                               SchoolID = p.SchoolID,
                               Session = p.Session,
                               Contact = p.Contact,
                               Email = p.Email,
                              SchoolName= _schoolBusiness.GetListWT(x => x.SchoolID == p.SchoolID).FirstOrDefault().SchoolName,
                               PrincipalImage = "/SchoolImage/" + p.PrincipalImage,                         
                                                        
                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

  




        public ActionResult Index()
        {
            return View();
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _princiBusiness.GetListWT(x => x.PrincipalID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _princiBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _princiBusiness.GetListWT(x => x.PrincipalID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }



	}
}