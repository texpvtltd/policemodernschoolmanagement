﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class TransportFeesCollectionController : Controller
    {
        // GET: TransportFeesCollection

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
     
        private DepartmentBusiness _deptBusiness;
        private TransPortFeesCollectionBusiness _trnsfeesBusiness;
        private TFeesCollection _tfeesCollection;
        public TransportFeesCollectionController()
        {
            this._unitOfWork = new UnitOfWork(_df);
          

      
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._trnsfeesBusiness = new TransPortFeesCollectionBusiness(_df, this._unitOfWork);
            this._tfeesCollection = new TFeesCollection(_df, this._unitOfWork);
        }

        public ActionResult Index()
        {

            var record = _tfeesCollection.GetListWT(x => x.IsDelete == "false");
         

            return View(record);


           
        }
        [HttpGet]
        public ActionResult TransportFeesCollection()
        {

            TransPortFeesCollectionViewModel feecollectioviewmodel = new TransPortFeesCollectionViewModel();
          
            var department = _deptBusiness.GetListWT();
            feecollectioviewmodel.departmentList = department.Select(x => new SelectListItem
            {
                Text = x.DepartmentName.ToString(),
                Value = x.DeptId.ToString()
            }).ToList();
        
            List<SelectListItem> BookCondition = new List<SelectListItem>();
            BookCondition.Add(new SelectListItem { Text = "Online", Value = "OnLine" });
            BookCondition.Add(new SelectListItem { Text = "Cheque", Value = "Cheque" });
            BookCondition.Add(new SelectListItem { Text = "Case", Value = "Case" });



            feecollectioviewmodel.ModeList = BookCondition;



            return View(feecollectioviewmodel);
        }

        [HttpPost]
        public ActionResult TransportFeesCollection(FeesCollectionViewModel clsmodel)
        {



            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;

            Mapper.CreateMap<FeesCollectionViewModel, TransprotFee>();
            TransprotFee cls = Mapper.Map<FeesCollectionViewModel, TransprotFee>(clsmodel);
            cls.SchoolId = Convert.ToInt32(currentUserId).ToString();
            cls.IsDelete = "false";

            bool isSuccess = _tfeesCollection.AddUpdateDeleteClass(cls, "I");
            if (isSuccess)
            {
                TempData["Success"] = " Created Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to create!!";
                TempData["isSuccess"] = "false";
            }
            // result = true;



            return RedirectToAction("Index", " TransportFeesCollection");
        }


        public JsonResult SearchBook(string prefix)
        {
            List<AddRoute> allUser = new List<AddRoute>();


            // Here "MyDatabaseEntities " is dbContext, which is created at time of model creation.
           //allUser = _addrautebusiness.GetListWT().ToList();

            //allUser = dc.UserMasters.Where(a => a.Username.Contains(prefix)).ToList();


            return new JsonResult { Data = allUser, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }


       

       
        

        [HttpGet]
         public ActionResult Edit(int txtval)
        
                {
                    TransPortFeesCollectionViewModel clsmodel = new TransPortFeesCollectionViewModel();

                    List<SelectListItem> BookCondition = new List<SelectListItem>();
                    BookCondition.Add(new SelectListItem { Text = "Online", Value = "OnLine" });
                    BookCondition.Add(new SelectListItem { Text = "Cheque", Value = "Cheque" });
                    BookCondition.Add(new SelectListItem { Text = "Case", Value = "Case" });



                    clsmodel.ModeList = BookCondition;



                    var userList = _trnsfeesBusiness.GetListWT(x => x.TransPortFeeCollId == txtval).ToList().FirstOrDefault();

                    clsmodel.ModeOfPay = userList.ModeOfPay;
                    clsmodel.AmountTobePaid = userList.AmountTobePaid;

                    clsmodel.BankName = userList.BankName;
                    clsmodel.ChequeDate = userList.ChequeDate;
                    clsmodel.ChequeNo = userList.ChequeNo;

                    clsmodel.Discount = userList.Discount;
                    clsmodel.FeeType = userList.FeeType;
                    clsmodel.Fine = userList.Fine;
                    clsmodel.ActualAmount = userList.ActualAmount;
                    clsmodel.ReciptNo = userList.ReciptNo;
                    clsmodel.Remark = userList.Remark;
                    clsmodel.TransPortFeeCollId = userList.TransPortFeeCollId;
                    clsmodel.TotalAmount = userList.TotalAmount;


                    return View(clsmodel);
        }


        [HttpPost]
        public ActionResult Edit(TransPortFeesCollectionViewModel clsmodel)

            {
                //Mapper.CreateMap<TransPortFeesCollectionViewModel, TransprotFee>();
               // TransprotFee cls = Mapper.Map<TransPortFeesCollectionViewModel, TransprotFee>(clsmodel);
               // cls.SchoolId = Convert.ToInt32(currentUserId).ToString();
                //cls.IsDelete = "false";



                var userList = _trnsfeesBusiness.GetListWT(x => x.TransPortFeeCollId == clsmodel.TransPortFeeCollId).ToList().FirstOrDefault();

                userList.ModeOfPay = clsmodel.ModeOfPay;
                userList.AmountTobePaid = clsmodel.AmountTobePaid;

                userList.BankName = clsmodel.BankName;
                userList.ChequeDate = clsmodel.ChequeDate;
                userList.ChequeNo = clsmodel.ChequeNo;

                userList.Discount = clsmodel.Discount;
                userList.FeeType = clsmodel.FeeType;
                userList.Fine = clsmodel.Fine;
                userList.ActualAmount = clsmodel.ActualAmount;

                bool isSuccess = _trnsfeesBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = " Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create!!";
                    TempData["isSuccess"] = "false";
                }
                return RedirectToAction("Index", " TransportFeesCollection");
        }

        public ActionResult delete(int StudentId)
        {
            var userList = _trnsfeesBusiness.GetListWT(x => x.TransPortFeeCollId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _trnsfeesBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }






            return RedirectToAction("Index", " TransportFeesCollection");
        }



    }
}