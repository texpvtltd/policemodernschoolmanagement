﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;

namespace PacSchoolManagement.Controllers
{
    public class HostalDetailController : Controller
    {
        // GET: HostalDetail




        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostalTypeBusiness _hostaltypeBusiness;
        private HostalDetailBusiness _hostaldetailBusiness;


        public HostalDetailController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this ._hostaltypeBusiness=new HostalTypeBusiness (_df,this._unitOfWork );
            this._hostaldetailBusiness = new HostalDetailBusiness(_df, this._unitOfWork);
        }




        public ActionResult Index()
        {
            return View();
        }



        public ActionResult HostalDetailList()
        {
            HostalDetailViewModel hosdetail = new HostalDetailViewModel();


            var hostype = _hostaltypeBusiness.GetListWT();
            hosdetail.HostalTypeList = hostype.Select(x => new SelectListItem
        {
            Text = x.HostalType1.ToString(),
            Value = x.HostalTypeID.ToString(),
        }).ToList();

            return View(hosdetail);
         // return View();
        }


        public JsonResult SaveDataInDatabase(HostalDetailViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.HostalId > 0)
            {
                var userList = _hostaldetailBusiness.GetListWT(x => x.HostalId == clsmodel.HostalId).ToList().FirstOrDefault();

                userList.HostalId = clsmodel.HostalId;
                userList.HostalName = clsmodel.HostalName;
                userList.HostalType = clsmodel.HostalType;
                userList.HostalContact = clsmodel.HostalContact;
                userList.WardanAddress = clsmodel.WardanAddress;
                userList.WardenName = clsmodel.WardenName;
                userList.WardenPhoneNo = clsmodel.WardenPhoneNo;
                userList.HostalAddress = clsmodel.HostalAddress;

                bool isSuccess = _hostaldetailBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<HostalDetailViewModel, HostalDetail>();
                HostalDetail cls = Mapper.Map<HostalDetailViewModel, HostalDetail>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _hostaldetailBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
           // int SchoolId = Convert.ToInt32(GlobalUser.getGlobalUser().SchoolID);
            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _hostaldetailBusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostalDetailViewModel
                           {
                               SchoolId = p.SchoolId,
                               HostalAddress = p.HostalAddress,
                               HostalContact = p.HostalContact,
                              // Hostaltyp = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostalId = p.HostalId,
                               HostalType = p.HostalType,
                               HostalName = p.HostalName,
                               WardanAddress = p.WardanAddress,
                              
                               WardenName = p.WardenName,
                               WardenPhoneNo = p.WardenPhoneNo,
                             
                           });
                          // .Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _hostaldetailBusiness.GetListWT(x => x.HostalId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _hostaldetailBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _hostaldetailBusiness.GetListWT(x => x.HostalId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }








    }
}