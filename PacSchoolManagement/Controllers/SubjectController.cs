﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class SubjectController : Controller
    {
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SubjectBusiness _subjectBusiness;
        public TeacherBusiness _teacherbusiness;
        public SectionBusiness _sectionbusiness;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private StudentBusiness _studentBusiness;
        private ClassBusiness _classBusiness;
        private SectionBusiness _sectionBusiness;
        private StudentDocumentBusiness _studentdocumentBusiness;
         public TeacherBusiness _teacherBusiness;
        public SubjectController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._subjectBusiness = new SubjectBusiness(_df, this._unitOfWork);
            this._teacherbusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._sectionbusiness = new SectionBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._studentdocumentBusiness = new StudentDocumentBusiness(_df, this._unitOfWork);
        }

        // GET: Subject
        [HttpGet]
        public ActionResult Subject()
        {
            SubjectViewModel subjViewModel = new SubjectViewModel();
           
           
            var classList1 = _classBusiness.GetListWT();
            subjViewModel.ClassList = classList1.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();
            var section = _sectionBusiness.GetListWT();
            subjViewModel.SectionList = section.Select(x => new SelectListItem
            {
                Text = x.SectionName.ToString(),
                Value = x.SectionID.ToString()
            }).ToList();
            //var session = _sessionBusiness.GetListWT();
            //subjViewModel.SessionList = session.Select(x => new SelectListItem
            //{
            //    Text = x.SessionID.ToString(),
            //    Value = x.Session1.ToString()
            //}).ToList();

            var teacher = _teacherBusiness.GetListWT();
            subjViewModel.TeacherList = teacher.Select(x => new SelectListItem
            {
                Text = x.SchoolUserID.ToString(),
                Value = x.FName+""+x.LName.ToString()
            }).ToList();




            return View(subjViewModel);

  
        }
        [HttpPost]
        public ActionResult Subject(string s)
        {

            return View();

        }

        //
        // POST: /Class/Create
        public JsonResult SaveDataInDatabase(SubjectViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.SubjectID > 0)
            {
                var userList = _subjectBusiness.GetListWT(x => x.SubjectID == clsmodel.SubjectID).ToList().FirstOrDefault();
                var sid = GlobalUser.getGlobalUser().SchoolID;
                userList.ClassID = clsmodel.ClassID;
                userList.SubjectName = clsmodel.SubjectName;
                userList.SectionID = clsmodel.SectionID;
                userList.TeacherID = clsmodel.TeacherID;
                userList.SchoolID = Convert.ToInt32(sid);
                bool isSuccess = _subjectBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;

                Mapper.CreateMap<SubjectViewModel, subjectnew>();
                subjectnew cls = Mapper.Map<SubjectViewModel, subjectnew>(clsmodel);
                clsmodel.SchoolID = Convert.ToInt32(currentUserId);
                // cls.TokenKey = GlobalMethods.GetToken();
               // bool isSuccess = _subjectBusiness.AddUpdateDeleteClass(cls, "I");
               // if (isSuccess)
               // {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
               // }
               // else
                //{
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                //}
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
          //  var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;
            var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;
            var userList = _subjectBusiness.GetListWT();
            List<SubjectViewModel> userViewModelList = new List<SubjectViewModel>();
            var teacherlist = _teacherbusiness.GetListWT();
            var sectionlist = _sectionbusiness.GetListWT();
            var records = (from p in userList
                           select new SubjectViewModel
                           {
                               SubjectID = p.SubjectID,
                              // ClassName = _classBusiness.GetUserById(Convert.ToInt32(c.ClassID)).ClassName,
                                                        
                               SubjectName = p.SubjectName
                               //SchoolID = p.SchoolID,
                              // TeacherName = q.FName,
                               //Section = r.SectionName
                           }).AsQueryable().Where(x => x.SchoolID == currentUserId);


            return Json(records, JsonRequestBehavior.AllowGet);
        }



        public JsonResult DeleteStudentRecord(int StudentId)
        {
            string JsonStr = "";
            bool isSuccess = true;
            string message = "Delete Successful!";
            _unitOfWork.BeginTransaction();

            //var Stu = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();
            if (StudentId != null)
            {
                var br = _subjectBusiness.Find(StudentId);
                _subjectBusiness.Delete(StudentId);
                _unitOfWork.SaveChanges();

            }
            //List<Class> userList = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList();
            //foreach (Class usr in userList)
            //{
            //    int deleteId = Convert.ToInt32(usr.ClassID);
            //   // var br = _classBusiness.Find(deleteId);
            //    _classBusiness.Deleteid(deleteId);
            //    _unitOfWork.SaveChanges();
            //}

            _unitOfWork.Commit();



            TempData["Success"] = message;
            TempData["isSuccess"] = isSuccess.ToString();

            JsonStr = "{\"message\":\"" + message + "\",\"isSuccess\":\"" + isSuccess + "\"}";
            return Json(JsonStr, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int SubjectID)
        {
            var model = _subjectBusiness.GetListWT(x => x.SubjectID == SubjectID).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }
    }
}