﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class AddHostelRoomController : Controller
    {
        // GET: AddHostelRoom

      
        // GET: BookCategory
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private HostalDetailBusiness _hostaldetailBusiness;
        private HostalTypeBusiness _hostaltypeBusiness;

        private AddHostelRoomBusiness _addhostalroomBusiness;
        public AddHostelRoomController()
        {
            this._unitOfWork = new UnitOfWork(_df);

            this._hostaldetailBusiness = new HostalDetailBusiness(_df, this._unitOfWork);
            this._hostaltypeBusiness = new HostalTypeBusiness(_df, this._unitOfWork);
            this._addhostalroomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);


        }

        public ActionResult Index()
        {


            var userList = _addhostalroomBusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new AddHostaRoomlViewModel
                           {
                               SchoolId = p.SchoolId,
                               HostalType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostalfloorName = p.HostalfloorName,
                               // Hostaltyp = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostalName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostalName)).HostalName,
                               IsDelete = p.IsDelete,
                               NoOfBed = p.NoOfBed,
                               FeeType = p.FeeType,

                               Amount = p.Amount,
                               BedNo = p.BedNo,
                               RoomId=p.RoomId,
                               RoomNo=p.RoomNo,
                               RoomRent=p.RoomRent,



                           }).ToList();
            // .Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();




         //   var record = _addhostalroomBusiness.GetListWT(x=>x.IsDelete=="false").ToList();

            return View(records);
        }

          [HttpGet]
        public ActionResult CreateHostelRoom()
        {
            AddHostaRoomlViewModel roomviewmodel = new AddHostaRoomlViewModel();


            List<SelectListItem> FeeTypeList = new List<SelectListItem>();
            FeeTypeList.Add(new SelectListItem { Text = "Annual", Value = "Annual" });
            FeeTypeList.Add(new SelectListItem { Text = "Tri-annual", Value = "Tri-annual" });
            FeeTypeList.Add(new SelectListItem { Text = "Bi-Annual", Value = "Bi-Annual" });
            FeeTypeList.Add(new SelectListItem { Text = "Quaterly", Value = "Quaterly" });




            roomviewmodel.HostalFeetypeList = FeeTypeList;






            var StudentAdmissionList1 = _hostaltypeBusiness.GetListWT();
            roomviewmodel.HostalTypeList = StudentAdmissionList1.Select(x => new SelectListItem
            {
                Text = x.HostalType1.ToString(),
                Value = x.HostalTypeID.ToString()
            }).ToList();

            var hostaldata = _hostaldetailBusiness.GetListWT();
            roomviewmodel.HostalNameList = hostaldata.Select(x => new SelectListItem
            {
                Text = x.HostalName.ToString(),
                Value = x.HostalId.ToString()
            }).ToList();



            return View(roomviewmodel);
        }

      [HttpPost]
        public ActionResult CreateHostelRoom( AddHostaRoomlViewModel clsmodel)
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;

           // var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID;

            Mapper.CreateMap<AddHostaRoomlViewModel, AddHostalRoom>();
            AddHostalRoom cls = Mapper.Map<AddHostaRoomlViewModel, AddHostalRoom>(clsmodel);
            cls.SchoolId = Convert.ToInt32(currentUserId).ToString();
            cls.IsDelete = "false";

            bool isSuccess = _addhostalroomBusiness.AddUpdateDeleteAddHostalRoom(cls, "I");
            if (isSuccess)
            {
                TempData["Success"] = "add Created Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to create !!";
                TempData["isSuccess"] = "false";
            }
            // result = true;




            return RedirectToAction("Index", "AddHostelRoom");
        }

        [HttpGet]
      public ActionResult EditRoom(int txtval)
      {
          AddHostaRoomlViewModel roomviewmodel = new AddHostaRoomlViewModel();
          var userList = _addhostalroomBusiness.GetListWT(x => x.RoomId == txtval).ToList().FirstOrDefault();

          List<SelectListItem> FeeTypeList = new List<SelectListItem>();
          FeeTypeList.Add(new SelectListItem { Text = "Annual", Value = "Annual" });
          FeeTypeList.Add(new SelectListItem { Text = "Tri-annual", Value = "Tri-annual" });
          FeeTypeList.Add(new SelectListItem { Text = "Bi-Annual", Value = "Bi-Annual" });
          FeeTypeList.Add(new SelectListItem { Text = "Quaterly", Value = "Quaterly" });




          roomviewmodel.HostalFeetypeList = FeeTypeList;




          var StudentAdmissionList1 = _hostaltypeBusiness.GetListWT();
          roomviewmodel.HostalTypeList = StudentAdmissionList1.Select(x => new SelectListItem
          {
              Text = x.HostalType1.ToString(),
              Value = x.HostalTypeID.ToString()
          }).ToList();

          var hostaldata = _hostaldetailBusiness.GetListWT();
          roomviewmodel.HostalNameList = hostaldata.Select(x => new SelectListItem
          {
              Text = x.HostalName.ToString(),
              Value = x.HostalId.ToString()
          }).ToList();
          roomviewmodel.Amount = userList.Amount;
          roomviewmodel.BedNo = userList.BedNo;
          roomviewmodel.FeeType = userList.FeeType;
          roomviewmodel.HostalfloorName = userList.HostalfloorName;
          roomviewmodel.HostalName = userList.HostalName;
          roomviewmodel.HostalType = userList.HostalType;
          roomviewmodel.RoomNo = userList.RoomNo;
          roomviewmodel.RoomId = userList.RoomId;
          roomviewmodel.RoomRent = userList.RoomRent;
          roomviewmodel.NoOfBed = userList.NoOfBed;
          

          return View(roomviewmodel);

      }

        [HttpPost]
      public ActionResult EditRoom(AddHostaRoomlViewModel roomviewmodel)
      {
          var userList = _addhostalroomBusiness.GetListWT(x => x.RoomId == roomviewmodel.RoomId).ToList().FirstOrDefault();

          userList.Amount = roomviewmodel.Amount;
          userList.BedNo = roomviewmodel.BedNo;
          userList.FeeType = roomviewmodel.FeeType;
          userList.HostalfloorName = roomviewmodel.HostalfloorName;
          userList.HostalName = roomviewmodel.HostalName;
          userList.HostalType = roomviewmodel.HostalType;
          userList.RoomNo = roomviewmodel.RoomNo;
          userList.RoomId = roomviewmodel.RoomId;
          userList.RoomRent = roomviewmodel.RoomRent;

          bool isSuccess = _addhostalroomBusiness.AddUpdateDeleteAddHostalRoom(userList, "U");
          if (isSuccess)
          {
              TempData["Success"] = "add Created Successfully!!";
              TempData["isSuccess"] = "true";
              // return RedirectToAction("Index");
          }
          else
          {
              TempData["Success"] = "Failed to create !!";
              TempData["isSuccess"] = "false";
          }
          return RedirectToAction("Index", "AddHostelRoom");
      }


        public ActionResult Delete(int txtval)
        {
            var userList = _addhostalroomBusiness.GetListWT(x => x.RoomId == txtval).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _addhostalroomBusiness.AddUpdateDeleteAddHostalRoom(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return RedirectToAction("Index", "AddHostelRoom");
        }




    }
}