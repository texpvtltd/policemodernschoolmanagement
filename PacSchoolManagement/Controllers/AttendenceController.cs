﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class AttendenceController : Controller
    {
        // GET: Attendence



        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private StudentBusiness _studentBusiness;
        private ClassBusiness _classBusiness;
        private SectionBusiness _sectionBusiness;
        private StudentDocumentBusiness _studentdocumentBusiness;
        private AttendenceBusiness _attendenceBusiness;
        private AttendenceDetailBusiness _attendencedetailBusiness;
        public AttendenceController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._studentdocumentBusiness = new StudentDocumentBusiness(_df, this._unitOfWork);

            this._attendenceBusiness = new AttendenceBusiness(_df, this._unitOfWork);
            this._attendencedetailBusiness = new AttendenceDetailBusiness(_df, this._unitOfWork);
        }






        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Attendence(string s)
        {
            AttendenceViewModel attendenceViewModel = new AttendenceViewModel();
            var schoolList1 = _sessionBusiness.GetListWT();
            attendenceViewModel.SessionList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.Session1.ToString(),
                Value = x.SessionID.ToString()
            }).ToList();

            var classList1 = _classBusiness.GetListWT();
            attendenceViewModel.ClassList = classList1.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();
            var section = _sectionBusiness.GetListWT();
            attendenceViewModel.SectionList = section.Select(x => new SelectListItem
            {
                Text = x.SectionName.ToString(),
                Value = x.SectionID.ToString()
            }).ToList();
            attendenceViewModel.ListStudentDetail = new List<StudentViewModel>();


           
            return View(attendenceViewModel);
        }
        [HttpPost]
        public ActionResult Attendence( AttendenceViewModel model)
        {
            return View();
        }

        [HttpGet]
        public ActionResult StudentData(string ClasId, string SectionId, string SessionId,string Date)
        {
            int cid = Convert.ToInt32(ClasId);
            int scid = Convert.ToInt32(SectionId);
            int seid = Convert.ToInt32(SessionId);

            AttendenceViewModel attendenceviewmodel = new AttendenceViewModel();
            var studetail = _studentBusiness.GetListWT(x => x.ClassID == cid && x.Section == scid && x.Session == seid).ToList();

            attendenceviewmodel.ListStudentDetail = (from c in studetail
                                                     select new StudentViewModel
                                                     {
                                                         SchoolID = c.SchoolID,
                                                         StudentID = c.StudentID,
                                                      
                                                         FatherName = c.FatherName,
                                                           FirstName=c.FirstName,
                                                            LastName = c.LastName,
                                                           ClassName=  _classBusiness.GetUserById(Convert.ToInt32(c.ClassID)).ClassName,
                                                        
                                                         Section = c.Section,
                                                         SessinId = c.Session.ToString(),

                                                     }).ToList();
            List<SelectListItem> listattendstatus = new List<SelectListItem>();
            listattendstatus.Add(new SelectListItem { Text = "Present", Value = "Present" });
            listattendstatus.Add(new SelectListItem { Text = "Absencet", Value = "Absencet" });

            attendenceviewmodel.AttendenceStatusList = listattendstatus;

            attendenceviewmodel.SessionID = seid;
            attendenceviewmodel.SectionId = scid;
            attendenceviewmodel.ClassID = cid;
            attendenceviewmodel.Date = Date;
             return View(attendenceviewmodel);
            //return RedirectToAction("Attendence", attendenceviewmodel);

        }
        //public JsonResult StudentData(string ClasId, string SectionId, string SessionId)
        //{
        //    int cid = Convert.ToInt32(ClasId);
        //    int scid = Convert.ToInt32(SectionId);
        //    int seid = Convert.ToInt32(SessionId);
        //    AttendenceViewModel attendenceviewmodel = new AttendenceViewModel();
        //    var studetail = _studentBusiness.GetListWT(x => x.ClassID == cid && x.Section == scid && x.Session == seid).ToList();

        //    attendenceviewmodel.ListStudentDetail = (from c in studetail
        //                                             select new StudentViewModel
        //                                             {
        //                                                 SchoolID = c.SchoolID,
        //                                                 StudentID = c.StudentID,
        //                                                 FatherName = c.FatherName,
        //                                                 FirstName=c.FirstName,
        //                                                 LastName = c.LastName,
        //                                                 Section = c.Section,
        //                                                 SessinId = c.Session.ToString(),

        //                                             }).ToList();
        //    List<SelectListItem> listattendstatus = new List<SelectListItem>();
        //    listattendstatus.Add(new SelectListItem { Text = "Present", Value = "Present" });
        //    listattendstatus.Add(new SelectListItem { Text = "Absencet", Value = "Absencet" });

        //    attendenceviewmodel.AttendenceStatusList = listattendstatus;
        //    // return View(attendenceviewmodel);
        //    return Json(attendenceviewmodel, JsonRequestBehavior.AllowGet);

        //}




        [HttpPost]
        public ActionResult StudentData(List<StudentViewModel> purchaseArray, AttendenceViewModel model, string ClassID, string SectionId, string SchoolId, string SessionID, string Date)
        {
           // AttendenceViewModel model =new AttendenceViewModel();
         //   List<AttendenceViewModel> lstSUDetail = new List<AttendenceViewModel>();
            //dynamic objdetail = Newtonsoft.Json.JsonConvert.DeserializeObject(model.StudentDetail);
             
           

            var currentUserId = GlobalUser.getGlobalUser().UserId;

            Mapper.CreateMap<AttendenceViewModel, Attendance>();
            Attendance att = Mapper.Map<AttendenceViewModel, Attendance>(model);
            model.SchoolID = Convert.ToInt32(currentUserId);
            model.SessionID = Convert.ToInt32(SessionID);
            model.ClassID = Convert.ToInt32(ClassID);
            model.SectionId = Convert.ToInt32(SessionID);
            model.Date = Date;
           //  cls.TokenKey = GlobalMethods.GetToken();

            bool isSuccess = _attendenceBusiness.AddUpdateDeleteClass(att, "I");
            if (isSuccess)
            {
                TempData["Success"] = "Student register Added Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to addStudent!!";
                TempData["isSuccess"] = "false";
            }
            AttendanceDetail attd = new AttendanceDetail();

            foreach (var qdetail in purchaseArray)
            {
                
                // SUDtl.Doorsize= qdetail.Doorsize;
                //SUDtl.ItemId = qdetail.ItemId;
               // var attendencelst = _attendencedetailBusiness.GetListWT().ToList().FirstOrDefault();
                attd.SchoolID = qdetail.SchoolID;
                attd.StudentID = qdetail.StudentID;
                //attd.Status = qdetail.AttStatus;
              //  attd.AttendanceID = att.AttendanceID;
              //  attd.Date = Date;
               // attd.Cl = qdetail.ClassID;
                attd.IsDelete = "true";

               // lstSUDetail.Add(SUDtl);

                bool isSuccess1 = _attendencedetailBusiness.AddUpdateDeleteClass(attd, "I");

            }
           // model.AttendenceStudentDetail = lstSUDetail;


            return Redirect("/Home/Index");
           // return View("StudentList","Student");
        }
    }
}