﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Models
{
    public class AdminDashboardController : Controller
    {

        //
        // GET: /Principle/


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        public StudentBusiness _sBusiness;
        public EmployeeBusiness _eBusiness;
        public StudentAdmissionBusiness _saBusiness;

        public AdminDashboardController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._sBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._eBusiness = new EmployeeBusiness(_df, this._unitOfWork);
            this._saBusiness = new StudentAdmissionBusiness(_df, this._unitOfWork);
           


        }

        public ActionResult Index()
        {
            AdminDashboardViewModel adminViewModel = new AdminDashboardViewModel();
            adminViewModel.School = _schoolBusiness.GetListWT().Count();
            adminViewModel.Student = _sBusiness.GetListWT().Count();
            adminViewModel.Employee = _eBusiness.GetListWT().Count();
            adminViewModel.PEmployee = _eBusiness.GetListWT().Where(u => u.Employeetype == "Permanent").Count();
            adminViewModel.CEmployee = _eBusiness.GetListWT().Where(u => u.Employeetype == "Contract").Count();
            adminViewModel.studentaddmission = _saBusiness.GetListWT().Count();

            return View(adminViewModel);
        }

	}
}