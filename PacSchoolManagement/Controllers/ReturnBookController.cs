﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;

namespace PacSchoolManagement.Controllers
{
    public class ReturnBookController : Controller
    {
        // GET: ReturnBook

         private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        private BookBusiness _bookbusiness;
        private BookCategoryBusiness _bookcategorybusiness;
        private IssueBookBusiness _issuebookbusiness;
        private ReturnBookBusiness _returnbookbusiness;
        // GET: IssueBook



        public ReturnBookController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._bookbusiness = new BookBusiness(_df, this._unitOfWork);
            this._bookcategorybusiness = new BookCategoryBusiness(_df, this._unitOfWork);
            this._issuebookbusiness = new IssueBookBusiness(_df, this._unitOfWork);
            this._returnbookbusiness = new ReturnBookBusiness(_df, this._unitOfWork);
            }
        public ActionResult Index()
        {
          var record = _returnbookbusiness.GetListWT(x => x.IsDelete == "false");
           // var record = _returnbookbusiness.GetListWT();
            return View(record);
        }
        [HttpGet]
        public ActionResult ReturnBook()
        {
            ReturnBookViewModel returnbookdviewmodel = new ReturnBookViewModel();
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Renual", Value = "Renual" });
            userytype.Add(new SelectListItem { Text = "Return", Value = "Return" });

            returnbookdviewmodel.BookStatusList = userytype;



            return View(returnbookdviewmodel);
        }
        [HttpPost]
        public ActionResult AddReturnBook(ReturnBookViewModel clsmodel)
        {

            
                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                //  var sch = Filters.AuthenticationModel.GlobalUser.GlobalData.SchoolName;


                Mapper.CreateMap<ReturnBookViewModel, Returnbook>();
                Returnbook cls = Mapper.Map<ReturnBookViewModel, Returnbook>(clsmodel);
              cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _returnbookbusiness.AddUpdateDeleteReturnbook(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

         



            return RedirectToAction("Index", "ReturnBook");
            
        }





        public JsonResult SearchBook(string prefix)
        {
            List<Book> allUser = new List<Book>();


            // Here "MyDatabaseEntities " is dbContext, which is created at time of model creation.
            allUser = _bookbusiness.GetListWT(c => c.BookNo.Contains(prefix) || c.Title.Contains(prefix) || c.BookCategory.Contains(prefix)).ToList();
           
                //allUser = dc.UserMasters.Where(a => a.Username.Contains(prefix)).ToList();
           

            return new JsonResult { Data = allUser, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            
        }




        public ActionResult Edit(int txtval)
        {

            var userList = _returnbookbusiness.GetListWT(x => x.ReturnBookID== txtval ).FirstOrDefault();
            List<ReturnBookViewModel> userViewModelList = new List<ReturnBookViewModel>();
            ReturnBookViewModel bimodel = new ReturnBookViewModel();
            bimodel.ReturnBookID = userList.ReturnBookID;
            bimodel.SchoolId = userList.SchoolId;
           bimodel.IsDelete = userList.IsDelete;
            bimodel.ReturnDate = userList.ReturnDate;
            bimodel.Status = userList.Status;
            bimodel.UserName = userList.UserName;
            bimodel.UserId = userList.UserId;
            bimodel.UserName = userList.UserName;
            bimodel.BookName = userList.BookName;
            bimodel.BookofId = userList.BookofId;
            bimodel.BookNo = userList.BookNo;
            bimodel.BookName = userList.BookName;
            bimodel.Remark = userList.Remark;
            bimodel.UserId = userList.UserId;
            bimodel.UserClass = userList.UserClass;
            bimodel.FineAmount = userList.FineAmount;
          
                
           // bimodel.DueDate = userList.DueDate;
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Renual", Value = "Renual" });
            userytype.Add(new SelectListItem { Text = "Return", Value = "Return" });
            bimodel.BookStatusList = userytype;





            return View(bimodel);
        }
        [HttpPost]
        public ActionResult Edit(ReturnBookViewModel clsmodel)
        {


            var userList = _returnbookbusiness.GetListWT(x => x.ReturnBookID == clsmodel.ReturnBookID).ToList().FirstOrDefault();
            userList.ReturnBookID = clsmodel.ReturnBookID;
            userList.Remark = clsmodel.Remark;
            userList.FineAmount = clsmodel.FineAmount;
            userList.BookNo = clsmodel.BookNo;
            userList.BookName = clsmodel.BookName;
            userList.UserClass = clsmodel.UserClass;
            userList.UserId = clsmodel.UserId;
            userList.UserName = clsmodel.UserName;
            userList.Status = clsmodel.Status;
            userList.Section = clsmodel.Section;
            userList.ReturnDate = clsmodel.ReturnDate;
            //userList.
            

           // Mapper.CreateMap<ReturnBookViewModel, Returnbook>();
          //  Returnbook cls = Mapper.Map<ReturnBookViewModel, Returnbook>(clsmodel);




            bool isSuccess = _returnbookbusiness.AddUpdateDeleteReturnbook(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Updated Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to create Update!!";
                TempData["isSuccess"] = "false";
            }




            return RedirectToAction("Index", "ReturnBook");
        }





        public ActionResult Delete(int txtval)
        {
            var userList = _returnbookbusiness.GetListWT(x => x.ReturnBookID == txtval).ToList().FirstOrDefault();

           userList.IsDelete = "true";

            bool isSuccess = _returnbookbusiness.AddUpdateDeleteReturnbook(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return RedirectToAction("Index", "ReturnBook");
        }







    }
}