﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class SchoolRoleController : Controller
    {

         private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private SchoolRoleBusiness _roleBusiness;
        public SchoolRoleController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._roleBusiness = new SchoolRoleBusiness(_df, this._unitOfWork);

        }
        //
        // GET: /SchoolRole/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SchoolRole()
        {

            return View();
        }
        public JsonResult SaveDataInDatabase(SchoolRoleViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.RoleId > 0)
            {
                var userList = _roleBusiness.GetListWT(x => x.RoleId == clsmodel.RoleId).ToList().FirstOrDefault();

                userList.RoleId = clsmodel.RoleId;
                userList.RoleName = clsmodel.RoleName;
                bool isSuccess = _roleBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

            
                
                Mapper.CreateMap<SchoolRoleViewModel, SchoolRole>();
                SchoolRole cls = Mapper.Map<SchoolRoleViewModel, SchoolRole>(clsmodel);
                cls.Schoolid = 0;
                cls.IsDelete = "false";

                bool isSuccess = _roleBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            
             int SchoolId = Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.getGlobalUser().SchoolID);
            var userList = _roleBusiness.GetListWT(x=>x.IsDelete=="false");
            List<SchoolRoleViewModel> userViewModelList = new List<SchoolRoleViewModel>();

            var records = (from p in userList
                           select new SchoolRoleViewModel
                           {
                               RoleId = p.RoleId,
                               RoleName = p.RoleName,
                               Schoolid = p.Schoolid,
                               IsDelete = p.IsDelete,

                           }).Where(i => i.Schoolid == 0).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _roleBusiness.GetListWT(x => x.RoleId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _roleBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _roleBusiness.GetListWT(x => x.RoleId== StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }




	}
}