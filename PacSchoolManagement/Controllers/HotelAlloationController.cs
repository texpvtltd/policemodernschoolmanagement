﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class HotelAlloationController : Controller
    {
        // GET: HotelAlloation




        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostalTypeBusiness _hostaltypeBusiness;
        private HostalDetailBusiness _hostaldetailBusiness;
        private HostelAllocationBusiness _hosteAllbusiness;
        private AddHostelRoomBusiness _hostelRoomBusiness;
        public TeacherBusiness _teacherBusiness;
        private StudentBusiness _studentBusiness;
        public HotelAlloationController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this ._hostaltypeBusiness=new HostalTypeBusiness (_df,this._unitOfWork );
            this._hostaldetailBusiness = new HostalDetailBusiness(_df, this._unitOfWork);
            this._hosteAllbusiness = new HostelAllocationBusiness(_df, this._unitOfWork);
            this._hostelRoomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);

            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
        }



        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HostalAllocationList()
        {
            HostelAllocationViewModel hostelalloctionviewmodel = new HostelAllocationViewModel();

            List<SelectListItem> UserTypeList = new List<SelectListItem>();
            UserTypeList.Add(new SelectListItem { Text = "Student", Value = "Student" });
            UserTypeList.Add(new SelectListItem { Text = "Employee", Value = "Empolyee" });


            hostelalloctionviewmodel.UserTypeList = UserTypeList;



            var hostype = _hostaltypeBusiness.GetListWT();
            hostelalloctionviewmodel.HostalTypeList = hostype.Select(x => new SelectListItem
            {
                Text = x.HostalType1.ToString(),
                Value = x.HostalTypeID.ToString(),
            }).ToList();
            var hostaldata = _hostaldetailBusiness.GetListWT();
            hostelalloctionviewmodel.HostalNameList = hostaldata.Select(x => new SelectListItem
            {
                Text = x.HostalName.ToString(),
                Value = x.HostalId.ToString()
            }).ToList();
            var RoomType = _hostelRoomBusiness.GetListWT();
            hostelalloctionviewmodel.HostalRoomList = RoomType.Select(x => new SelectListItem
            {
                Text = x.RoomNo.ToString(),
                Value = x.RoomId.ToString(),
            }).ToList();

            var student = _studentBusiness.GetListWT();
            hostelalloctionviewmodel.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();

            var Emploeyee = _teacherBusiness.GetListWT();
            hostelalloctionviewmodel.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName+""+x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();





            return View(hostelalloctionviewmodel);
            // return View();
        }


        public JsonResult SaveDataInDatabase(HostelAllocationViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.HostelAllocatedId > 0)
            {
                var userList = _hosteAllbusiness.GetListWT(x => x.HostelAllocatedId == clsmodel.HostelAllocatedId).ToList().FirstOrDefault();

                userList.HostelAllocatedId = clsmodel.HostelAllocatedId;
                userList.HostelName = clsmodel.HostelName;
                userList.HostalType = clsmodel.HostalType;
                userList.HostelRegdate = clsmodel.HostelRegdate;
                userList.HostelRoom = clsmodel.HostelRoom;
                userList.UsarName = clsmodel.UsarName;
                userList.UserType = clsmodel.UserType;


                bool isSuccess = _hosteAllbusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<HostelAllocationViewModel, HostelAllocation>();
                HostelAllocation cls = Mapper.Map<HostelAllocationViewModel, HostelAllocation>(clsmodel);
                cls.SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                cls.UsarName = clsmodel.Employee;
                bool isSuccess = _hosteAllbusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            // int SchoolId = Convert.ToInt32(GlobalUser.getGlobalUser().SchoolID);
            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _hosteAllbusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostelAllocationViewModel
                           {
                               SchoolId = p.SchoolId,
                              // HostalType = p.HostalType,
                               HostelAllocatedId = p.HostelAllocatedId,
                               HostalType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostelName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostalName,
                               HostelRegdate = p.HostelRegdate,
                               HostelRoom = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).RoomNo,
                              // HostelRoom = p.HostelRoom,
                               VacatingDate = p.VacatingDate,
                               FloorName = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).HostalfloorName,
                               UserType = p.UserType,
                               UsarName = p.UsarName,

                           });
            // .Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _hosteAllbusiness.GetListWT(x => x.HostelAllocatedId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _hosteAllbusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _hosteAllbusiness.GetListWT(x => x.HostelAllocatedId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RequestList( )

        {

            var SchoolId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var userList = _hosteAllbusiness.GetListWT(x => x.IsDelete == "false");
            List<HostalDetailViewModel> userViewModelList = new List<HostalDetailViewModel>();

            var records = (from p in userList
                           select new HostelAllocationViewModel
                           {
                               SchoolId = p.SchoolId,
                               // HostalType = p.HostalType,
                               HostelAllocatedId = p.HostelAllocatedId,
                               HostalType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostalType)).HostalType1,
                               HostelName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostalName,
                               HostelRegdate = p.HostelRegdate,
                               HostelRoom = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).RoomNo,
                               // HostelRoom = p.HostelRoom,
                               VacatingDate = p.VacatingDate,
                               FloorName = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).HostalfloorName,
                               UserType = p.UserType,
                               UsarName = p.UsarName,

                           });




            return View(records);
        }

        [HttpPost]
        public ActionResult RequestList( string s)
        {

           

            return View();
        }


    }
}