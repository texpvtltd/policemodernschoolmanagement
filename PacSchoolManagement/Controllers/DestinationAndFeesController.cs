﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class DestinationAndFeesController : Controller
    {
        // GET: DestinationAndFees
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private StudentBusiness _studentBusiness;
        private ClassBusiness _classBusiness;
        private SectionBusiness _sectionBusiness;
        private StudentDocumentBusiness _studentdocumentBusiness;
        private Destination_FeesBusiness _destination_fessBusiness;
        private AddTransportBusiness _addtransportbusiness;
     //   private AddRauteBusiness _addrautebusiness;
        public DestinationAndFeesController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._studentdocumentBusiness = new StudentDocumentBusiness(_df, this._unitOfWork);
            this._destination_fessBusiness = new Destination_FeesBusiness(_df, this._unitOfWork);
            this._addtransportbusiness = new AddTransportBusiness(_df, this._unitOfWork);
         //   this._addrautebusiness = new AddRauteBusiness(_df, this._unitOfWork);
        } 

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DestinationAndFeesList()
        {
            DestinationandFeesViewModel destinationfeesviewmodel = new DestinationandFeesViewModel();
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Annual", Value = "Annual" });
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });

            destinationfeesviewmodel.FeesTypeList = userytype;


           // var raute = _addrautebusiness.GetListWT();
            //destinationfeesviewmodel.RauteCodeList = raute.Select(x => new SelectListItem
            //{
            //    Text = x.RouteCode.ToString(),
            //    Value = x.RouteID.ToString()
            //}).ToList();

            return View(destinationfeesviewmodel);
        }

        public JsonResult SaveDataInDatabase(DestinationandFeesViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.DestinationId > 0)
            {
                var userList = _destination_fessBusiness.GetListWT(x => x.DestinationId== clsmodel.DestinationId).ToList().FirstOrDefault();
                userList.FeeType = clsmodel.FeeType;
                userList.Amount = clsmodel.Amount;
                userList.PickupDrop = clsmodel.PickupDrop;
                userList.RouteCode = clsmodel.RouteCode;
                userList.StopTime = clsmodel.StopTime;
               
               
                userList.IsDelete = "false";
                bool isSuccess = _destination_fessBusiness.AddUpdateDeleteAddDestination_Fees(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);

                Mapper.CreateMap<DestinationandFeesViewModel, AddDestination_Fees>();
                AddDestination_Fees destinationist = Mapper.Map<DestinationandFeesViewModel, AddDestination_Fees>(clsmodel);
                destinationist.SchoolId= Convert.ToInt32(SchoolId).ToString();
                destinationist.IsDelete = "false";
                // cls.TokenKey = GlobalMethods.GetToken();
                bool isSuccess = _destination_fessBusiness.AddUpdateDeleteAddDestination_Fees(destinationist, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Section!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetStudentList()  //Gets the todo Lists.
        {

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _destination_fessBusiness.GetAllUsers();
            List<SectionViewModel> userViewModelList = new List<SectionViewModel>();

            var records = (from p in userList
                           select new DestinationandFeesViewModel
                           {
                               DestinationId = p.DestinationId,
                               FeeType = p.FeeType,
                               SchoolId = p.SchoolId,
                               StopTime=p.StopTime,
                               RouteCode=p.RouteCode,
                               Amount=p.RouteCode,
                             

                           }).Where(i => i.SchoolId== SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }




        public JsonResult GetStudentById(int StudentId)
        {
            var model = _destination_fessBusiness.GetListWT(x => x.DestinationId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _destination_fessBusiness.GetListWT(x => x.DestinationId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _destination_fessBusiness.AddUpdateDeleteAddDestination_Fees(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }





    }
}