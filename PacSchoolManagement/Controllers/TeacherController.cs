﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class TeacherController : Controller
    {
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        public TeacherController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

        }

        // GET: Teacher
        [HttpGet]
        public ActionResult Teacher()
        {
            //SchoolUserViewModel schooluserViewModel = new SchoolUserViewModel();
            //var schooluserList1 = _teacherBusiness.GetListWT();
            //schooluserViewModel.schooluserList = schooluserList1.Select(x => new SelectListItem
            //{
            //    Text = x.FName.ToString() + "" + x.LName.ToString(),
            //    Value = x.SchoolUserID.ToString()
            //}).ToList();

            //   int SchoolId = Convert.ToInt32(Filters.AuthenticationModel.GlobalUser.GlobalData.SchoolID);
            SchoolUserViewModel userviewmodel = new SchoolUserViewModel();
            List<SelectListItem> ListUtyp = new List<SelectListItem>();
            ListUtyp.Add(new SelectListItem { Text = "Teacher", Value = "Teacher" });
            ListUtyp.Add(new SelectListItem { Text = "NonTeacher", Value = "NonTeacher" });
            // ListUtyp.Add(new SelectListItem { Text = "Factory", Value = "Factory" });
            userviewmodel.schooluserList = ListUtyp;

            return View(userviewmodel);
        }
        [HttpPost]
        public ActionResult Teacher(string s)
        {

            return View();

        }
   
        // POST: /Class/Create
        public JsonResult SaveDataInDatabase(SchoolUserViewModel clsmodel, HttpPostedFileBase file)
        {
            // var result = false;
            if (clsmodel.SchoolUserID > 0)
            {
                var userList = _teacherBusiness.GetListWT(x => x.SchoolUserID == clsmodel.SchoolUserID).ToList().FirstOrDefault();

                userList.SchoolID = clsmodel.SchoolUserID;
                userList.FName = clsmodel.FName;
                userList.LName = clsmodel.LName;
                userList.UserImage = clsmodel.UserImage;
                userList.UserType = userList.UserType;
                //Upload User Image*****
                if (clsmodel.ImageUpload.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(clsmodel.ImageUpload.FileName);
                    var path = Path.Combine(Server.MapPath("~/SchoolImage"), fileName);
                    var fileName1 = Path.GetFileName(file.FileName);
                    clsmodel.ImageUpload.SaveAs(path);
                    //* For Adhar Upload
                    //var Adharfile = Path.GetFileName(clsmodel.AdharUpload.FileName);
                    //var Adharpath = Path.Combine(Server.MapPath("~/Content/AdharImage"), Adharfile);
                    //clsmodel.AdharUpload.SaveAs(Adharpath);
                    //userList.AdhaarImage = Adharfile;
                    userList.UserImage = fileName;
                    userList.IsDelete = "true";
                }

                bool isSuccess = _teacherBusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;

                Mapper.CreateMap<SchoolUserViewModel, SchoolUser>();
                SchoolUser cls = Mapper.Map<SchoolUserViewModel, SchoolUser>(clsmodel);
                clsmodel.SchoolID = Convert.ToInt32(currentUserId);

                var userList = _userBusiness.GetListWT().ToList().FirstOrDefault();
                userList.Password = Md5Encryption.Encrypt(clsmodel.Passward);
                userList.UserType = clsmodel.UserType;
                // userList.UserId =clsmodel.UserID;
                if (clsmodel.ImageUpload.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(clsmodel.ImageUpload.FileName);
                    var path = Path.Combine(Server.MapPath("~/SchoolImage"), fileName);
                    //var fileName1 = Path.GetFileName(file.FileName);
                    clsmodel.ImageUpload.SaveAs(path);
                    cls.UserImage = fileName;
                    //* For Adhar Upload
                    //var Adharfile = Path.GetFileName(clsmodel.AdharUpload.FileName);
                    // var Adharpath = Path.Combine(Server.MapPath("~/Content/AdharImage"),Adharfile);
                    // clsmodel.AdharUpload.SaveAs(Adharpath);
                    // cls.AdhaarImage = Adharfile;
                    cls.IsDelete = "false";
                    cls.UserName = clsmodel.UserName;
                }

                bool isSuccess1 = _userBusiness.AddUpdateDeleteUser(userList, "I");
                bool isSuccess = _teacherBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = " Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create teacher!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId1 = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var currentUserId = _userBusiness.Find(currentUserId1).SchoolID;
            int SchoolId = Convert.ToInt32(currentUserId);

            var userList = _teacherBusiness.GetListWT(x=>x.IsDelete=="false");
            List<SchoolUserViewModel> userViewModelList = new List<SchoolUserViewModel>();

            var records = (from p in userList
                           select new SchoolUserViewModel
                           {
                               FName = p.FName,
                               LName = p.LName,
                               UserImage = "/SchoolImage/" + p.UserImage,
                               UserType = p.UserType,
                               UserName = p.UserName.ToString(),

                             SchoolUserID = p.SchoolUserID,
                           }).Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }




        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _teacherBusiness.GetListWT(x => x.SchoolUserID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _teacherBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetStudentById(int SchoolUserID)
        {

            var model = _teacherBusiness.GetListWT(x => x.SchoolUserID == SchoolUserID).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }
        //

    }
}