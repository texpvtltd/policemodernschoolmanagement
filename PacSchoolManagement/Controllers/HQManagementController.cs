﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class HQManagementController : Controller
    {      
        
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
         public SchoolBusiness _schoolBusiness;
         public PrincipalBusiness _princiBusiness;
         public SessionBusiness _sessionBusiness;
         private UserLoginBusiness _userBusiness;
         public HQManagementController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);


        }









        // GET: HQManagement
        [HttpGet]
        public ActionResult PrincipalList()
        {
            PrincipalViewModel principleViewModel = new PrincipalViewModel();
            var schoolList1 = _sessionBusiness.GetListWT();
            principleViewModel.PricipalList = schoolList1.Select(x => new SelectListItem
            {
                Text = x.Session1.ToString(),
                Value = x.SessionID.ToString()
            }).ToList();

            var schoolList2 = _schoolBusiness.GetListWT();
            principleViewModel.schoollList = schoolList2.Select(x => new SelectListItem
            {
                Text = x.SchoolName.ToString(),
                Value = x.SchoolID.ToString()
            }).ToList();
            return View(principleViewModel);

        }
     
        public ActionResult PrincipalList(string s)
        {
            return View();

        }





        //
        // POST: /Class/Create

  




        public JsonResult GetStudentById1(int PrincipalID)
        {
            var model = _princiBusiness.GetListWT(x => x.PrincipalID == PrincipalID).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        //Start Session::

        // GET: Session




        [HttpGet]
        public ActionResult SessionList()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SessionList(string s)
        {

            return View();

        }

        //
        // POST: /Class/Create
        public JsonResult SaveDataInDatabaseSession(SessionViewModel clsmodel)
        {
            // var result = false;
            //if (clsmodel.ClassID > 0)
            //{  

          //  if(clsmodel.FromYear==clsmodel.ToYear+1)
           // { 
            var currentUserId = GlobalUser.getGlobalUser().UserId;
            
            Mapper.CreateMap<SessionViewModel, Session>();
            Session cls = Mapper.Map<SessionViewModel, Session>(clsmodel);
            clsmodel.SessionID = Convert.ToInt32(currentUserId);
            // cls.TokenKey = GlobalMethods.GetToken();
            cls.Session1 = clsmodel.FromYear + "-" + clsmodel.ToYear;
            bool isSuccess = _sessionBusiness.AddUpdateDeleteClass(cls, "I");
            if (isSuccess)
            {
                TempData["Success"] = "Principal Added Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to add principal!!";
                TempData["isSuccess"] = "false";
            }
            // result = true;
           // }
            //  }
           // else
           // {

            //    TempData["Success"] = "Plz Select Valied year!";
            //    TempData["isSuccess"] = "false";
           // }
            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSessionList()  //Gets the todo Lists.
        {


            var userList = _sessionBusiness.GetListWT();
            List<SessionViewModel> userViewModelList = new List<SessionViewModel>();

            var records = (from p in userList
                           select new SessionViewModel
                           {
                              SessionID=p.SessionID,
                              ToYear=p.Session1,
                              SchoolID=p.SchoolID
                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _sessionBusiness.GetListWT(x => x.SessionID== StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            string JsonStr = "";
            bool isSuccess = true;
            string message = "Delete Successful!";
            _unitOfWork.BeginTransaction();

          //  var Stu = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();
            if (StudentId != null)
            {
                var br = _sessionBusiness.Find(StudentId);

                _sessionBusiness.Update(br);
                _unitOfWork.SaveChanges();

            }
            //List<Class> userList = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList();
            //foreach (Class usr in userList)
            //{
            //    int deleteId = Convert.ToInt32(usr.ClassID);
            //   // var br = _classBusiness.Find(deleteId);
            //    _classBusiness.Deleteid(deleteId);
            //    _unitOfWork.SaveChanges();
            //}

            _unitOfWork.Commit();



            TempData["Success"] = message;
            TempData["isSuccess"] = isSuccess.ToString();

            JsonStr = "{\"message\":\"" + message + "\",\"isSuccess\":\"" + isSuccess + "\"}";
            return Json(JsonStr, JsonRequestBehavior.AllowGet);
        }


 



    }
}