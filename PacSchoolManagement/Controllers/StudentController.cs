﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{




    public class StudentController : Controller
    {
        // GET: Student

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private StudentBusiness _studentBusiness;
        private ClassBusiness _classBusiness;
        private SectionBusiness _sectionBusiness;
        private StudentDocumentBusiness _studentdocumentBusiness;
        public StudentController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._studentdocumentBusiness = new StudentDocumentBusiness(_df, this._unitOfWork);
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult StudentList()
        {
            StudentViewModel studentviewmodel = new StudentViewModel();

            List<SelectListItem> Listgender = new List<SelectListItem>();
            Listgender.Add(new SelectListItem { Text = "Male", Value = "Male" });
            Listgender.Add(new SelectListItem { Text = "Felmale", Value = "Female" });

            studentviewmodel.GenderList = Listgender;
            var classList1 = _classBusiness.GetListWT();
            studentviewmodel.ClassList = classList1.Select(x => new SelectListItem
            {
                Text = x.ClassName.ToString(),
                Value = x.ClassID.ToString()
            }).ToList();
            var section = _sectionBusiness.GetListWT();
            studentviewmodel.SectionList = section.Select(x => new SelectListItem
            {
                Text = x.SectionName.ToString(),
                Value = x.SectionID.ToString()
            }).ToList();
            var session = _sessionBusiness.GetListWT();
            studentviewmodel.SessionList = session.Select(x => new SelectListItem
            {
                Text = x.SessionID.ToString(),
                Value = x.Session1.ToString()
            }).ToList();



            return View(studentviewmodel);
        }





        public JsonResult SaveDataInDatabase(StudentViewModel model)
        {
            // var result = false;
            //if (clsmodel.ClassID > 0)
            //{  

            //  if(clsmodel.FromYear==clsmodel.ToYear+1)
            // { 
            var currentUserId = GlobalUser.getGlobalUser().UserId;

            Mapper.CreateMap<StudentViewModel, Student>();
            Student stu = Mapper.Map<StudentViewModel, Student>(model);
            model.SchoolID = Convert.ToInt32(currentUserId);
            // cls.TokenKey = GlobalMethods.GetToken();

            bool isSuccess = _studentBusiness.AddUpdateDeleteClass(stu, "I");
            if (isSuccess)
            {
                TempData["Success"] = "Student register Added Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Failed to addStudent!!";
                TempData["isSuccess"] = "false";
            }
            // result = true;
            // }
            //  }
            // else
            // {

            //    TempData["Success"] = "Plz Select Valied year!";
            //    TempData["isSuccess"] = "false";
            // }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetStudentList()  //Gets the todo Lists.
        {


            var userList = _studentBusiness.GetListWT();
            List<StudentViewModel> userViewModelList = new List<StudentViewModel>();
          int SchoolId = Convert.ToInt32( GlobalUser.getGlobalUser().SchoolID);
            var records = (from p in userList
                           select new StudentViewModel
                           {
                               FirstName = p.FatherName,
                               FatherName = p.FatherName,
                               LastName = p.LastName,
                               ClassID = p.ClassID,
                               StudentID = p.StudentID,
                               SessinId = p.Session.ToString(),
                               Gender = p.Gender,
                               UserID = p.UserID,
                               RegdNo = p.RegdNo,
                               Section = p.Section,
                               SchoolID = p.SchoolID

                           }).AsQueryable();
            //.Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GeneratePass(int StudentID)
        {
            StudentViewModel stu = new StudentViewModel();
            var model1 = _studentBusiness.GetListWT(x => x.StudentID == StudentID).ToList().FirstOrDefault();

            stu.SchoolID = model1.SchoolID;
            stu.FirstName = model1.FirstName + "" + model1.LastName;
            stu.FatherName = model1.FatherName;
            stu.FatherName = model1.FatherName;
            stu.Gender = model1.Gender;
            stu.StudentID = model1.StudentID;
            return View(stu);


        }
        public ActionResult GenerateIdCard(string StudentID)
        {
            int StudentID1 = Convert.ToInt32(StudentID);
            StudentViewModel stu = new StudentViewModel();
            var model1 = _studentBusiness.GetListWT(x => x.StudentID == StudentID1).ToList().FirstOrDefault();

            //  stu.SchoolID = model1.SchoolID;
            stu.FirstName = model1.FirstName + "" + model1.LastName;
            //stu.FatherName = model1.FatherName;
            stu.FatherName = model1.FatherName;
            stu.Gender = model1.Gender;
            stu.StudentID = model1.StudentID;
            return View(stu);
            // return RedirectToAction("GenerateIdCard", "Student", new {StudentID=stu.StudentID});
            // return RedirectToAction("GenerateIdCard","Student");
        }


        public ActionResult GenerateTC(int StudentID)
        {

            StudentViewModel stu = new StudentViewModel();
            var model1 = _studentBusiness.GetListWT(x => x.StudentID == StudentID).ToList().FirstOrDefault();
            stu.StudentID = model1.StudentID;
            stu.SchoolID = model1.SchoolID;
            stu.FirstName = model1.FirstName + "" + model1.LastName;
            stu.FatherName = model1.FatherName;
            // stu.FatherName = model1.FatherName;
            stu.Gender = model1.Gender;
            // stu.ReturnDate = DateTime.Now.ToString();
            return View(stu);


        }


        [HttpGet]
        public ActionResult UploadDocument(int StudentID)
        {

            StudentViewModel stu = new StudentViewModel();
            var model1 = _studentBusiness.GetListWT(x => x.StudentID == StudentID).ToList().FirstOrDefault();
            stu.StudentID = model1.StudentID;
            stu.SchoolID = model1.SchoolID;
            stu.FirstName = model1.FirstName + "" + model1.LastName;
            stu.FatherName = model1.FatherName;
            // stu.FatherName = model1.FatherName;
            stu.Gender = model1.Gender;
            // stu.ReturnDate = DateTime.Now.ToString();
            return View(stu);


        }
        [HttpPost]
        public ActionResult UploadDocument(HttpPostedFileBase postedFile, StudentViewModel stu)
        {
            StudentUploadDocViewModel stumodel = new StudentUploadDocViewModel();

            //byte[] bytes;
            //using (BinaryReader br = new BinaryReader(postedFile.InputStream))
            //{
            //    bytes = br.ReadBytes(postedFile.ContentLength);
            //}

            if (postedFile.ContentLength > 0)
            {
                var fileName = Path.GetFileName(postedFile.FileName);
                var path = Path.Combine(Server.MapPath("~/UploadsDocument"), fileName);

                postedFile.SaveAs(path);
             //  stumodel.SchoolId = Convert.ToInt32(GlobalUser.GlobalData.SchoolName).ToString();
               stumodel.StudentId = stu.StudentID.ToString();
               stumodel.SchoolName = stu.SchoolID.ToString();

                stumodel.FilePath = path;
                stumodel.FileName = fileName;
               
                Mapper.CreateMap<StudentUploadDocViewModel, StudentDocument>();
                StudentDocument studoc = Mapper.Map<StudentUploadDocViewModel, StudentDocument>(stumodel);
                bool isSuccess = _studentdocumentBusiness.AddUpdateDeleteClass(studoc, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Student register Added Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to addStudent!!";
                    TempData["isSuccess"] = "false";
                }

            }

           return RedirectToAction("GenerateIdCard","Student");

            //return View();





        }
    }
}