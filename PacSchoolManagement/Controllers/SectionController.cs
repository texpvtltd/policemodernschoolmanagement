﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class SectionController : Controller
    {
        //
        // GET: /Section/


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public ClassBusiness _classBusiness;
        public SectionBusiness _sectionBusiness;
        public UserLoginBusiness _userBusiness;

        public SectionController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

        }
        public ActionResult Section()
        {
            return View();
        }
        public JsonResult SaveDataInDatabase(SectionViewModel sectionmodel)
        {
            // var result = false;
            if (sectionmodel.SectionID > 0)
            {
                var userList = _sectionBusiness.GetListWT(x => x.SectionID == sectionmodel.SectionID).ToList().FirstOrDefault();

                userList.SectionID = sectionmodel.SectionID;
                userList.SectionName = sectionmodel.SectionName;
                userList.IsDelete = "false";
                bool isSuccess = _sectionBusiness.AddUpdateDeleteData(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);

                Mapper.CreateMap<SectionViewModel, Section>();
                Section sectionlist = Mapper.Map<SectionViewModel,Section>(sectionmodel);
                sectionlist.SchoolID = Convert.ToInt32(SchoolId);
                sectionlist.IsDelete = "false";
                // cls.TokenKey = GlobalMethods.GetToken();
                bool isSuccess = _sectionBusiness.AddUpdateDeleteData(sectionlist, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Section!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(sectionmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _sectionBusiness.GetAllUsers();
            List<SectionViewModel> userViewModelList = new List<SectionViewModel>();

            var records = (from p in userList
                           select new SectionViewModel
                           {
                               SectionName = p.SectionName,
                               SectionID = p.SectionID,
                               SchoolID = p.SchoolID

                           }).Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }



 
        public JsonResult GetStudentById(int StudentId)
        {
            var model = _sectionBusiness.GetListWT(x => x.SectionID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }


        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _sectionBusiness.GetListWT(x => x.SectionID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _sectionBusiness.AddUpdateDeleteData(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }









	}
}