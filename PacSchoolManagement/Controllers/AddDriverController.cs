﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class AddDriverController : Controller
    {
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private AddTransportBusiness _addtranport;
        // GET: AddDriver
        private AddDriverBusiness _adddriverbusiness;
        public AddDriverController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._addtranport = new AddTransportBusiness(_df, this._unitOfWork);
            this._adddriverbusiness = new AddDriverBusiness(_df, this._unitOfWork);
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DriverList()
        {
            DriverViewModel dr = new DriverViewModel();
            var rauteNo = _addtranport.GetListWT();
            dr.RouteNoList = rauteNo.Select(x => new SelectListItem
            {
                Text = x.VehicleNo.ToString(),
                Value = x.VehicleID.ToString()
            }).ToList();



            return View(dr);

        }



        public JsonResult SaveDataInDatabase(DriverViewModel clsmodel)
        {
            // var result = false;
            //if (clsmodel.ClassID > 0)
            //{  

            if (clsmodel.DriverId > 0)
            {
                var userList = _adddriverbusiness.GetListWT(x => x.DriverId == clsmodel.DriverId).ToList().FirstOrDefault();
                userList.DriverId = clsmodel.DriverId;
                userList.DriverName = clsmodel.DriverName;
                userList.PermanentAddress = clsmodel.LicenseNumber;

                userList.PresentAddress = clsmodel.PresentAddress;
                userList.LicenseNumber = clsmodel.LicenseNumber;
                userList.VehicleNo = clsmodel.VehicleNo;
                userList.DateofBirth = clsmodel.DateofBirth;
                userList.ContactNo = clsmodel.ContactNo;

                bool isSuccess =_adddriverbusiness.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create updated!!";
                    TempData["isSuccess"] = "false";
                }
            }


            else
            {


                var currentUserId = GlobalUser.getGlobalUser().UserId;

                Mapper.CreateMap<DriverViewModel, AddDriver>();
                AddDriver cls = Mapper.Map<DriverViewModel, AddDriver>(clsmodel);
                cls.IsDelete = "false";
                cls.SchoolId = currentUserId.ToString();


                bool isSuccess = _adddriverbusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Driver Added Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Driver to add principal!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;
            }
            //  }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrinciList()  //Gets the todo Lists.
        {


            var userList = _adddriverbusiness.GetListWT(x => x.IsDelete == "false"); ;
            List<DriverViewModel> userViewModelList = new List<DriverViewModel>();

            var records = (from p in userList
                           select new DriverViewModel
                           {
                               DateofBirth = p.DateofBirth,
                               ContactNo = p.ContactNo,
                               DriverId = p.DriverId,
                               DriverName = p.DriverName,
                               LicenseNumber = p.LicenseNumber,
                               PermanentAddress = p.PermanentAddress,
                               PresentAddress = p.PresentAddress,
                               VehicleNo = p.VehicleNo,
                              

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _adddriverbusiness.GetListWT(x => x.DriverId == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _adddriverbusiness.GetListWT(x => x.DriverId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _adddriverbusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

    }
}