﻿using Business;
using Entities.Models;
using PacSchoolManagement.Models;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace PacSchoolManagement.Controllers
{
    public class EventController : Controller
    {
        //
        // GET: /Event/
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public eventBusiness _eventBusiness;
        public EventController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._eventBusiness = new eventBusiness(_df, this._unitOfWork);
         }
        public ActionResult Index()
        {
            return View();
        }

        #region Get Calendar data method.

        /// <summary>  
        /// GET: /Home/GetCalendarData  
        /// </summary>  
        /// <returns>Return data</returns>  
        public ActionResult GetCalendarData()
        {
            // Initialization.  
            JsonResult result = new JsonResult();

            try
            {
                // Loading.  
                List<Event> data = this.LoadData();

                // Processing.  
                result = this.Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Info  
                Console.Write(ex);
            }

            // Return info.  
            return result;
        }

        #endregion

        #region Helpers

        #region Load Data

        /// <summary>  
        /// Load data method.  
        /// </summary>  
        /// <returns>Returns - Data</returns>  
        private List<Event> LoadData()
        {
            // Initialization.  
            List<Event> lst = new List<Event>();

            try
            {
                // Initialization.  
                string line = string.Empty;
                string srcFilePath = "Content/files/PublicHoliday.txt";
                var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                var fullPath = Path.Combine(rootPath, srcFilePath);
                string filePath = new Uri(fullPath).LocalPath;
                StreamReader sr = new StreamReader(new FileStream(filePath, FileMode.Open, FileAccess.Read));

                // Read file.  
                while ((line = sr.ReadLine()) != null)
                {
                    // Initialization.  
                    Event infoObj = new Event();
                    string[] info = line.Split(',');

                    // Setting.  
                    infoObj.Id = Convert.ToInt32(info[0].ToString());
                    infoObj.Subject = info[1].ToString();
                    infoObj.Description = info[2].ToString();
                    infoObj.Start = Convert.ToDateTime(info[3]);
                    infoObj.End = Convert.ToDateTime(info[4]);

                    // Adding.  
                    lst.Add(infoObj);
                }

                // Closing.  
                sr.Dispose();
                sr.Close();
            }
            catch (Exception ex)
            {
                // info.  
                Console.Write(ex);
            }

            // info.  
            return lst;
        }

        #endregion

        #endregion  
	}
}