﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
using Filters.ActionFilters;
using Filters;



namespace PacSchoolManagement.Controllers
{
    public class HostelTransferController : Controller
    {

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        private DepartmentBusiness _deptBusiness;
        private UserLoginBusiness _userBusiness;
        private HostelVisitorBusiness _hostelvisitorBusiness;
        private StudentBusiness _studentBusiness;
        private AddHostelRoomBusiness _roomBusiness;
        private HostelAllocationBusiness _allocationBusiness;
        public TeacherBusiness _teacherBusiness;
        public HostelRegisterBusiness _hostelregisterbusiness;
        public HostelTransferBusiness _hosteltransferbusiness;


        private HostalTypeBusiness _hostaltypeBusiness;
        private HostalDetailBusiness _hostaldetailBusiness;
          private AddHostelRoomBusiness _hostelRoomBusiness;
        public HostelTransferController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._hostelvisitorBusiness = new HostelVisitorBusiness(_df, this._unitOfWork);
            this._studentBusiness = new StudentBusiness(_df, this._unitOfWork);
            this._roomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
            this._allocationBusiness = new HostelAllocationBusiness(_df, this._unitOfWork);

            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._hostelregisterbusiness = new HostelRegisterBusiness(_df, this._unitOfWork);
            this._hosteltransferbusiness = new HostelTransferBusiness(_df, this._unitOfWork);
            this._hostaltypeBusiness = new HostalTypeBusiness(_df, this._unitOfWork);
            this._hostaldetailBusiness = new HostalDetailBusiness(_df, this._unitOfWork);
           
            this._hostelRoomBusiness = new AddHostelRoomBusiness(_df, this._unitOfWork);
        }

        // GET: HostelTransfer
        public ActionResult Index()
        {



            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            //int SchoolId = 1;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
            var userList = _hosteltransferbusiness.GetListWT();


            var records = (from p in userList
                           select new HostelTransferViewModel
                           {
                               HostelRoom = _hostelRoomBusiness.GetUserById(Convert.ToInt32(p.HostelRoom)).RoomNo,
                               HostelType = _hostaltypeBusiness.GetUserById(Convert.ToInt32(p.HostelType)).HostalType1,
                               SchoolID = p.SchoolID,
                               IsDelete = p.IsDelete,
                               HostelName = _hostaldetailBusiness.GetUserById(Convert.ToInt32(p.HostelName)).HostalName,
                             
                               UserName = p.UserName,
                               TransferId = p.TransferId,
                               Status = p.Status,
                              
                           }).Where(i => i.SchoolID == SchoolId.ToString()).AsQueryable();





            return View(records);


            
        }
        public ActionResult TransferList()
        {
            HostelTransferViewModel hosteltransfer = new HostelTransferViewModel();
            var student = _studentBusiness.GetListWT();
            hosteltransfer.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();
            var Emploeyee = _teacherBusiness.GetListWT();
            hosteltransfer.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName + "" + x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();

            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            hosteltransfer.UserTypeList = userytype;

            List<SelectListItem> status = new List<SelectListItem>();
            status.Add(new SelectListItem { Text = "Tranfer", Value = "Transfer" });
            status.Add(new SelectListItem { Text = "Vacate", Value = "Vacate" });

            hosteltransfer.transfersstatusList = status;
            var hostype = _hostaltypeBusiness.GetListWT();
            hosteltransfer.HostelTypeList = hostype.Select(x => new SelectListItem
            {
                Text = x.HostalType1.ToString(),
                Value = x.HostalTypeID.ToString(),
            }).ToList();
            var hostaldata = _hostaldetailBusiness.GetListWT();
            hosteltransfer.HostelNameList = hostaldata.Select(x => new SelectListItem
            {
                Text = x.HostalName.ToString(),
                Value = x.HostalId.ToString()
            }).ToList();
            var RoomType = _hostelRoomBusiness.GetListWT();
            hosteltransfer.RoomList = RoomType.Select(x => new SelectListItem
            {
                Text = x.RoomNo.ToString(),
                Value = x.RoomId.ToString(),
            }).ToList();


            return View(hosteltransfer);







           
        }

        public JsonResult SaveDataInDatabase(HostelTransferViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.TransferId > 0)
            {
                var userList = _hosteltransferbusiness.GetListWT(x => x.TransferId == clsmodel.TransferId).ToList().FirstOrDefault();

                userList.HostelName = clsmodel.HostelName;
                userList.UserName = clsmodel.UserName;
                userList.TransferId = clsmodel.TransferId;
                userList.UsesType = clsmodel.UsesType;
                userList.SchoolID = clsmodel.SchoolID;




                bool isSuccess = _hosteltransferbusiness.AddUpdateDeleteHostelTransfer(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;



                Mapper.CreateMap<HostelTransferViewModel, HostelTransfer>();
                HostelTransfer cls = Mapper.Map<HostelTransferViewModel, HostelTransfer>(clsmodel);
                cls.SchoolID = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _hosteltransferbusiness.AddUpdateDeleteHostelTransfer(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }



        public ActionResult EditTransfer(int txtval)
        {
            HostelTransferViewModel clsmodel = new HostelTransferViewModel();


            var student = _studentBusiness.GetListWT();
            clsmodel.StudentList = student.Select(x => new SelectListItem
            {
                Text = x.FirstName + "" + x.LastName.ToString(),
                Value = x.StudentID.ToString(),
            }).ToList();
            var Emploeyee = _teacherBusiness.GetListWT();
            clsmodel.EmpolyeeList = Emploeyee.Select(x => new SelectListItem
            {
                Text = x.FName + "" + x.LName.ToString(),
                Value = x.UserID.ToString(),
            }).ToList();
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
            userytype.Add(new SelectListItem { Text = "Employee", Value = "Employee" });

            clsmodel.UserTypeList = userytype;


            var userList = _hosteltransferbusiness.GetListWT(x => x.TransferId == txtval).ToList().FirstOrDefault();
            clsmodel.UsesType = userList.UsesType;
            clsmodel.SchoolID = userList.SchoolID;
            clsmodel.UserName = userList.UserName;
            clsmodel.HostelName = userList.HostelName;
            clsmodel.HostelRoom = userList.HostelRoom;
            clsmodel.Status = userList.Status;
            clsmodel.HostelType = userList.HostelType;
           


            return View(clsmodel);

        }


        public ActionResult DeleteStudentRecord(int StudentId)
        {
            var userList = _hosteltransferbusiness.GetListWT(x => x.TransferId == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _hosteltransferbusiness.AddUpdateDeleteHostelTransfer(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return RedirectToAction("Index", "HostelTransfer");
        }








    }
}