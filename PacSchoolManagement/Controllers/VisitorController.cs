﻿using AutoMapper;
using Business;
using Entities.Models;
using PacSchoolManagement.Models;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PacSchoolManagement.Controllers
{
    public class VisitorController : Controller
    {

          private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public UserLoginBusiness _userBusiness;
        public VisitorBusiness _visitorBusiness;
        public VisitorController()
        {
            this._unitOfWork = new UnitOfWork(_df);
        
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._visitorBusiness = new VisitorBusiness(_df, this._unitOfWork);

        }
        //
        // GET: /Visitor/
        public ActionResult Index()
        {
            VisitorViewModel viewmodel = new VisitorViewModel();

            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var SchoolId = _userBusiness.Find(currentUserId).SchoolID;
            var Session = Filters.AuthenticationModel.GlobalUser.getGlobalUser().Session;
            List<SelectListItem> ListUtyp = new List<SelectListItem>();
            ListUtyp.Add(new SelectListItem { Text = "Parents", Value = "Parents" });
            ListUtyp.Add(new SelectListItem { Text = "Students", Value = "Students" });
            ListUtyp.Add(new SelectListItem { Text = "Staff", Value = "Staff" });
            ListUtyp.Add(new SelectListItem { Text = "Others", Value = "Others" });
            viewmodel.CategoryList = ListUtyp;
            int count = _visitorBusiness.GetListWT(x => x.SchoolId == SchoolId ).Count();
            int sum = count + 1;
            viewmodel.TokenNo = sum.ToString();
            return View(viewmodel);
        }
       
        public JsonResult SaveDataInDatabase(VisitorViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.Id > 0)
            {
                var userList = _visitorBusiness.GetListWT(x => x.Id == clsmodel.Id).ToList().FirstOrDefault();

                userList.Id = clsmodel.Id;
                userList.Name = clsmodel.Name;

                bool isSuccess = _visitorBusiness.AddUpdateDeleteVisitor(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                var SchoolId = _userBusiness.Find(currentUserId).SchoolID;
                clsmodel.SchoolId = SchoolId;


                Mapper.CreateMap<VisitorViewModel, Visitor>();
                Visitor cls = Mapper.Map<VisitorViewModel, Visitor>(clsmodel);
               
                bool isSuccess = _visitorBusiness.AddUpdateDeleteVisitor(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetStudentList()  //Gets the todo Lists.
        {  var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            var SchoolId = _userBusiness.Find(currentUserId).SchoolID;
            var Session = Filters.AuthenticationModel.GlobalUser.getGlobalUser().Session;


            var courseList = _visitorBusiness.GetListWT(x => x.SchoolId == SchoolId && x.SessionId == Convert.ToInt32(Session));
            List<VisitorViewModel> courseViewModelList = new List<VisitorViewModel>();

            var records = (from p in courseList
                           select new VisitorViewModel
                           {
                               MobileNo = p.MobileNo,
                               Name = p.Name,
                               Purpose = p.Purpose,
                               Remarks = p.Remarks,

                               // ReadConfigData.GetAppSettingsValue("CMSUrl") + "/SliderImage/" + c.SliderImage
                               Category = p.Category,
                               TokenNo = p.TokenNo,
                               SchoolId = p.SchoolId,
                               Id = p.Id
                            
                             
                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }
	}
}