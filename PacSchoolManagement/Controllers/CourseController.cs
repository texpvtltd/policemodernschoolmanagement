﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class CourseController : Controller
    {
        //
        // GET: /Course/

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
       private CourseBusiness _courseBusiness;

       public CourseController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._courseBusiness = new CourseBusiness(_df, this._unitOfWork);


        }
       [HttpGet]
       public ActionResult Course()
       {

           CourseViewModel viewmodel = new CourseViewModel();
           List<SelectListItem> ListUtyp = new List<SelectListItem>();
           ListUtyp.Add(new SelectListItem { Text = "Daily", Value = "Daily" });
           ListUtyp.Add(new SelectListItem { Text = "Subject Wise", Value = "Subject Wise" });
           // ListUtyp.Add(new SelectListItem { Text = "Factory", Value = "Factory" });
           viewmodel.AttendancetypeList = ListUtyp;
           List<SelectListItem> ListSylla = new List<SelectListItem>();
           ListSylla.Add(new SelectListItem { Text = "GPA", Value = "GPA" });
           ListSylla.Add(new SelectListItem { Text = "CCE", Value = "CCE" });
           // ListUtyp.Add(new SelectListItem { Text = "Factory", Value = "Factory" });
           viewmodel.SyllabusNameList = ListSylla;

           return View(viewmodel);
         

       }
       [HttpPost]
       public ActionResult Course(string s)
       {

           return View();

       }
       // POST: /Class/Create
       public JsonResult SaveDataInDatabase(CourseViewModel clsmodel)
       {
           //HttpPostedFileBase fu = Request.Files["file"];
           //HttpPostedFileBase file1 = clsmodel.Logo;

          // file = clsmodel.ImageUpload;
           int schoolid = Convert.ToInt32(clsmodel.SchoolId);
           if (schoolid > 0)
           {
               var CourseList = _courseBusiness.GetListWT().ToList().FirstOrDefault();

               CourseList.SchoolId = clsmodel.SchoolId;
               CourseList.CourseName = clsmodel.CourseName;
               CourseList.Attendancetype = clsmodel.Attendancetype;
               CourseList.SessionId = clsmodel.SessionId;
               CourseList.Description = clsmodel.Description;
               CourseList.Code = clsmodel.Code;
               CourseList.MinAttendance = clsmodel.MinAttendance;
               CourseList.Totalworkingdays = clsmodel.Totalworkingdays;
               CourseList.IsDelete = "false";

               bool isSuccess = _courseBusiness.AddUpdateDeleteCourse(CourseList, "U");
               if (isSuccess)
               {
                   TempData["Success"] = "Updated Successfully!!";
                   TempData["isSuccess"] = "true";
                   // return RedirectToAction("Index");
               }
               else
               {
                   TempData["Success"] = "Failed to create Brand!!";
                   TempData["isSuccess"] = "false";
               }
           }



           else
           {

               var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
               var schid = _userBusiness.Find(currentUserId).SchoolID;

               Mapper.CreateMap<CourseViewModel, Course>();
               Course cls = Mapper.Map<CourseViewModel, Course>(clsmodel);
               cls.SchoolId = schid.ToString();
               cls.IsDelete = "false";
               cls.SessionId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().Session;
             //  cls.SessionID = Convert.ToInt32(currentUserId);
               // cls.TokenKey = GlobalMethods.GetToken();
           
               bool isSuccess = _courseBusiness.AddUpdateDeleteCourse(cls, "I");
               if (isSuccess)
               {
                   TempData["Success"] = "School Created Successfully!!";
                   TempData["isSuccess"] = "true";
                   // return RedirectToAction("Index");
               }
               else
               {
                   TempData["Success"] = "Failed to create School!!";
                   TempData["isSuccess"] = "false";
               }
               // result = true;

               //  }
           }
           return Json(JsonRequestBehavior.AllowGet);
       }
       public JsonResult GetStudentList()  //Gets the todo Lists.
       {


           var courseList = _courseBusiness.GetListWT(x => x.IsDelete == "false");
           List<CourseViewModel> courseViewModelList = new List<CourseViewModel>();

           var records = (from p in courseList
                          select new CourseViewModel
                          {
                              Attendancetype = p.Attendancetype,
                              Code = p.Code,
                              CourseName = p.CourseName,
                              CourseId = p.CourseId,
                             
                              // ReadConfigData.GetAppSettingsValue("CMSUrl") + "/SliderImage/" + c.SliderImage
                              Description = p.Description,
                              SyllabusName = p.SyllabusName,
                              SchoolId = p.SchoolId,
                              MinAttendance = p.MinAttendance,
                              SessionId = p.SessionId,
                              Totalworkingdays = p.Totalworkingdays

                          }).AsQueryable();


           return Json(records, JsonRequestBehavior.AllowGet);
       }

       public JsonResult DeleteStudentRecord(int StudentId)
       {
           var userList = _courseBusiness.GetListWT(x => x.CourseId == StudentId).ToList().FirstOrDefault();

           userList.IsDelete = "fales";

           bool isSuccess = _courseBusiness.AddUpdateDeleteCourse(userList, "U");
           if (isSuccess)
           {
               TempData["Success"] = "Updated Successfully!!";
               TempData["isSuccess"] = "true";
               // return RedirectToAction("Index");
           }
           else
           {
               TempData["Success"] = "Failed to create Brand!!";
               TempData["isSuccess"] = "false";
           }
           return Json(userList, JsonRequestBehavior.AllowGet);
       }

       public JsonResult GetStudentById(int StudentId)
       {
           var model = _courseBusiness.GetListWT(x => x.CourseId == StudentId).ToList().FirstOrDefault();
           string value = string.Empty;
           value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
           {
               ReferenceLoopHandling = ReferenceLoopHandling.Ignore
           });
           return Json(value, JsonRequestBehavior.AllowGet);
       }
       public JsonResult GetDetail(int StudentId)
       {
           var model = _courseBusiness.GetListWT(x => x.CourseId == StudentId).ToList().FirstOrDefault();
           string value = string.Empty;
           value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
           {
               ReferenceLoopHandling = ReferenceLoopHandling.Ignore
           });
           return Json(value, JsonRequestBehavior.AllowGet);
       }

    


	}
}