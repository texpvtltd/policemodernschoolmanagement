﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class AddTransportController : Controller
    {
        // GET: AddTransport


            


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public SchoolBusiness _schoolBusiness;
        public PrincipalBusiness _princiBusiness;
        public SessionBusiness _sessionBusiness;
        private UserLoginBusiness _userBusiness;
        private AddTransportBusiness _addtranport;
        public AddTransportController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._schoolBusiness = new SchoolBusiness(_df, this._unitOfWork);
            this._princiBusiness = new PrincipalBusiness(_df, this._unitOfWork);
            this._sessionBusiness = new SessionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._addtranport = new AddTransportBusiness(_df, this._unitOfWork);
        }





        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TransePortList()
        {

            TransportViewModel transportviewmodel = new TransportViewModel();

            List<SelectListItem> vihaicletype = new List<SelectListItem>();
            vihaicletype.Add(new SelectListItem { Text = "Contract", Value = " Contract" });
            vihaicletype.Add(new SelectListItem { Text = "Wonership", Value = "Wonership" });


            transportviewmodel.VehicleTypeList = vihaicletype;

            return View(transportviewmodel);
        }

        public JsonResult SaveDataInDatabase(TransportViewModel clsmodel, HttpPostedFileBase file)
        {
            // var result = false;
            //if (clsmodel.ClassID > 0)
            //{  

            if (clsmodel.VehicleID > 0)
            {
                var userList = _addtranport.GetListWT(x => x.VehicleID == clsmodel.VehicleID).ToList().FirstOrDefault();
                userList.ContactPerson = clsmodel.ContactPerson;
                userList.SchoolID = clsmodel.SchoolID;
                userList.InsuranceRenewalDate = clsmodel.InsuranceRenewalDate;
               
                userList.MaximumAllowed = clsmodel.MaximumAllowed;
                userList.NoofSeats = clsmodel.NoofSeats;
                userList.VehicleNo = clsmodel.VehicleNo;
                userList.TrackID = clsmodel.TrackID;
                userList.VehicleType = clsmodel.VehicleType;
               
                bool isSuccess = _addtranport.AddUpdateDeleteClass(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create updated!!";
                    TempData["isSuccess"] = "false";
                }
            }


            else
            {

            
                var currentUserId = GlobalUser.getGlobalUser().UserId;

                Mapper.CreateMap<TransportViewModel, AddTransport>();
                AddTransport cls = Mapper.Map<TransportViewModel, AddTransport>(clsmodel);
                cls.IsDelete = "false";
                cls.SchoolID = currentUserId.ToString();

               
                bool isSuccess = _addtranport.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Principal Added Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to add principal!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;
            }
            //  }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrinciList()  //Gets the todo Lists.
        {


            var userList = _addtranport.GetListWT(x => x.IsDelete == "false"); ;
            List<TransportViewModel> userViewModelList = new List<TransportViewModel>();

            var records = (from p in userList
                           select new TransportViewModel
                           {
                               VehicleID = p.VehicleID,
                               NoofSeats = p.NoofSeats,
                               SchoolID = p.SchoolID,
                               RenewalDate = p.RenewalDate,
                               ContactPerson = p.ContactPerson,
                               InsuranceRenewalDate = p.InsuranceRenewalDate,
                             MaximumAllowed=p.MaximumAllowed,
                             TrackID=p.TrackID,
                             VehicleNo=p.VehicleNo,
                             VehicleType=p.VehicleType,

                           }).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _addtranport.GetListWT(x => x.VehicleID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _addtranport.GetListWT(x => x.VehicleID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _addtranport.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }















    }
}