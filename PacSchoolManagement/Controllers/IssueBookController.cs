﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;

namespace PacSchoolManagement.Controllers
{
    public class IssueBookController : Controller
    {
        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        private BookBusiness _bookbusiness;
        private BookCategoryBusiness _bookcategorybusiness;
        private IssueBookBusiness _issuebookbusiness;
        // GET: IssueBook

        public IssueBookController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._bookbusiness = new BookBusiness(_df, this._unitOfWork);
            this._bookcategorybusiness = new BookCategoryBusiness(_df, this._unitOfWork);
            this._issuebookbusiness = new IssueBookBusiness(_df, this._unitOfWork);
        }

        public ActionResult Index()
        {
            var record = _issuebookbusiness.GetListWT(x=>x.IsDelete=="false");


            return View(record);
        }

        [HttpGet]
        public ActionResult IssuedBookList()
        {

            BookIssuedViewModel bookissuedviewmodel = new BookIssuedViewModel();
            List<SelectListItem> userytype= new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Teacher", Value = "Teacher" });
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });
          
            bookissuedviewmodel.UserTypeList = userytype;
            bookissuedviewmodel.BookDetailList = new List<BookViewModel>();


            return View(bookissuedviewmodel);

        }
        [HttpPost]
        public ActionResult IssuedBookList( BookIssuedViewModel clsmodel)
        {

            // var result = false;
            if (clsmodel.BookIssuedID> 0)
            {
                var userList = _issuebookbusiness.GetListWT(x => x.BookIssuedID== clsmodel.BookIssuedID).ToList().FirstOrDefault();

                userList.BookIssuedID = clsmodel.BookIssuedID;
                userList.BookNo = clsmodel.BookNo;
                userList.BooKTitle=clsmodel.BooKTitle;

                    userList.DueDate=clsmodel.DueDate;

                    userList.IssuedDate=clsmodel.IssuedDate;

                        userList.UserCourse=clsmodel.UserCourse;

                        userList.UserID=clsmodel.UserID;

                            userList.UserSection=clsmodel.UserSection;
                            userList.UserType=clsmodel.UserType;

                                
                bool isSuccess = _issuebookbusiness.AddUpdateDeleteBook(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Update!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {


                var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                //  var sch = Filters.AuthenticationModel.GlobalUser.GlobalData.SchoolName;


                Mapper.CreateMap<BookIssuedViewModel, IssueBook>();
                IssueBook cls = Mapper.Map<BookIssuedViewModel, IssueBook>(clsmodel);
                cls.SchoolID = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
                cls.IsDelete = "false";
                bool isSuccess = _issuebookbusiness.AddUpdateDeleteBook(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "flase";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }



            return RedirectToAction("Index", "IssueBook");
            
        }

        [HttpGet]
        public JsonResult Searchbook(string txtSearch)
        {
            BookIssuedViewModel bookissueviewmodel=new BookIssuedViewModel();
           
            var bookList = _bookbusiness.GetListWT(c => c.BookNo.Contains(txtSearch) || c.Title.Contains(txtSearch) ||c.BookCategory.Contains(c.BookCategory)).ToList();
            bookissueviewmodel.BookDetailList =  (from c in bookList
                                                 select new BookViewModel
                                                     {
                                                         BookISBNNo = c.BookISBNNo,
                                                         BookID = c.BookID,
                                                         BookCost = c.BookCost,
                                                         BookNo = c.BookNo,
                                                         BookCondition = c.BookCondition,
                                                         BookPosition = c.BookPosition,
                                                         Edition = c.Edition,
                                                         Language = c.Language,
                                                         NoofCopies = c.NoofCopies,
                                                         Publisher = c.Publisher,
                                                         PurchaseDate = c.PurchaseDate,
                                                         SchoolId = c.SchoolId,
                                                         ShelfNo = c.ShelfNo,
                                                         Title = c.Title,

                                                     }).ToList();




            return Json(bookissueviewmodel, JsonRequestBehavior.AllowGet);
           // return View(bookissueviewmodel);

          
        }


        public ActionResult Edit(int txtval)
        {

              var userList = _issuebookbusiness.GetListWT(x=>x.BookIssuedID==txtval && x.IsDelete=="false").FirstOrDefault();
            List<BookIssuedViewModel> userViewModelList = new List<BookIssuedViewModel>();
            BookIssuedViewModel bimodel=new BookIssuedViewModel();
           bimodel.IssuedDate=userList.IssuedDate;
            bimodel.SchoolID=userList.SchoolID;
            bimodel.IsDelete=userList.IsDelete;
            bimodel.UserCourse=userList.UserCourse;
            bimodel.UserID=userList.UserID;
            bimodel.UserName=userList.UserName;
            bimodel.UserSection=userList.UserSection;
            bimodel.UserType=userList.UserType;
            bimodel.BookName=userList.BookName;
            bimodel.BookIssuedID=userList.BookIssuedID;
            bimodel.BookNo=userList.BookNo;
            bimodel.BooKTitle = userList.BooKTitle;
            bimodel.DueDate = userList.DueDate;
            List<SelectListItem> userytype = new List<SelectListItem>();
            userytype.Add(new SelectListItem { Text = "Teacher", Value = "Teacher" });
            userytype.Add(new SelectListItem { Text = "Student", Value = "Student" });

            bimodel.UserTypeList = userytype;





            return View(bimodel);
        }
        [HttpPost]
        public ActionResult Edit( BookIssuedViewModel clsmodel)
        {





              var userList = _issuebookbusiness.GetListWT(x => x.BookIssuedID== clsmodel.BookIssuedID).ToList().FirstOrDefault();

                userList.BookIssuedID = clsmodel.BookIssuedID;
                userList.BookNo = clsmodel.BookNo;
                userList.BooKTitle=clsmodel.BooKTitle;

                    userList.DueDate=clsmodel.DueDate;

                    userList.IssuedDate=clsmodel.IssuedDate;

                        userList.UserCourse=clsmodel.UserCourse;

                        userList.UserID=clsmodel.UserID;

                            userList.UserSection=clsmodel.UserSection;
                            userList.UserType=clsmodel.UserType;
                            userList.UserName = clsmodel.UserName;
                                
                bool isSuccess = _issuebookbusiness.AddUpdateDeleteBook(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Update!!";
                    TempData["isSuccess"] = "false";
                }
            
              //var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
              //  //  var sch = Filters.AuthenticationModel.GlobalUser.GlobalData.SchoolName;


              //  Mapper.CreateMap<BookIssuedViewModel, IssueBook>();
              //  IssueBook cls = Mapper.Map<BookIssuedViewModel, IssueBook>(clsmodel);
              //  cls.SchoolID = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID).ToString();
              //  cls.IsDelete = "false";
              //  bool isSuccess = _issuebookbusiness.AddUpdateDeleteBook(cls, "U");
              //  if (isSuccess)
              //  {
              //      TempData["Success"] = "Update Created Successfully!!";
              //      TempData["isSuccess"] = "flase";
              //      // return RedirectToAction("Index");
              //  }
              //  else
              //  {
              //      TempData["Success"] = "Failed to create Brand!!";
              //      TempData["isSuccess"] = "false";
              //  }
              //  // result = true;

            

            return RedirectToAction("Index", "IssueBook");
    }





        public ActionResult Delete(int txtval)
        {
            var userList = _issuebookbusiness.GetListWT(x => x.BookIssuedID == txtval).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _issuebookbusiness.AddUpdateDeleteBook(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return RedirectToAction("Index", "IssueBook");
        }


    }
}