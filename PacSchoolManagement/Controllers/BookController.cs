﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class BookController : Controller
    {
        // GET: Book


        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
        public TeacherBusiness _teacherBusiness;
        private UserLoginBusiness _userBusiness;
        private BookBusiness _bookbusiness;
        private BookCategoryBusiness _bookcategorybusiness;
           public BookController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);

            this._bookbusiness = new BookBusiness(_df, this._unitOfWork);
            this._bookcategorybusiness = new BookCategoryBusiness(_df, this._unitOfWork);
        }

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult AddBook(string s)
        {
            BookViewModel bookviewmodel = new BookViewModel();
            List<SelectListItem> BookCondition = new List<SelectListItem>();
            BookCondition.Add(new SelectListItem { Text = "Good", Value = "Good" });
            BookCondition.Add(new SelectListItem { Text = "Bad", Value = "Bad" });
            BookCondition.Add(new SelectListItem { Text = "New", Value = "New" });


            
          bookviewmodel.BookConditionList = BookCondition;
          var bookcategory = _bookcategorybusiness.GetListWT();
          bookviewmodel.BookCategoryList = bookcategory.Select(x => new SelectListItem
      {
          Text = x.Category.ToString(),
          Value = x.BookCategoryID.ToString()
      }).ToList();

            


            return View(bookviewmodel);

           
        }
        [HttpPost]
        public ActionResult AddBook()
        {

            return View();

        }
        public JsonResult SaveDataInDatabase(BookViewModel clsmodel)
        {
            // var result = false;
            if (clsmodel.BookID > 0)
            {
                var userList = _bookbusiness.GetListWT(x => x.BookID == clsmodel.BookID).ToList().FirstOrDefault();

                userList.BookCost = clsmodel.BookCost;
                userList.Author = clsmodel.Author;
                userList.BillNo = clsmodel.BillNo;
                userList.BookCategory = clsmodel.BookCategory;
                userList.BookCondition = userList.BookCondition;
                userList .BookISBNNo = clsmodel.BookISBNNo;
                userList.BookPosition= clsmodel.BookPosition;
                userList.Edition = clsmodel.Edition;
                userList.IsDelete ="false";
                userList.Language = clsmodel.Language;
                  userList.NoofCopies = clsmodel.NoofCopies;
            userList.Publisher = clsmodel.Publisher;
                   userList.PurchaseDate=clsmodel.Publisher;
                userList.ShelfNo=clsmodel.ShelfNo;
                userList.Title=clsmodel.Title;
                

                bool isSuccess = _bookbusiness.AddUpdateDeleteBook(userList, "U");
                if (isSuccess)
                {
                    TempData["Success"] = "Updated Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
            }
            else
            {

                var currentUserId = GlobalUser.getGlobalUser().SchoolID;

                Mapper.CreateMap<BookViewModel, Book>();
               Book cls = Mapper.Map<BookViewModel, Book>(clsmodel);
                cls.SchoolId = Convert.ToInt32(currentUserId).ToString();

               
               
                    cls.IsDelete = "false";
                   
                

              
                bool isSuccess = _bookbusiness.AddUpdateDeleteBook(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = " Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Book!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            int SchoolId = Convert.ToInt32(GlobalUser.getGlobalUser().SchoolID);

            var userList = _bookbusiness.GetListWT(x => x.IsDelete == "false");
            List<BookViewModel> userViewModelList = new List<BookViewModel>();

            var records = (from p in userList
                           select new BookViewModel
                           {
                                SchoolId= p.SchoolId,
                             BookCondition=p.BookCondition,
                                BookCategory = p.BookCategory,
                               BookName= _bookcategorybusiness.GetUserById(Convert.ToInt32(p.BookCategory)).Category,
                             BookCost=p.BookCost,
                             BookPosition=p.BookPosition,
                             BookISBNNo=p.BookISBNNo,
                             BookNo=p.BookNo,
                             //BookName=p.BookName
                             BookID=p.BookID,
                            Edition=p.Edition,
                            NoofCopies=p.NoofCopies,
                            Language=p.Language,
                            Publisher=p.Publisher,
                            PurchaseDate=p.PurchaseDate,
                           // SchoolId=p.SchoolId,
                            ShelfNo=p.ShelfNo,
                            Title=p.Title,
                            Author=p.Author,
                           }).Where(i => i.SchoolId == SchoolId.ToString()).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _bookbusiness.GetListWT(x => x.BookID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "true";

            bool isSuccess = _bookbusiness.AddUpdateDeleteBook(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentById(int StudentId)
        {
            var model = _bookbusiness.GetListWT(x => x.BookID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }






















    }
}