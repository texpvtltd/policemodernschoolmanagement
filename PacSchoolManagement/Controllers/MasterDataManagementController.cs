﻿using AutoMapper;
using Business;
using Commom.GlobalMethods;
using Common.Cryptography;
using Common.GlobalData;
using Entities.Models;
using Newtonsoft.Json;
using Repository.RepositoryFactoryCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using PacSchoolManagement.Helper;
using PacSchoolManagement.Models;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Drawing.Imaging;
using Filters.AuthenticationModel;
namespace PacSchoolManagement.Controllers
{
    public class MasterDataManagementController : Controller
    {
        //

        private DatabaseFactory _df = new DatabaseFactory();
        private UnitOfWork _unitOfWork;
         public ClassBusiness _classBusiness;
         public SectionBusiness _sectionBusiness;
         public UserLoginBusiness _userBusiness;
         public TeacherBusiness _teacherBusiness;
         private DepartmentBusiness _deptBusiness;

       public MasterDataManagementController()
        {
            this._unitOfWork = new UnitOfWork(_df);
            this._classBusiness = new ClassBusiness(_df, this._unitOfWork);
            this._sectionBusiness = new SectionBusiness(_df, this._unitOfWork);
            this._userBusiness = new UserLoginBusiness(_df, this._unitOfWork);
            this._teacherBusiness = new TeacherBusiness(_df, this._unitOfWork);
            this._deptBusiness = new DepartmentBusiness(_df, this._unitOfWork);
        }
  
        // GET: /MasterDataManagement/
        public ActionResult Index()
        {



            return View();
        }

     
        [HttpGet]
        public ActionResult Class()
        {
            ClassViewModel classViewModel = new ClassViewModel();
        var classList1 = _sectionBusiness.GetListWT();
        classViewModel.sectionList = classList1.Select(x => new SelectListItem
            {
                Text = x.SectionName.ToString(),
                Value = x.SectionID.ToString()
           }).ToList();
        var teacher = _teacherBusiness.GetListWT();
        classViewModel.teacherList = teacher.Select(x => new SelectListItem
        {
            Text = x.FName+""+x.LName.ToString(),
            Value = x.SchoolUserID.ToString()
        }).ToList();

        var department = _deptBusiness.GetListWT();
        classViewModel.departmentList = department.Select(x => new SelectListItem
               {
                   Text = x.DepartmentName.ToString(),
                   Value = x.DeptId.ToString()
               }).ToList();


        return View(classViewModel);

        }
        [HttpPost]
        public ActionResult Class( string s)
        {

            return View();

        }

        //
        // POST: /Class/Create
        public JsonResult SaveDataInDatabase(ClassViewModel clsmodel)
        {
            // var result = false;
           if (clsmodel.ClassID > 0)
            {
             var userList = _classBusiness.GetListWT(x => x.ClassID ==clsmodel .ClassID).ToList().FirstOrDefault();
               
               userList.ClassID =clsmodel.ClassID;
              userList.ClassName = clsmodel.ClassName;
               //userList.ClassID=clsmodel.ClassID
              userList.ClassTeacherID = clsmodel.SchoolUserID;
              userList.SectionID = clsmodel.SectionID;
              userList.DepartmentId = clsmodel.DeptId.ToString();
              userList.DepartmentId = clsmodel.DeptId.ToString();
            //   userList.

              bool isSuccess = _classBusiness.AddUpdateDeleteClass(userList, "U");
              if (isSuccess)
              {
                  TempData["Success"] = "Updated Successfully!!";
                  TempData["isSuccess"] = "true";
                  // return RedirectToAction("Index");
              }
              else
              {
                  TempData["Success"] = "Failed to create Brand!!";
                  TempData["isSuccess"] = "false";
              }
           }
                else
                {

                    var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
                    //int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);
                    var schoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);

                Mapper.CreateMap<ClassViewModel, Class>();
                Class cls = Mapper.Map<ClassViewModel, Class>(clsmodel);
                cls.SchoolID = Convert.ToInt32(schoolId);
                cls.ClassTeacherID = clsmodel.SchoolUserID;
                cls.IsDelete = "false";
                cls.DepartmentId = clsmodel.DeptId.ToString();
                // cls.TokenKey = GlobalMethods.GetToken();
                bool isSuccess = _classBusiness.AddUpdateDeleteClass(cls, "I");
                if (isSuccess)
                {
                    TempData["Success"] = "Update Created Successfully!!";
                    TempData["isSuccess"] = "true";
                    // return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "Failed to create Brand!!";
                    TempData["isSuccess"] = "false";
                }
                // result = true;

           }

            return Json(clsmodel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStudentList()  //Gets the todo Lists.
        {
            var currentUserId = Filters.AuthenticationModel.GlobalUser.getGlobalUser().UserId;
            int SchoolId = Convert.ToInt32(_userBusiness.Find(currentUserId).SchoolID);

            var userList = _classBusiness.GetListWT();
            List<ClassViewModel> userViewModelList = new List<ClassViewModel>();

            var records = (from p in userList
                           select new ClassViewModel
                           {
                               ClassID=p.ClassID,
                               ClassName= p.ClassName,
                                SchoolID = p.SchoolID,
                               ClassTeacherID = p.ClassTeacherID,
                               SectionID=p.SectionID,
                          //  DeptName=_deptBusiness.GetUserById(Convert.ToInt32(p.DepartmentId)).DepartmentName,
                                                        
                           }).Where(i => i.SchoolID == SchoolId).AsQueryable();


            return Json(records, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteStudentRecord(int StudentId)
        {
            var userList = _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();

            userList.IsDelete = "fales";

            bool isSuccess = _classBusiness.AddUpdateDeleteClass(userList, "U");
            if (isSuccess)
            {
                TempData["Success"] = "Delete Successfully!!";
                TempData["isSuccess"] = "true";
                // return RedirectToAction("Index");
            }
            else
            {
                TempData["Success"] = "Not Delete Successfully!!";
                TempData["isSuccess"] = "true";
            }
            return Json(userList, JsonRequestBehavior.AllowGet);
        }


       

        public JsonResult GetStudentById(int StudentId)
        {
            var  model =   _classBusiness.GetListWT(x => x.ClassID == StudentId).ToList().FirstOrDefault();
            string value = string.Empty;
            value = JsonConvert.SerializeObject(model, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });
            return Json(value, JsonRequestBehavior.AllowGet);
        }

}}