﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PacSchoolManagement.Startup))]
namespace PacSchoolManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
