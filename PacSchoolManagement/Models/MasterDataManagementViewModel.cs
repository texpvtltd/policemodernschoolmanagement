﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class MasterDataManagementViewModel
    {
        
    }
    public class ClassViewModel
        {
            public int ClassID { get; set; }
            public int SchoolUserID { get; set; }
            public int DeptId { get; set; }
            public string DeptName { get; set; }
            public string ClassName { get; set; }
            public int SectionID { get; set; }
            public int SchoolID { get; set; }
            public Nullable<int> ClassTeacherID { get; set; }
            public List<System.Web.Mvc.SelectListItem> classList { get; set; }
            public List<System.Web.Mvc.SelectListItem> teacherList { get; set; }
            public List<System.Web.Mvc.SelectListItem> departmentList{ get; set; }
            public List<System.Web.Mvc.SelectListItem> sectionList { get; set; }
          //  public List<System.Web.Mvc.SelectListItem> classList { get; set; }
        }
    public class SchoolUserViewModel
    {
        public int SchoolUserID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string UserImage { get; set; }
        public string AdhaarImage { get; set; }
        public string UserType { get; set; }
        public string Passward { get; set; }
        public string UserName { get; set; }
        public int SchoolID { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }
        public HttpPostedFileBase AdharUpload { get; set; }


        public List<System.Web.Mvc.SelectListItem> schooluserList { get; set; }
    }
    public class SubjectViewModel
    {
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public string TeacherName { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> SectionID { get; set; }
        public string Section { get; set; }
        public int SchoolID { get; set; }
        public List<System.Web.Mvc.SelectListItem> subjectList { get; set; }
        public List<System.Web.Mvc.SelectListItem> ClassList { get; set; }
        public List<System.Web.Mvc.SelectListItem> StudentAdmissionList { get; set; }
        // public List<System.Web.Mvc.SelectListItem> SessioList { get; set; }
        public List<System.Web.Mvc.SelectListItem> SessionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> SectionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> TeacherList { get; set; }
       
    }


    public  class SectionViewModel
    {
        public int SectionID { get; set; }
        public string SectionName { get; set; }
        public int SchoolID { get; set; }
    }


    public class HouseViewModel
    {
        public int HouseID { get; set; }
        public string HouseName { get; set; }
        public string HouseColor { get; set; }
        public int SchoolID { get; set; }
        public string IsDelete { get; set; }
    }





   
}