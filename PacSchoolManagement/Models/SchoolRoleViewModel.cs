﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class SchoolRoleViewModel
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> Schoolid { get; set; }
        public string IsDelete { get; set; }
    }
    public class AssignTeacherViewModel
    {
        public int AssignTeacherId { get; set; }
        public Nullable<int> TeacherId { get; set; }
        public List<System.Web.Mvc.SelectListItem> TeacherList { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public List<System.Web.Mvc.SelectListItem> SchoolList { get; set; }
        public Nullable<int> CourseId { get; set; }
        public List<System.Web.Mvc.SelectListItem> CourseList { get; set; }
        public Nullable<int> BatchId { get; set; }
        public List<System.Web.Mvc.SelectListItem> BatchList { get; set; }
        public Nullable<int> ClassId { get; set; }
        public List<System.Web.Mvc.SelectListItem> ClassList { get; set; }
        public string IsDelete { get; set; }
        public string IsClassTeacher { get; set; }
        public Nullable<int> SubjectId { get; set; }
        public List<System.Web.Mvc.SelectListItem> SubjectList { get; set; }
    }
}