﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class TransportViewModel
    {

        public int VehicleID { get; set; }
        public string VehicleNo { get; set; }
        public string NoofSeats { get; set; }
        public string MaximumAllowed { get; set; }
        public string VehicleType { get; set; }
        public string ContactPerson { get; set; }
        public string InsuranceRenewalDate { get; set; }
        public string RenewalDate { get; set; }
        public string TrackID { get; set; }
        public string SchoolID { get; set; }
        public string IsDelete { get; set; }
        public List<System.Web.Mvc.SelectListItem> VehicleTypeList { get; set; }

    }


    public class DriverViewModel
    {

        public int DriverId { get; set; }
        public string VehicleNo { get; set; }
        public string DriverName { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string DateofBirth { get; set; }
        public string ContactNo { get; set; }
        public string LicenseNumber { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
        public List<System.Web.Mvc.SelectListItem> RouteNoList { get; set; }



    } 
    
    
    public class TransportAllocationViewModel
    {
    public int AllocationId { get; set; }
        public string Route { get; set; }
        public string Destination { get; set; }
        public string Type { get; set; }
        public string StartFrequency { get; set; }
        public string EndFrequency { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
      public List<System.Web.Mvc.SelectListItem> RouteCodeList { get; set; }
      public List<System.Web.Mvc.SelectListItem> UserTypeList{ get; set; }
      public List<System.Web.Mvc.SelectListItem> VehaicleList { get; set; }
    
    }

    public class DestinationandFeesViewModel
    {
        public int DestinationId { get; set; }
        public string RouteCode { get; set; }
        public string PickupDrop { get; set; }
        public string StopTime { get; set; }
        public string Amount { get; set; }
        public string FeeType { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
       public List<System.Web.Mvc.SelectListItem> FeesTypeList { get; set; }
       public List<System.Web.Mvc.SelectListItem> RauteCodeList { get; set; }

    }
    public class AddRatueViewModel
    {

        public int RouteID { get; set; }
        public string VehicleNo { get; set; }
        public string RouteCode { get; set; }
        public string RouteStartPlace { get; set; }
        public string RouteStopPlace { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
       public List<System.Web.Mvc.SelectListItem> VehicleList { get; set; }
    
    }


    public class TransPortReportViewModel
    {
        public List<System.Web.Mvc.SelectListItem> ClassList{ get; set; }
        public List<System.Web.Mvc.SelectListItem>SectionList { get; set; }

        public string ClassId{ get; set; }

        public string SectionId { get; set; }


    }

    public class FeesCollectionViewModel
    {

        public int TransPortFeeCollId { get; set; }
        public string FeeType { get; set; }
        public string ActualAmount { get; set; }
        public string AmountTobePaid { get; set; }
        public string Fine { get; set; }
        public string Discount { get; set; }
        public string ModeOfPay { get; set; }
        public string Remark { get; set; }
        public string TotalAmount { get; set; }
        public string ReciptNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }


    }




    public class TransPortFeesCollectionViewModel
    {

        public int TransPortFeeCollId { get; set; }
        public string FeeType { get; set; }
        public string ActualAmount { get; set; }
        public string AmountTobePaid { get; set; }
        public string Fine { get; set; }
        public string Discount { get; set; }
        public string ModeOfPay { get; set; }
        public string Remark { get; set; }
        public string TotalAmount { get; set; }
        public string ReciptNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }





        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }
    
        public List<System.Web.Mvc.SelectListItem> EmployeeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> departmentList { get; set; }
        public string DeptId { get; set; }
        public string User { get; set; }

        public string usertype { get; set; }
        public List<System.Web.Mvc.SelectListItem> ModeList { get; set; }
      
      

    }
   


}