﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class SchoolViewModel
    {
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string ContactNo { get; set; }
        public string Logo { get; set; }
        public string SchoolCode { get; set; }
        public Nullable<int> SessionID { get; set; }
        public string City { get; set; }
       
        public HttpPostedFileBase ImageUpload { get; set; }
        public string IsDelete { get; set; }
        public List<System.Web.Mvc.SelectListItem> schoolList { get; set; }

    }


    public partial class SchoolDetailViewModel
    {
        public int SchoolDetID { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Contactperson { get; set; }
        public string City { get; set; }
        public string InstitutionCode { get; set; }
        public string Openedon { get; set; }
        public string Landtype { get; set; }
        public string Landdoc { get; set; }


    }
}