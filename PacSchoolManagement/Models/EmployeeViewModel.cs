﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class EmployeeViewModel
    {
        public int EmpId { get; set; }
        public string EmployeeSerialNo { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public Nullable<int> RoleId { get; set; }
        public Nullable<int> DeptId { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string Joiningdate { get; set; }
        public string Address { get; set; }
        public string Salary { get; set; }
        public string Photo { get; set; }
        public string AdhaarCardNo { get; set; }
        public string PanCardNo { get; set; }
        public string DateofBirth { get; set; }
        public string HRA { get; set; }
        public string DA { get; set; }
        public string TA { get; set; }
        public string PaidLeaves { get; set; }
        public string Leavestaken { get; set; }
        public string Department { get; set; }
        public string Role { get; set; }
        public string School { get; set; }
        public string Employeecategory { get; set; }
        public List<System.Web.Mvc.SelectListItem> catList { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeType { get; set; }
        public List<System.Web.Mvc.SelectListItem> deptList { get; set; }
        public List<System.Web.Mvc.SelectListItem> roleList { get; set; }
        public List<System.Web.Mvc.SelectListItem> typeList { get; set; }
       
        public List<System.Web.Mvc.SelectListItem> schoollList { get; set; }
    }
}