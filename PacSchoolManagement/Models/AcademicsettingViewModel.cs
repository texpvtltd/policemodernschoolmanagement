﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class AcademicsettingViewModel
    {
     
            public int Id { get; set; }
            public Nullable<int> SchoolId { get; set; }
            public Nullable<int> SessionId { get; set; }
            public string Startdate { get; set; }
            public string Sessionenddate { get; set; }
        
    }
}