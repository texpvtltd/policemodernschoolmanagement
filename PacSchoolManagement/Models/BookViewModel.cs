﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class BookViewModel
    {
        public int BookID { get; set; }
        public string BookCategoryID { get; set; }
        public List<System.Web.Mvc.SelectListItem> BookConditionList { get; set; }
        public string BillNo { get; set; }
        public string BookISBNNo { get; set; }
        public string BookNo { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Edition { get; set; }
        public string BookCategory { get; set; }
        public string Publisher { get; set; }
        public string NoofCopies { get; set; }
        public string ShelfNo { get; set; }
        public string BookPosition { get; set; }
        public string BookCost { get; set; }
        public string Language { get; set; }
        public string BookCondition { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string PurchaseDate { get; set; }
        public string BookName { get; set; }
        public List<System.Web.Mvc.SelectListItem> BookCategoryList { get; set; }







    }

    public class BookCategoryViewModel
    {
        public int BookCategoryID { get; set; }
        public string Category { get; set; }
        public string SectionCode { get; set; }
        public string SchoolID { get; set; }
        public string IsDelete { get; set; }
    }



    public class BookIssuedViewModel
    {

        public int BookIssuedID { get; set; }
        public string IssuedDate { get; set; }
        public string DueDate { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string UserCourse { get; set; }
        public string BookName { get; set; }
        public string BookNo { get; set; }
        public string BooKTitle { get; set; }
        public string UserSection { get; set; }
        public string SchoolID { get; set; }
        public string UserID { get; set; }
        public string IsDelete { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }

        public List<BookViewModel> BookDetailList { get; set; }











    }
    public class ReturnBookViewModel
    {
        public int ReturnBookID { get; set; }
        public byte[] BookID { get; set; }
        public string BookNo { get; set; }
        public string BookName { get; set; }
        public string Status { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string Remark { get; set; }
        public string ReturnDate { get; set; }
        public string FineAmount { get; set; }

        public string IsDelete { get; set; }
        public string UserClass { get; set; }
        public string SchoolId { get; set; }
        public string Section { get; set; }
        public string BookofId { get; set; }
        public List<System.Web.Mvc.SelectListItem> BookStatusList { get; set; }
    }

    public class ReportBookViewModel
    {


        public int BookID { get; set; }
        public string BookCategoryID { get; set; }
        public List<System.Web.Mvc.SelectListItem> Reoprtsforbook { get; set; }
        public string BillNo { get; set; }
        public string BookISBNNo { get; set; }
        public string BookNo { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Edition { get; set; }
        public string BookCategory { get; set; }
        public string Publisher { get; set; }
        public string NoofCopies { get; set; }
        public string ShelfNo { get; set; }
        public string BookPosition { get; set; }
        public string BookCost { get; set; }
        public string Language { get; set; }
        public string BookCondition { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string PurchaseDate { get; set; }
        public string BookName { get; set; }
        public List<System.Web.Mvc.SelectListItem> BookCategoryList { get; set; }

        public string Report { get; set; }


        public string ReportType { get; set; }
        public List<System.Web.Mvc.SelectListItem> ReportTypeList { get; set; }




    }






}


