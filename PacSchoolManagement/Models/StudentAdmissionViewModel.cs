﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class StudentAdmissionViewModel
    {
        
            public int AddmissionID { get; set; }
            public string RegdNo { get; set; }
            public string Name { get; set; }
            public string Gender { get; set; }
            public string DateofBirth { get; set; }
            public Nullable<int> Class { get; set; }
            public string FatherName { get; set; }
            public string MobileNo { get; set; }
            public string AppliedDate { get; set; }
            public int SchoolID { get; set; }
            public string Status { get; set; }
            public Nullable<int> SessionId { get; set; }
            public List<System.Web.Mvc.SelectListItem> StudentAdmissionList { get; set; }
        }
    
}