﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class AdminDashboardViewModel
    {
        public int SchoolId { get; set; }
        public int School { get; set; }
        public int Student { get; set; }
        public int Employee { get; set; }
        public int studentaddmission { get; set; }
        public int PEmployee { get; set; }
        public int CEmployee { get; set; }
    }
}