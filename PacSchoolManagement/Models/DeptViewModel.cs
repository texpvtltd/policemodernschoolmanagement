﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class DeptViewModel
    {
        public int DeptId { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public string IsDelete { get; set; }
    }
}