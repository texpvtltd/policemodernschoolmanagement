﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class StudentViewModel
    {
        public int StudentID { get; set; }
        public string RegdNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> Section { get; set; }
        public Nullable<int> Session { get; set; }
        public int SchoolID { get; set; }
        public string SudentImage { get; set; }
        public string SudentName { get; set; }
        public string ScloolName { get; set; }
        public string Attendance { get; set; }
        public string Grade { get; set; }
        public string Marksobtained { get; set; }
        public Nullable<int> UserID { get; set; }
        public string IsDelete { get; set; }
        public string Document { get; set; }
        public string SessinId { get; set; }
        public string ClassName { get; set; }
        public string AttStatus { get; set; }
        public string Remark { get; set; }
        public List<System.Web.Mvc.SelectListItem> ClassList { get; set; }
        public List<System.Web.Mvc.SelectListItem> StudentAdmissionList { get; set; }
        // public List<System.Web.Mvc.SelectListItem> SessioList { get; set; }
        public List<System.Web.Mvc.SelectListItem> SessionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> SectionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> GenderList { get; set; }
        public List<System.Web.Mvc.SelectListItem> AttendenceStatusList { get; set; }



    }

    public class StudentUploadDocViewModel
    {
        public int DocID { get; set; }
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string StudentId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }

    }


    public class AttendenceViewModel
    {
        public int AttendanceID { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> SectionId { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public Nullable<int> CourseID { get; set; }
        public string Date { get; set; }
        public string SchoolId { get; set; }
        public string StudentName { get; set; }
        public Nullable<int> SessionID { get; set; }
        public string IsDelete { get; set; }
        public string AttStatus { get; set; }
        public List<System.Web.Mvc.SelectListItem> SessionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> SectionList { get; set; }
        public List<System.Web.Mvc.SelectListItem> ClassList { get; set; }
        public List<StudentViewModel> ListStudentDetail { get; set; }
        public string StudentDetail { get; set; }
        public List<System.Web.Mvc.SelectListItem> AttendenceStatusList { get; set; }
        public string AttenceDetail { get; set; }
        public List<AttendenceViewModel> AttendenceStudentDetail { get; set; }

        public string ClassName { get; set; }
        public string Remark { get; set; }

    }







}