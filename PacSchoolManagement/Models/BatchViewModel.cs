﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class BatchViewModel
    {
        public int BatchId { get; set; }
        public string BatchName { get; set; }
        public string CourseName { get; set; }
        public Nullable<int> CourseId { get; set; }
        public List<System.Web.Mvc.SelectListItem> CourseList { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> SessionId { get; set; }
        public string IsDelete { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string MaximumStudents { get; set; }
    }
}