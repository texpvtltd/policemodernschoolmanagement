﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class VisitorViewModel
    {
        public int Id { get; set; }
        public Nullable<int> SchoolId { get; set; }
        public Nullable<int> SessionId { get; set; }
        public string TokenNo { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Purpose { get; set; }
        public string MobileNo { get; set; }
        public string Remarks { get; set; }
        public List<System.Web.Mvc.SelectListItem> CategoryList { get; set; }
    }
}