﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class HeadQViewModel
    {
    }

    public class PrincipalViewModel
    {
        public int PrincipalID { get; set; }
        public string PrincipalName { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public string PrincipalImage { get; set; }
        public Nullable<int> Session { get; set; }
        public string Contact { get; set; }
        public string IsDelete { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string SchoolName { get; set; }
        public HttpPostedFileBase ImageUpload { get; set; }

        public List<System.Web.Mvc.SelectListItem> PricipalList { get; set; }
        public List<System.Web.Mvc.SelectListItem> schoollList { get; set; }

    }


    public class SessionViewModel
    {

        public int SessionID { get; set; }
        public string Session1 { get; set; }
        public string FromYear { get; set; }
        public string ToYear { get; set; }

        public Nullable<int> SchoolID { get; set; }

        public List<System.Web.Mvc.SelectListItem> SessionList { get; set; }
    }


    //public class CourseViewModel
    //{

    //    public int CourseId { get; set; }
    //    public string CourseName { get; set; }
    //    public string SubId { get; set; }
    //    public string ClassId { get; set; }
    //    public string SessionId { get; set; }
    //    public string SchoolId { get; set; }
    //    public string IsDelete { get; set; }
    //    public Nullable<int> SchoolID { get; set; }

    //    public List<System.Web.Mvc.SelectListItem> SessionList { get; set; }
    //}
















}