﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PacSchoolManagement.Models
{
    public class HostalViewModel
    {
    }
    public class HostalTypeViewModel
    {
        public int HostalTypeID { get; set; }
        public string HostalType1 { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }

    }

    public class HostalDetailViewModel
    {



        public int HostalId { get; set; }
        public string HostalName { get; set; }
        public string HostalType { get; set; }
        public string HostalAddress { get; set; }
        public string HostalContact { get; set; }
        public string WardenName { get; set; }
        public string WardenPhoneNo { get; set; }
        public string WardanAddress { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
     //   public List<System.Web.Mvc.SelectListItem> HostalTypeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> HostalTypeList { get; set; }
        public string Hostaltyp { get; set; }
    }




     public class AddHostaRoomlViewModel
     {
      public int RoomId { get; set; }
        public string HostalType { get; set; }
        public string HostalName { get; set; }
        public string HostalfloorName { get; set; }
        public string RoomNo { get; set; }
        public string NoOfBed { get; set; }
        public string FeeType { get; set; }
        public string BedNo { get; set; }
        public string RoomRent { get; set; }
        public string Amount { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
            public List<System.Web.Mvc.SelectListItem> HostalTypeList { get; set; }
            public List<System.Web.Mvc.SelectListItem> HostalNameList { get; set; }
            public List<System.Web.Mvc.SelectListItem> HostalFeetypeList { get; set; }
    
     }




   public class HostelAllocationViewModel
   {
       public int HostelAllocatedId { get; set; }
        public string UserType { get; set; }
        public string UsarName { get; set; }
        public string HostelName { get; set; }
        public string HostalType { get; set; }
        public string HostelRoom { get; set; }
        public string HostelRegdate { get; set; }
        public string VacatingDate { get; set; }
        public string SchoolId { get; set; }
        public string IsDelete { get; set; }
        public string FloorName { get; set; }
        public string Employee { get; set; }
        public List<System.Web.Mvc.SelectListItem> HostalTypeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> HostalNameList { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> HostalRoomList { get; set; }
        public List<System.Web.Mvc.SelectListItem> StudentList { get; set; }
        public List<System.Web.Mvc.SelectListItem> EmpolyeeList { get; set; }
       
    

   }




    public class HostelVisitorViewModel
    {
      public int VisitorId { get; set; }
        public string UserName { get; set; }
        public string StudentName { get; set; }
        public string Employee { get; set; }
        public string UserType { get; set; }
        public string VisitDate { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
        public string VisitorName { get; set; }
        public string VisitTime { get; set; }
        public string Relation { get; set; }
        public string HostelName { get; set; }
        public string Hosteltype { get; set; }
        public List<System.Web.Mvc.SelectListItem> StudentList { get; set; }
        public List<System.Web.Mvc.SelectListItem> EmpolyeeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }
    }
    public class HostelRegisterViewModel
    {

        public int HostelRegID { get; set; }
        public string UserType { get; set; }
        public string UserName { get; set; }
        public string UStatus { get; set; }
        public string HosDate { get; set; }
        public string HosTime { get; set; }
        public string IsDelete { get; set; }
        public string SchoolID { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string RoomNo { get; set; }
        public string FloorName { get; set; }
        public List<System.Web.Mvc.SelectListItem> InOutList { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }

        public List<System.Web.Mvc.SelectListItem> StudentList { get; set; }
        public List<System.Web.Mvc.SelectListItem> EmpolyeeList { get; set; }

    }

    public class HostelTransferViewModel
    {

        public int TransferId { get; set; }
        public string UsesType { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string HostelRoom { get; set; }
        public string IsDelete { get; set; }
        public string SchoolID { get; set; }
        public List<System.Web.Mvc.SelectListItem> transfersstatusList { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }

        public List<System.Web.Mvc.SelectListItem> StudentList { get; set; }
        public List<System.Web.Mvc.SelectListItem> EmpolyeeList { get; set; }
        public List<System.Web.Mvc.SelectListItem> RoomList { get; set; }

        public List<System.Web.Mvc.SelectListItem> HostelTypeList { get; set; }

        public List<System.Web.Mvc.SelectListItem> HostelNameList { get; set; }

    }











     public class HostelFeeCollectionViewModel
     {

     public int FeesId { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string RoomNo { get; set; }
        public string HostelType { get; set; }
        public string HostelName { get; set; }
        public string FeeType { get; set; }
        public string ModeOfPay { get; set; }
        public string Amount { get; set; }
        public string Fine { get; set; }
        public string Discount { get; set; }
        public string TotalAmount { get; set; }
        public string Recipt { get; set; }
        public string ReciptNo { get; set; }
        public string BankAccountNo { get; set; }
        public string BankName { get; set; }
        public string ChequeNo { get; set; }
        public string CheaqueDate { get; set; }
        public string IsDelete { get; set; }
        public string SchoolId { get; set; }
        public string Remark { get; set; }
        public List<System.Web.Mvc.SelectListItem> ModeOfPayList { get; set; }
        public List<System.Web.Mvc.SelectListItem> UserTypeList { get; set; }

        public List<System.Web.Mvc.SelectListItem> StudentList { get; set; }
        public List<System.Web.Mvc.SelectListItem> EmpolyeeList { get; set; }

     }
}